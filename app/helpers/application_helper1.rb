module ApplicationHelper
	def obtener_departamento_municipio(municipio_id)
        begin
          departamento_municipio = "---"
          municipio = CfgMunicipio.find(municipio_id)
          if !municipio.nil?
             departamento = municipio.cfg_departamento
             if !departamento.nil?
                 departamento_municipio = departamento.nombre + " - " + municipio.nombre
             end    
          end
        rescue Exception => exc
          puts "Exception ", exc
        ensure
           return departamento_municipio
        end     
    end

    def lista_errores(objeto)
    	html = String.new
	  	if objeto.errors.any?
	  		html += '<div id="error_explanation">' + "\n"
	      	html += '<h2><%= pluralize(objeto.errors.count, "error") %> impide que estos datos se guarden:</h2>' + "\n"

	      	html += '<ul>' + "\n" 
	       	objeto.errors.full_messages.each do |msg| 
	        	html += '<li><%= msg %></li>' + "\n"
	       	end 
	      	html += '</ul>'  + "\n"
	    	html += '</div>' + "\n"
	   	end     
	   	html
	end
	    
end
