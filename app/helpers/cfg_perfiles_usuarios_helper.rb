module CfgPerfilesUsuariosHelper
	def obtener_opciones_padre_menu_activas
      CfgOpcionesMenu.select("id, UPPER(nombre_opcion) AS nombre_opcion, id_opcion_padre")
      .where("activo IS TRUE AND id_opcion_padre = ? AND id <> ?", 0, 0)
      .order("id ASC") #orden  #nombre_opcion
  end 

  def obtener_subopciones_activas(id_opcion_padre)
      CfgOpcionesMenu.select("id, UPPER(nombre_opcion) AS nombre_opcion, id_opcion_padre")
      .where("activo IS TRUE AND id_opcion_padre = ? ", id_opcion_padre)
      .order("id ASC") #nombre_opcion #nombre_opcion
  end 

  def operaciones_segun_opcion(opcion_id)
      CfgOperacionesOpcionesMenu.select("id, operacion")
      .where("activo IS TRUE AND cfg_opciones_menu_id = ? ", opcion_id) 
      .order("operacion")
  end

  def opciones_menu_perfil(perfil_id)
    html = ""
      
        # Obtener las opciones padre del menú segun el perfil
        # NIVEL 0       
        opciones_padre_perfil = obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, 0)          
        # Recorrer las opciones del menu padre e hijas segun el perfil
        if !opciones_padre_perfil.nil? and opciones_padre_perfil.size > 0     
          opciones_padre_perfil.each do |opcion_padre|            
            # Obtener las opciones por cada menu
            # NIVEL 1
            opciones = obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, opcion_padre.id)
             if !opciones.nil? and opciones.size > 0 
                # Mostrar la opcion padre
                html = html + "<b>" + opcion_padre.nombre_opcion.upcase + "</b>" 
                html = html + "<BR/>"               
                html = html + "<ul>"
                # Recorrer las opciones
                opciones.each do |opcion|                          
                  html = html + "<li>" +  opcion.nombre_opcion.upcase + "</li>" 
                  #html = html + "<BR/>"
                  # Buscar las sub-opciones
                  # NIVEL 2
                  sub_opciones = obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, opcion.id)
                  if !sub_opciones.nil? 
                    if sub_opciones.size > 0
                      # Mostrar la opcion con submenu
                      sub_opciones.each do |sub_opcion|
                        html = html + "        - " +  sub_opcion.nombre_opcion.upcase              
                        html = html + "<BR/>"
                      end  
                    end                      
                  end                   
                end   # end del each opciones
                html = html + "</ul>"
             end   
          end  # end del each opciones_padre
        end # end del if opciones_padre          
    return html      
  end

  def operaciones_segun_opcion_texto(perfil_id, opcion_id)      
      puts "helper - operaciones_segun_opcion_texto... "
      puts perfil_id
      puts opcion_id
      operaciones = CfgOperacionesPerfil.select("operacion")
                    .joins("JOIN cfg_operaciones_opciones_menu ON cfg_operaciones_perfiles.cfg_operaciones_opciones_menu_id = cfg_operaciones_opciones_menu.id")  
                    .where("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu_id = ? ", perfil_id, opcion_id) 
                    .order("operacion")
      if operaciones.size > 0
        texto_operaciones = "("
        operaciones.each do |o|
          if texto_operaciones == "("
            texto_operaciones = texto_operaciones + o.operacion
          else
            texto_operaciones = texto_operaciones + ", " +  o.operacion
          end          
        end              
        texto_operaciones = texto_operaciones + ")" 
      end  
      return texto_operaciones
  end


end
