module CitTurnosHelper
  # Listar todas las sedes
  def obtener_sedes
  	CfgSede.all
  end

  # Listar todos los usuarios prestadores
  def obtener_prestadores(cfg_especialidad_id)
  	(cfg_especialidad_id.blank?) ? CfgUsuario.where(tipo_usuario: 1774) : CfgUsuario.where("cfg_especialidad_id = ? AND tipo_usuario = 1774",cfg_especialidad_id)
  end

  # Listar todos los horarios
  def obtener_horarios
  	CfgHorario.all
  end

  # Listar todas las especialidades
  def obtener_especialidades_turno
  	CfgEspecialidad.order("descripcion")
  end
  
  def obtener_duracion(cfg_especialidad_id)
  	x = CfgEspecialidad.where(id: cfg_especialidad_id)
  	(x.blank?) ? nil : x.first.duracion_consulta  
  end

  def events_ajax_previous_link
    params_copy = params.delete_if {|key, value| key == "action" }
    ->(param, date_range) { link_to raw("&laquo;"), {param => date_range.first - 1.day}.merge(params_copy), remote: :true}
  end

  def events_ajax_next_link
    params_copy = params.delete_if {|key, value| key == "action" }
    ->(param, date_range) { link_to raw("&raquo;"), {param => date_range.last + 1.day}.merge(params_copy), remote: :true}
  end  

  def get_horarios(horario_id)    
    CfgItemsHorario.where(cfg_horario_id: horario_id)
  end

  def get_pacientes(identificacion)
    x = CfgPaciente.last(5).reverse if identificacion.blank?
    x = CfgPaciente.where("identificacion = ?", identificacion) if !identificacion.blank?    
    return x
  end

  def obtener_servicios
    FacServicio.all
  end

end
