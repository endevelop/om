module CfgPacientesHelper
	# Método para mostrar la descripción del campo EPS-Empresa Prestadora de Servicio
	def obtener_eps(fac_administradora_id)		
		razon_social = "---"
		eps = FacAdministradora.find(fac_administradora_id) if !fac_administradora_id.blank?
		if !eps.nil?
			razon_social = eps.razon_social
		end
		return razon_social
	end

	# Métodos para obtener la descripción de los principales renglones de tablas maestras en cfg_clasificaciones
    #def obtener_cfg_clasificaciones(maestro)
    #    CfgClasificacion.where(maestro: maestro, activo: true).order(descripcion: :asc)
    #end

    def obtener_tipo_identificacion
    	obtener_cfg_clasificaciones("TipoIdentificacion")
    end

    def obtener_genero
    	obtener_cfg_clasificaciones("Genero")
    end

    def obtener_ocupacion
    	obtener_cfg_clasificaciones("Ocupacion")
    end

    def obtener_estado_civil
    	obtener_cfg_clasificaciones("EstadoCivil")
    end

    def obtener_grupo_sanguineo
    	obtener_cfg_clasificaciones("GrupoSanguineo")
    end

    def obtener_departamentos
    	CfgDepartamento.order(:nombre)
    end

    def obtener_zonas
    	obtener_cfg_clasificaciones("Zona")
    end

    def obtener_all_eps()
    	FacAdministradora.all
    end

    def obtener_tipo_afiliado
    	obtener_cfg_clasificaciones("TipoAfiliado")
    end

    def obtener_regimen
    	obtener_cfg_clasificaciones("Regimen")
    end

    def obtener_categoria_paciente
    	obtener_cfg_clasificaciones("CategoriaPaciente")
    end

    def obtener_etnia
    	obtener_cfg_clasificaciones("Etnia")
    end

    def obtener_escolaridad
    	obtener_cfg_clasificaciones("Escolaridad")
    end

    def obtener_parentesco
    	obtener_cfg_clasificaciones("Parentesco")
    end

    def obtener_estrato
        obtener_cfg_clasificaciones("Estrato")
    end

end
