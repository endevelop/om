module CfgSedesHelper
	def lista_todas_sedes_por_empresa(cfg_empresa)
		cfg_sedes = CfgSede.select("cfg_sedes.id, cfg_sedes.codigo_sede, cfg_sedes.nombre_sede, cfg_sedes.activo, cfg_sedes.cfg_empresa_id, cfg_sedes.cfg_municipio_id, cfg_departamentos.nombre || ' - ' || cfg_municipios.nombre AS descripcion_departamento_municipio")
		.joins(:cfg_municipio => :cfg_departamento)
		.where(cfg_empresa_id: cfg_empresa.id)
		.order('nombre_sede')
	end	

    def devolver_valor_activo(param)
      param == true ? 'true':'false'
    end

end
