module CfgOperacionesOpcionesMenusHelper
	def obtener_detalle_operacion(operacion_id)
	  @detalles = CfgOperacionesDetalle.select("id, cfg_operaciones_opciones_menu_id, controlador, accion, activo")
      .order("controlador, accion")
      .where(["cfg_operaciones_opciones_menu_id = ?", operacion_id ])
    end 
end
