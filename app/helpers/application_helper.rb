module ApplicationHelper

  # Método para obtener la descripción de departamento - municipio, dado el municipio_id
	def obtener_departamento_municipio(municipio_id)
		begin
		  departamento_municipio = "---"
		  municipio = CfgMunicipio.find(municipio_id)
		  if !municipio.nil?
		     departamento = municipio.cfg_departamento
		     if !departamento.nil?
		     	departamento_municipio = departamento.nombre + " - " + municipio.nombre
		     end	
		  end
	  rescue Exception => exc
          puts "Exception ", exc
	  ensure
           return departamento_municipio
	  end	 
  end

  def obtener_departamento_municipio_n(municipio_id)
    
    begin
      departamento_municipio = "---"
      #municipio = CfgMunicipio.find(municipio_id)

      municipio = CfgMunicipio.select("cfg_municipios.nombre as nombre_municipio, cfg_departamentos.nombre as nombre_departamento")
      .left_outer_joins(:cfg_departamento)
        .where(id: municipio_id)
      puts municipio.inspect
      if !municipio.nil?
      #   departamento = municipio.cfg_departamento
      #   if !departamento.nil?
      #    departamento_municipio = departamento.nombre + " - " + municipio.nombre
      #   end  
          departamento_municipio = municipio.nombre_departamento + " - " + municipio.nombre_municipio
      end

      #  cfg_sedes = CfgSede.select("cfg_sedes.id, cfg_sedes.codigo_sede, cfg_sedes.nombre_sede, cfg_sedes.activo, cfg_sedes.cfg_empresa_id, cfg_sedes.cfg_municipio_id, cfg_departamentos.nombre || ' - ' || cfg_municipios.nombre AS descripcion_departamento_municipio")
      #.joins(:cfg_municipio => :cfg_departamento)
      #.where(cfg_empresa_id: cfg_empresa.id)
      #.order('nombre_sede')
    rescue Exception => exc
      puts "Exception ", exc
    ensure
      return departamento_municipio
    end  




  end
  
    # Método para obtener todas las especialidades
    def obtener_especialidades
    	#especialidades = CfgClasificacion.where(maestro: 'Especialidad').order(descripcion: :asc)
      obtener_cfg_clasificaciones("Especialidad")
    end	

    # Método para obtener todos los tipos de documento
    def obtener_tipo_documentos
      obtener_cfg_clasificaciones("TipoDocumento")
    end 

    # Método para obtener todos los tipos de identificacion
    def obtener_tipo_identificacion
      obtener_cfg_clasificaciones("TipoIdentificacion")
    end 

    # Método para obtener todos los tipos de usuarios
    def obtener_tipo_usuarios
      obtener_cfg_clasificaciones("TipoUsuario")
    end 

    # Método para obtener todos los generos
    def obtener_generos
      obtener_cfg_clasificaciones("Genero")
    end 

    def obtener_tipo_personal
      obtener_cfg_clasificaciones("PersonalAtiende")
    end  

    def obtener_motivos_consulta
      obtener_cfg_clasificaciones("MotivoConsulta")
    end

    def obtener_motivos_cancelacion
      obtener_cfg_clasificaciones("MotiCancCitas")
    end

    def obtener_cfg_clasificaciones(maestro)
      CfgClasificacion.select("id, UPPER(codigo) || ' - ' || UPPER(descripcion) AS descripcion").
      where(maestro: maestro, activo: true).order(codigo: :asc)
    end  

    def obtener_estatus_activo_inactivo
      [["ACTIVO", true],["INACTIVO", false]]
    end

    def obtener_estatus_si_no
      [["SI", true],["NO", false]]
    end

    def obtener_descripcion_cfg_clasificaciones(codigo)
    	descripcion = "---"
      if !codigo.blank?
    	  clasificacion = CfgClasificacion.find(codigo)
    	  descripcion = clasificacion.descripcion.upcase if !clasificacion.nil?
      end
      return descripcion
    end	

    def mostrar_titulo_activar(param)
      texto = ""
      if !param.nil?         
         texto = "Activar"   if param == false
         texto = "Inactivar" if param == true         
      end
      return texto  
    end  

    def mostrar_titulo_activar_icono(param)
      texto = ""
      if !param.nil?                  
         if param == false # texto = "Activar"   
           texto = "open_iconic :icon, :check"
         end
         if param == true  # texto = "Inactivar" 
           texto = "open_iconic :icon, :x"
         end
      end
      return texto  
    end  
    
    def obtener_perfiles_usuarios_activos
      CfgPerfilesUsuario.select("id, UPPER(nombre_perfil) AS nombre_perfil").
      where(activo: true).order(nombre_perfil: :asc)
    end  

    #
    #def obtener_opciones_menu_padre
    #  CfgOpcionesMenu.select("DISTINCT(id_opcion_padre) AS id, nombre_opcion, style, url_opcion orden ").
    #  order(orden: :asc)
    #end 
    
    # 2 - 
    def obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, opcion_padre_id)
      CfgOpcionesMenuPerfilUsuario.select("cfg_opciones_menu.id, cfg_opciones_menu_id, 
                           id_opcion_padre, nombre_opcion, orden, url_opcion").
      left_outer_joins(:cfg_opciones_menu).
      where("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu.id_opcion_padre = ? AND cfg_opciones_menu.id <>0 AND cfg_opciones_menu.activo IS TRUE", perfil_id, opcion_padre_id).
      order("orden ASC")
    end  

    # 1 - 
    #def obtener_opciones_padre_menu_segun_perfil(perfil_id)
    #  CfgOpcionesMenuPerfilUsuario.select("cfg_opciones_menu.id, cfg_opciones_menu_id, 
    #                        id_opcion_padre, nombre_opcion, orden, style, url_opcion").
    #  left_outer_joins(:cfg_opciones_menu).
    #  where("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu.id_opcion_padre = ? AND cfg_opciones_menu.activo IS TRUE", perfil_id, 0).
    #  order("orden asc")
    #end  

  def opciones_menu(perfil_id)
      html = ""
      if user_signed_in? #and !session[:muestra_menu].nil? and session[:muestra_menu] == false
        # Obtener las opciones padre del menú segun el perfil
        #opciones_padre_perfil = obtener_opciones_padre_menu_segun_perfil(perfil_id)
        # NIVEL 0 
        opciones_padre_perfil = obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, 0)

        # Empezar a construir el header
        html = html + "<div class='navbar navbar-expand-lg fixed-top navbar-dark bg-primary'>"
        #html = html + "<div class='container'>"
        #html = html + "<a class='navbar-brand' href='#'>OPEN MEDICAL</a>"
        html = html + "<a class='navbar-brand' data-method='delete' href='/users/sign_out'> OPEN MEDICAL </a>"

        html = html + "<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>"
        html = html + "<span class='navbar-toggler-icon'></span>"
        html = html + "</button>"
        html = html + "<div class='collapse navbar-collapse' id='navbarResponsive'>"
        html = html + "<ul class='navbar-nav'>"#--------------------
        #html = html + "<ul class='navbar-nav' data-target='.navbar-collapse' data-toggle='collapse'>"
      
        # Recorrer las opciones del menu padre e hijas segun el perfil
        if !opciones_padre_perfil.nil? and opciones_padre_perfil.size > 0
          opciones_padre_perfil.each do |opcion_padre|            
            # Obtener las opciones por cada menu
            # NIVEL 1
            opciones = obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, opcion_padre.id)
            if !opciones.nil? and opciones.size > 0
                # Mostrar la opcion padre
                html = html + "<li class='nav-item dropdown'>"
                #html = html + "<a class='nav-link dropdown-toggle' href='#' id='themes' data-toggle='dropdown'  >" #
                html = html + "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'  >" #
                #html = html + "<a class='nav-item nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'  >" #
                html = html + opcion_padre.nombre_opcion.upcase # + "<span class='caret'></span>"
                html = html + "</a>"
                #html = html + "<div class='dropdown-menu' aria-labelledby='themes'>"
                html = html + "<ul class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>"
                # Recorrer las opciones
                opciones.each do |opcion|              
                  url = ""
                  url = opcion.url_opcion if !opcion.url_opcion.blank?
                  #html = html + "<a class='dropdown-item' " + " href='" + url  + "'>" + opcion.nombre_opcion + "</a>"              
                  # Buscar las sub-opciones
                  # NIVEL 2
                  sub_opciones = obtener_opciones_menu_segun_perfil_opcion_padre(perfil_id, opcion.id)
                  if !sub_opciones.nil? 
                    if sub_opciones.size > 0
                      # Mostrar la opcion con submenu
                      html = html + "<li class='dropdown-submenu'><a class='dropdown-item dropdown-toggle' " + " href='" + url  + "'>" + opcion.nombre_opcion.upcase + "</a>"          
                      html = html + "<ul class='dropdown-menu'>"
                      sub_opciones.each do |sub_opcion|
                        url = ""
                        url = sub_opcion.url_opcion if !sub_opcion.url_opcion.blank?
                        html = html + "<li><a class='dropdown-item' " + " href='" + url  + "'>" + sub_opcion.nombre_opcion.upcase + "</a></li>"              
                      end  
                      html = html + "</ul>"
                    else 
                      # Mostrar la opcion sin submenu
                      html = html + "<li><a class='dropdown-item' " + " href='" + url  + "'>" + opcion.nombre_opcion.upcase + "</a></li>"              
                    end  
                  end                  
                end
                html = html + "</ul>"                       
                
                #html = html + "</div>"
                #html = html + "</ul>"
                #html = html + "</li>"
            end  # fin del if !opciones.nil? and opciones.size > 0 
           #end #----------
           html = html + "</li>"
          end  # end del each opciones_padre
        end # end del if opciones_padre
        html = html + "</ul>" # SI - cierre del ul principal que engloba las opciones

        # Opción de logout    
        html = html + "<ul class='nav navbar-nav ml-auto'>"
        html = html + "<li class='nav-item dropdown'>"
        html = html + "<a class='nav-link dropdown-toggle' data-toggle='dropdown' href='/users/sign_out' id='download'>"
        html = html +  session[:nombre_usuario].upcase
        html = html + "<span class='caret'></span></a>"
        html = html + "<ul class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>"
        html = html + "<li><a class='dropdown-item' data-method='delete' href='/users/sign_out'> SALIR </a></li>"
        html = html + "</ul>"
        html = html + "</li>"
        html = html + "</ul> "
        html = html + "</div>"
        html = html + "</div>"    

        session[:muestra_menu] = true
      end      
    return html      
  end 
 

  def mostrar_fecha(fecha)
    fecha_formateada = ""
    if !fecha.blank?
      fecha_formateada = fecha.strftime("%d/%m/%Y")
    end 
    return fecha_formateada
  end

  def mostrar_hora(hora)
    hora_formateada = ""
    if !hora.blank?
      hora_formateada = hora.to_time.strftime("%I:%M%p")
    end
    return hora_formateada
  end

  def obtener_empresas_activas
    CfgEmpresa.select("id, UPPER(razon_social) AS razon_social").
      where(activo: true).order(razon_social: :asc)    
  end 

  def obtener_empresas_activas_id(empresa_id)
    CfgEmpresa.select("id, UPPER(razon_social) AS razon_social").
      where(activo: true).where(id: empresa_id).order(razon_social: :asc)    
  end 

  def obtener_empresas_todas
    CfgEmpresa.select("id, UPPER(razon_social) AS razon_social").
      order(razon_social: :asc)    
  end  

  # METODO QUE RETORNA TRUE/FALSE, SI EL PERFIL DE USUARIO 
  # ESTA ASOCIADO A UNA EMPRESA
  def perfil_asociado_empresa(perfil_id)
    #perfil = CfgPerfilesUsuario.find(perfil_id)
    #perfil.asociado_empresa
    session[:usuario_asociado_empresa] 
  end
  
  def obtener_sedes_todas(empresa_id)
    if !empresa_id.blank?
      CfgSede.select("id, nombre_sede").where(cfg_empresa_id: empresa_id).order(nombre_sede: :asc)
    else
      Array.new  
    end
  end
  #def obtener_empresas_sedes
  #  empresas = CfgEmpresa.select("id, UPPER(razon_social) AS razon_social").order(razon_social: :asc)    
  #  arreglo = []
  #  i = 0
  #  empresas.each do |empresa|
  #     arreglo[i,0] << empresa.razon_social
  #     CfgSede.
  #     arreglo[i,1] << 
  #  end
  #end  

  def mostrar_imagen(imagen)
    if !imagen.nil?
       image_tag imagen.image.url(:small)
    else
       image_tag "missing.png"
    end   
  end

  def mostrar_operacion_activar(valor_activo)
    operacion = ""
    if valor_activo == true
      operacion = "<span class='oi oi-x' title='Inactivar' aria-hidden='true'></span>"
    elsif valor_activo == false 
      operacion = "<span class='oi oi-check' title='Activar' aria-hidden='true'></span>"
    end
    return operacion
  end  

end
