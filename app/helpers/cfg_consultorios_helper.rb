module CfgConsultoriosHelper
	def lista_todos_consultorios_por_sede(cfg_empresa, cfg_sede)
		cfg_consultorios = CfgConsultorio.select("cfg_consultorios.id, cod_consultorio, nom_consultorio, piso_consultorio, cfg_consultorios.activo, cfg_clasificaciones.descripcion AS descripcion_especialidad, cfg_consultorios.cfg_sede_id")
		.joins(:cfg_clasificacion)
		.where(cfg_sede_id: cfg_sede.id)
		.order('nom_consultorio')
	end	
end
