module CfgOpcionesMenusHelper

  def obtener_opciones_padres_activas
  	CfgOpcionesMenu.select("cfg_opciones_menu.id, cfg_opciones_menu.activo,
  		            UPPER(cfg_opciones_menu_padre.nombre_opcion) || ' - ' ||
                    UPPER(cfg_opciones_menu.nombre_opcion) as nombre_opcion")
  	  .joins("LEFT JOIN cfg_opciones_menu cfg_opciones_menu_padre  
  	  	     ON cfg_opciones_menu.id_opcion_padre = cfg_opciones_menu_padre.id")
      .where("cfg_opciones_menu.activo IS TRUE").
      order("cfg_opciones_menu_padre.nombre_opcion ASC, cfg_opciones_menu.nombre_opcion ASC")
  end
end
