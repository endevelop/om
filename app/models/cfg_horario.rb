class CfgHorario < ApplicationRecord
  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update]

  # Relaciones con otros modelos
  has_many :cfg_items_horarios, -> { order("dia, hora_inicio") }, dependent: :restrict_with_exception
  has_many :cit_turnos, dependent: :restrict_with_exception

  # Validaciones
  validates :descripcion, presence: {message: "Descripción: Debe ingresar el valor"}

  protected
  	def cambiar_mayusculas
      self.descripcion 		  = descripcion.upcase if !descripcion.blank?      
  	end

end
