class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :authentication_keys => {email: true, login: false}
  
  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update] 

   has_one :cfg_usuario

   #devise :database_authenticatable, :authentication_keys => [:username]
   #validates :email,uniqueness: true 

   validates :username, uniqueness: { message: "Ya existe un usuario con el mismo login" }
   validates :email, uniqueness: { message: "Ya existe un usuario con el mismo email" }, if: :email_lleno?

   #validates :email, presence: {message: "Email: debe ingresar el valor"}, on: :create
   validates :password, presence: {message: "Clave: debe ingresar el valor"}, on: :create
   validates :password_confirmation, presence: {message: "Confirmación de la clave: debe ingresar el valor"}, on: :create   
   validates :username, presence: {message: "Login: debe ingresar el valor"}, on: :create

  def email_required?
    false
  end
  def email_lleno?
    !self.email.blank?
  end  
  
  #def self.find_for_database_authentication(params)
  #  User.find_by(email: "#{params[:email]}")
  #end

  protected
    def cambiar_mayusculas
      #self.username    = username.upcase if !username.blank?         
    end
end
