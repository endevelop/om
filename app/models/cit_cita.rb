class CitCita < ApplicationRecord
  # Relaciones con otros modelos
  belongs_to :cfg_paciente, optional: true
  belongs_to :fac_servicio, optional: true
  belongs_to :cit_turno, optional: true
  belongs_to :cfg_usuario, optional: true

  # Validación de campos obligatorios
  validates :cfg_paciente_id, presence: {message: "Paciente: debe seleccionar uno de la lista"}
  validates :fac_servicio_id, presence: {message: "Servicio: debe seleccionar uno de la lista"}
  validates :cit_turno_id, presence: {message: "Turno: debe seleccionar uno del calendario"}
  validates :cfg_usuario_id, presence: {message: "Prestador: debe seleccionar uno de la lista"}
  validates :tipo_cita, presence: {message: "Motivo Consulta: debe seleccionar uno de la lista"}

  def start_time
  	self.cit_turno.fecha
  end

  def nombre_completo_paciente
    "#{pprimer_nombre} #{psegundo_nombre} #{pprimer_apellido} #{psegundo_apellido}"
  end

  def nombre_completo_usuario
    "#{uprimer_nombre} #{usegundo_nombre} #{uprimer_apellido} #{usegundo_apellido}"
  end

end
