class CfgEmpresa < ApplicationRecord
  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update] 

  # Relaciones con otros modelos    
  has_many :cfg_sedes
  belongs_to :cfg_municipio, optional: true
  belongs_to :cfg_imagen, optional: true

  # Validaciones de campos obligatorios
  validates :codigo_empresa, presence: {message: "Código de empresa: debe ingresar el valor"}
  validates :razon_social, presence: {message: "Razón social: debe ingresar el valor"}
  validates :cfg_municipio_id, presence: {message: "Departamento-Municipio: debe seleccionar algún valor"}
  validate :file_size_under_one_mb

  ###
  has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200" }, :default_url => "missing.png"
  #has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  NUM_BYTES_IN_MEGABYTE = 1048576
  def file_size_under_one_mb
    if (self.image.size.to_f / NUM_BYTES_IN_MEGABYTE) > 1
      errors.add(:file, 'File size cannot be over one megabyte.')
    end
  end
  ###

  def esta_activo?
    self.activo == false ?'No':'Si'
  end  


  protected
  	def cambiar_mayusculas
      self.num_identificacion = num_identificacion.upcase if !num_identificacion.blank?      
  		self.dv                 = dv.upcase if !dv.blank?
      self.razon_social       = razon_social.upcase if !razon_social.blank?
      self.num_doc_rep_legal  = num_doc_rep_legal.upcase if !num_doc_rep_legal.blank?
      self.nom_rep_legal      = nom_rep_legal.upcase if !nom_rep_legal.blank?
      self.direccion          = direccion.upcase if !direccion.blank?
      self.regimen            = regimen.upcase if !regimen.blank?
      self.nivel              = nivel.upcase if !nivel.blank?
      self.razon_rip          = razon_rip.upcase if !razon_rip.blank?
      self.observaciones      = observaciones.upcase if !observaciones.blank?
      self.codigo_empresa     = codigo_empresa.upcase if !codigo_empresa.blank?
  	end
  

end
