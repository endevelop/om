class CfgMunicipio < ApplicationRecord
  belongs_to :cfg_departamento
  has_many :cfg_sedes
end
