class CfgUsuario < ApplicationRecord

	# Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update] 
  before_create :asignar_valores
  before_update :validaciones

  #
  attr_accessor :perfiles_asociados_empresa

  # Relaciones con otros modelos
	belongs_to :user
	belongs_to :cfg_perfiles_usuario	
	belongs_to :clasif_tipo_identificacion, class_name: "CfgClasificacion", foreign_key: "tipo_identificacion"
	belongs_to :clasif_tipo_usuario,  class_name: "CfgClasificacion", foreign_key: "tipo_usuario"
	belongs_to :clasif_genero,  class_name: "CfgClasificacion", foreign_key: "genero"
	belongs_to :clasif_especialidad,  class_name: "CfgClasificacion", foreign_key: "especialidad", optional: true
	belongs_to :clasif_personal_atiende,  class_name: "CfgClasificacion", foreign_key: "personal_atiende", optional: true
  belongs_to :cfg_sede, optional: true
  belongs_to :cfg_especialidad, optional: true
  # Foto del usuario
  belongs_to :cfg_imagen, optional: true
  # Firma del usuario
  belongs_to :imagen_firma,  class_name: "CfgImagen", foreign_key: "firma_id", optional: true
  has_many :cit_turnos
  has_many :cit_citas

	# Validaciones de los campos unique key
	validates :identificacion, uniqueness: { message: "Ya existe un usuario con el mismo identificador" }
	validates :login_usuario, uniqueness: { message: "Ya existe un usuario con el mismo login" }
	# Validaciones de los campos obligatorios
  validates :tipo_identificacion, presence: {message: "Tipo de documento de identificacion: debe seleccionar algún valor"}
  validates :identificacion, presence: {message: "Identificacion: debe ingresar algún valor"}
  validates :genero, presence: {message: "Género: debe seleccionar algún valor"}
  validates :primer_nombre, presence: {message: "Primer nombre: debe ingresar algún valor"}
  validates :primer_apellido, presence: {message: "Primer apellido: debe ingresar algún valor"}
  validates :cfg_perfiles_usuario_id, presence: {message: "Perfil: debe seleccionar algún valor"}
  validates :tipo_usuario, presence: {message: "Tipo de usuario: debe seleccionar algún valor"}
  #validates :estado_cuenta, presence: {message: "Estado de la cuenta: debe seleccionar algún valor"}
  validates :estado_cuenta, inclusion: { in: [true, false], message: "Debe seleccionar algún valor activo / inactivo" }
  #validates :visible, presence: {message: "Visible: debe seleccionar algún valor"}
  validates :visible, inclusion: { in: [true, false], message: "Debe seleccionar algún valor si / no" }
  #validates :mostrar_en_historias, presence: {message: "Mostrar en historias: debe seleccionar algún valor"}
  validates :mostrar_en_historias, inclusion: { in: [true, false], message: "Debe seleccionar algún valor si / no" }

  # Validaciones de los campos si selecciona el tipo de usuario prestador
  validates :especialidad, presence: {message: "Especialidad: debe seleccionar algún valor"}, if: :tipo_usuario_prestador?
  validates :registro_profesional, presence: {message: "Registro Profesional: debe ingresar el valor"}, if: :tipo_usuario_prestador?
  validates :personal_atiende, presence: {message: "Tipo de personal: debe seleccionar el valor"}, if: :tipo_usuario_prestador?	
  validates :unidad_funcional, presence: {message: "Unidad funcional: debe ingresar el valor"}, if: :tipo_usuario_prestador?	
  validates :porcentaje_honorario, presence: {message: "Porcentaje honorario: debe ingresar el valor"}, if: :tipo_usuario_prestador?	
  
  # Validaciones de los campos si selecciona el perfil asociado a una empresa
  validates :cfg_sede_id, presence: {message: "Empresa-Sede: debe seleccionar algún valor"}, if: :perfil_asociado_empresa?

    # Método que retorna si el tipo de usuario
    # es prestador
    def tipo_usuario_prestador?
       self.tipo_usuario == CONFIG["TIPO_USUARIO_PRESTADOR"] 
    end

    def perfil_asociado_empresa?
      CONFIG["PERFILES_ASOCIADOS_EMPRESA"].include?(self.cfg_perfiles_usuario_id)
      #self.perfiles_asociados_empresa.include?(self.cfg_perfiles_usuario_id)
    end

    # Obtener descripcion segun el estado de la cuenta del usuario
    # (Activa o Inactiva)
    def estado_cuenta_descripcion?
      self.estado_cuenta == true ?'ACTIVA':'INACTIVA'
    end

  # Busqueda de registros particulares
  scope :nombre_ilike, lambda {|x| where('primer_nombre ILIKE ? OR segundo_nombre ILIKE ? OR primer_apellido ILIKE ? OR segundo_apellido ILIKE ? OR cfg_especialidades.descripcion ILIKE ?', "%#{x}%","%#{x}%","%#{x}%","%#{x}%","%#{x}%")}

    def estatus?(valor)
      valor == true ?'SI':'NO'
    end

    # Concatenación del primer_nombre y segundo_nombre
	def nombres
	  nombres = ""	
	  nombres = nombres + self.primer_nombre  if !self.primer_nombre.blank?
	  nombres = nombres + " " + self.segundo_nombre if !self.segundo_nombre.blank?
	  return nombres
	end

	# Concatenación del primer_apellido y segundo_apellido
	def apellidos
	  apellidos = ""		
	  apellidos = apellidos + self.primer_apellido  if !self.primer_apellido.blank?
	  apellidos = apellidos + " " + self.segundo_apellido  if !self.segundo_apellido.blank?
	  return apellidos
	end	

  def nombre_completo
    return nombres + " " + apellidos
  end

  def nombre_especialidad
    if self.cfg_especialidad.nil?
      return nombre_completo
    else
      return nombre_completo + " (" + self.cfg_especialidad.descripcion + ")"
    end 
  end
   
    def descripcion_especialidad
      descrip_especialidad = ""	
      if !self.especialidad.blank?
        clasif_especialidad = self.clasif_especialidad
        if !clasif_especialidad.nil?
            descrip_especialidad = clasif_especialidad.descripcion if !clasif_especialidad.descripcion.blank?
        end    
      end  
      return descrip_especialidad	
    end

    def descripcion_tipo_personal
      descrip_tipo_personal = "" 
      if !self.personal_atiende.blank?
        clasif_personal_atiende = self.clasif_personal_atiende
        if !clasif_personal_atiende.nil?
          descrip_tipo_personal = clasif_personal_atiende.descripcion.upcase if !clasif_personal_atiende.descripcion.blank?
        end
      end  
      return descrip_tipo_personal   
    end

    def descripcion_perfil
      #clasif_especialidad.descripcion
      descrip_perfil = "" 
      if !self.cfg_perfiles_usuario_id.blank?
        usuario_perfil = self.cfg_perfiles_usuario
        if !usuario_perfil.nil?
           descrip_perfil = usuario_perfil.nombre_perfil.upcase if !usuario_perfil.nombre_perfil.blank?
        end    
      end  
      return descrip_perfil
    end    

    def descripcion_tipo_usuario
      #clasif_tipo_usuario.descripcion
      descripcion = "" 
      if !self.tipo_usuario.blank?
        clasif_tipo_usuario = self.clasif_tipo_usuario
        if !clasif_tipo_usuario.nil?
           descripcion = clasif_tipo_usuario.descripcion.upcase if !clasif_tipo_usuario.descripcion.blank?
        end    
      end  
      return descripcion
    end   

    def empresa_sede
      descripcion = ""
      sede = self.cfg_sede
      if !sede.nil?
         empresa = sede.cfg_empresa
         if !empresa.nil?
          descripcion = empresa.razon_social + " - " + sede.nombre_sede
         else
          descripcion =  sede.nombre_sede
         end  
      end  
      return descripcion
    end 

    # Metodo que verifica el acceso al usuario solicitado
    # por la empresa asociada al usuario de la sesion
    def autorizado_por_empresa?(empresa_id)
      autorizado = false
      sede = self.cfg_sede
      if !sede.nil?
         autorizado = true if sede.cfg_empresa_id == empresa_id
      end
      return autorizado  
    end 

	protected
  	  def cambiar_mayusculas
        #self.login_usuario    = login_usuario.upcase if !login_usuario.blank?         
        self.observacion      = observacion.upcase if !observacion.blank?        	
        self.primer_nombre    = primer_nombre.upcase if !primer_nombre.blank?        	        
        self.segundo_nombre   = segundo_nombre.upcase if !segundo_nombre.blank?       
        self.primer_apellido  = primer_apellido.upcase if !primer_apellido.blank?       
        self.segundo_apellido = segundo_apellido.upcase if !segundo_apellido.blank?       	
        self.direccion        = direccion.upcase if !direccion.blank?       	
        self.cargo_actual     = cargo_actual.upcase if !cargo_actual.blank?       	
        self.registro_profesional = registro_profesional.upcase if !registro_profesional.blank?       	
		    self.unidad_funcional     = unidad_funcional.upcase if !unidad_funcional.blank?       	
	  end

	  def asignar_valores
	  	self.fecha_creacion = Time.now
	  end	

    def validaciones
      if self.tipo_usuario_prestador?
        # Vienen llenos por el cfg_usuario_params
      else
        # Blanquear los campos
        self.especialidad         = nil
        self.registro_profesional = nil
        self.personal_atiende     = nil
        self.unidad_funcional     = nil
        self.porcentaje_honorario = nil
      end
      if self.perfil_asociado_empresa?
        # Vienen llenos por el cfg_usuario_params
      else
        # Blanquear los campos
        self.cfg_sede_id  = nil
      end    
    end

end
