class CfgConsultorio < ApplicationRecord
  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update]	

  # Relaciones con otros modelos    	
  belongs_to :cfg_sede
  belongs_to :cfg_clasificacion, foreign_key: "cod_especialidad" 
  has_many :cit_turnos
    
  # Validaciones de campos obligatorios
  validates :cod_consultorio, presence:  {message: "Código de consultorio: debe ingresar el valor"}
  validates :nom_consultorio, presence:  {message: "Nombre de consultorio: debe ingresar el valor"}
  validates :piso_consultorio, presence: {message: "Piso de consultorio: debe ingresar el valor"}
  validates :cod_especialidad, presence: {message: "Especialidad: debe seleccionar algún valor"}

  def esta_activo?
    self.activo == false ?'No':'Si'
  end 

  protected
    def cambiar_mayusculas
      self.cod_consultorio = cod_consultorio.upcase if !cod_consultorio.blank?
      self.nom_consultorio = nom_consultorio.upcase if !nom_consultorio.blank?
    end

end
