class CfgOperacionesOpcionesMenu < ApplicationRecord
	# Setear el nombre de la tabla
	self.table_name = "cfg_operaciones_opciones_menu"

	# Llamado a método para cambiar contenido a mayúsculas
    before_validation :cambiar_mayusculas, on: [:create, :update] 

    # Relaciones con otros modelos
	belongs_to :cfg_opciones_menu 
    
    # Validaciones de campos obligatorios    
    validates :cfg_opciones_menu_id, presence: {message: "Opción de menú: debe seleccionar el valor"}
    validates :operacion, presence: {message: "Nombre de operación: debe ingresar el valor"}

    # Retornar el valor SI / NO dependiendo del valor
	def estatus?(valor)
	      valor == true ?'SI':'NO'
	end

	protected
	    # Cambiar a mayusculas el contenido de los campos texto
	  	def cambiar_mayusculas
	      self.operacion = operacion.upcase if !operacion.blank?
	  	end

end 
 