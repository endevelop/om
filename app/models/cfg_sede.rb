class CfgSede < ApplicationRecord
  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update] 
  # Relaciones con otros modelos  
  has_many :cfg_consultorios
  belongs_to :cfg_empresa
  belongs_to :cfg_municipio, optional: true

  # Validaciones 
  validates :codigo_sede, presence:  {message: "Código sede: debe ingresar el valor"}
  validates :nombre_sede, presence:  {message: "Nombre sede: debe ingresar el valor"}
  validates :cfg_municipio_id, presence: {message: "Departamento-Municipio: debe seleccionar algún valor"}

  def esta_activo?
    self.activo == false ?'No':'Si'
  end 

  protected
    def cambiar_mayusculas
      self.codigo_sede = codigo_sede.upcase if !codigo_sede.blank?      
      self.nombre_sede = nombre_sede.upcase if !nombre_sede.blank?
      self.encargado   = encargado.upcase   if !encargado.blank?
      self.direccion   = direccion.upcase   if !direccion.blank?
    end

end
