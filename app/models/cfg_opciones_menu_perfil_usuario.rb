class CfgOpcionesMenuPerfilUsuario < ApplicationRecord

	# Relaciones con otros modelos    
	belongs_to :cfg_opciones_menu
	belongs_to :cfg_perfiles_usuario

    # Validaciones de campos obligatorios
    validates :cfg_perfiles_usuario_id, presence: {message: "Perfil de usuario: debe seleccionar el valor"}
    validates :cfg_opciones_menu_id, presence: {message: "Opción del menú: debe seleccionar el valor"}

end
