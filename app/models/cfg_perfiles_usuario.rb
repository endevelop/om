class CfgPerfilesUsuario < ApplicationRecord
  after_validation :validaciones_crear, on: [:create] 
  after_validation :validaciones_actualizar, on: [:update] 

  # Relaciones con otros modelos    
  has_many :cfg_opciones_menu_perfil_usuarios
  
  # Validaciones de campos obligatorios
  validates :nombre_perfil, presence: {message: "Nombre del perfil: debe ingresar el valor"}
  validates :activo, inclusion: { in: [true, false], message: "Debe seleccionar algún valor si / no" }
  validates :asociado_empresa, inclusion: { in: [true, false], message: "Debe seleccionar algún valor si / no" }

  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update] 

  def esta_activo?
    self.activo == false ?'No':'Si'
  end  

  def estatus?(valor)
      valor == true ?'SI':'NO'
  end

  def validaciones_crear
    # Verificar que el nombre del perfil no esté ya previamente registrado
    if !self.nombre_perfil.blank?
      nombre_perfil = self.nombre_perfil.strip    
      perfiles = CfgPerfilesUsuario.select("id").where("UPPER(nombre_perfil) = ?", nombre_perfil.upcase)
      self.errors.add("nombre_perfil", "Nombre de perfil ya existe") if perfiles.size > 0
    end
  end

  def validaciones_actualizar
    # Verificar que el nombre del perfil no esté ya previamente registrado
    if !self.nombre_perfil.blank?
      nombre_perfil = self.nombre_perfil.strip    
      perfiles = CfgPerfilesUsuario.select("id")
                 .where("UPPER(nombre_perfil) = ?", nombre_perfil.upcase)
                 .where("id <> ?", self.id)
      self.errors.add("nombre_perfil", "Nombre de perfil ya existe") if perfiles.size > 0
    end
  end

  protected
  	def cambiar_mayusculas
      self.nombre_perfil = nombre_perfil.strip.upcase if !nombre_perfil.blank?      
  	end

end
