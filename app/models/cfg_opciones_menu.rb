class CfgOpcionesMenu < ApplicationRecord
  has_many :cfg_operaciones_opciones_menu
  after_validation :validaciones_crear, on: [:create] 
  after_validation :validaciones_actualizar, on: [:update] 

  # Relaciones con otros modelos    
  has_many :cfg_opciones_menu_perfil_usuarios
  
  # Validaciones de campos obligatorios
  validates :id_opcion_padre, presence: {message: "Opción padre: debe seleccionar el valor"}
  validates :nombre_opcion, presence: {message: "Nombre de la opción: debe ingresar el valor"}
  #validates :style, presence: {message: "Estilo: debe ingresar el valor"}
  validates :url_opcion, presence: {message: "Url de la opción: debe ingresar el valor"}
  validates :orden, presence: {message: "Orden de la opción: debe ingresar el valor"}
  validates :activo, inclusion: { in: [true, false], message: "Debe seleccionar algún valor si / no" }

  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update] 

  #def esta_activo?
  #  self.activo == false ?'No':'Si'
  #end  

  def estatus?(valor)
      valor == true ?'SI':'NO'
  end

  def descripcion_opcion_padre
    descripcion = ""
    if self.id_opcion_padre == 0
      descripcion = "---"
    else  
      nivel1 = CfgOpcionesMenu.find(self.id_opcion_padre)
      if !nivel1.nil?
        nivel2 = CfgOpcionesMenu.find(nivel1.id_opcion_padre)
        if !nivel2.nil?
          if nivel2.nombre_opcion.blank?
            descripcion = nivel1.nombre_opcion.upcase
          else
            descripcion = nivel2.nombre_opcion.upcase + " - " + nivel1.nombre_opcion.upcase
          end
        else
          descripcion = nivel1.nombre_opcion.upcase
        end
      end
    end  
    return descripcion
  end

  def validaciones_crear
    # Verificar que el nombre no esté ya previamente registrado
    if !self.nombre_opcion.blank?
      nombre_opcion = self.nombre_opcion.strip  
      opciones = CfgOpcionesMenu.select("id").where("UPPER(nombre_opcion) = ?", nombre_opcion.upcase)
      self.errors.add("nombre_opcion", "Nombre de opción ya existe") if opciones.size > 0
    end
  end

  def validaciones_actualizar
    # Verificar que el nombre no esté ya previamente registrado
    if !self.nombre_opcion.blank?
      nombre_opcion = self.nombre_opcion.strip    
      opciones = CfgOpcionesMenu.select("id")
                 .where("UPPER(nombre_opcion) = ?", nombre_opcion.upcase)
                 .where("id <> ?", self.id)
      self.errors.add("nombre_opcion", "Nombre de opción ya existe") if opciones.size > 0
    end
  end

  protected
  	def cambiar_mayusculas
      self.nombre_opcion = nombre_opcion.upcase if !nombre_opcion.blank?      
  	end

end
