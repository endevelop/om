class CfgImagen < ApplicationRecord
  # Relaciones con otros modelos
  has_many :cfg_empresas
  has_many :cfg_paciente
  has_many :cfg_usuarios
  
  # Validaciones
  validate :file_size_under_one_mb

  has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200" }, :default_url => "missing.png"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  # Método para ajustar el tope del tamaño de archivo permitido en MB
  NUM_BYTES_IN_MEGABYTE = 1048576
  def file_size_under_one_mb
    if (self.image.size.to_f / NUM_BYTES_IN_MEGABYTE) > 1
      errors.add(:file, 'File size cannot be over one megabyte.')
    end
  end  

end
