class CfgItemsHorario < ApplicationRecord
  # Relaciones con otros modelos
  belongs_to :cfg_horario, optional: true
  # Constantes
  DIAS = {'LUNES' => 1,'MARTES' => 2,'MIERCOLES' => 3,'JUEVES' => 4,'VIERNES' => 5,'SABADO' => 6,'DOMINGO' => 0}
  DIAS2 = ['LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO','DOMINGO']
end
