class CfgPaciente < ApplicationRecord
  # Llamado a método para cambiar contenido a mayúsculas
  before_validation :cambiar_mayusculas, on: [:create, :update]	

  # Relación con otros modelos
  belongs_to :fac_administradora, optional: true
  belongs_to :cfg_imagen, optional: true
  has_many :cit_citas

  # Validaciones
  validates :identificacion, presence: {message: "Identificación: Debe ingresar el valor"}
  validate :file_size_under_one_mb

  has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200" }, :default_url => "missing.png"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  NUM_BYTES_IN_MEGABYTE = 1048576
  def file_size_under_one_mb
    if (self.image.size.to_f / NUM_BYTES_IN_MEGABYTE) > 1
      errors.add(:file, 'File size cannot be over one megabyte.')
    end
  end  

  # Búsqueda de registros particulares
  scope :identifica, lambda {|x| where('identificacion = ?', x)}
  scope :nombre_ilike, lambda {|x| where('primer_nombre ILIKE ? OR segundo_nombre ILIKE ? OR primer_apellido ILIKE ? OR segundo_apellido ILIKE ?', "%#{x}%","%#{x}%","%#{x}%","%#{x}%")}

  # Concatenación de nombres
  def nombres
    "#{primer_nombre} #{segundo_nombre}"
  end

  # Concatenación de apellidos
  def apellidos
    "#{primer_apellido} #{segundo_apellido}"
  end

  protected
  	def cambiar_mayusculas
  	  self.identificacion = identificacion.upcase if !identificacion.blank?
  	  self.lugar_expedicion = lugar_expedicion.upcase if !lugar_expedicion.blank?
  	  self.primer_apellido = primer_apellido.upcase if !primer_apellido.blank?
  	  self.segundo_apellido = segundo_apellido.upcase if !segundo_apellido.blank?
  	  self.primer_nombre = primer_nombre.upcase if !primer_nombre.blank?
  	  self.segundo_nombre = segundo_nombre.upcase if !segundo_nombre.blank?
  	  self.barrio = barrio.upcase if !barrio.blank?
  	  self.direccion = direccion.upcase if !direccion.blank?
  	  self.numero_autorizacion = numero_autorizacion.upcase if !numero_autorizacion.blank?
  	  self.responsable = responsable.upcase if !responsable.blank?
  	  self.acompanante = acompanante.upcase if !acompanante.blank?
  	  self.carnet = carnet.upcase if !carnet.blank?
  	  self.observaciones = observaciones.upcase if !observaciones.blank?
  	end
end
