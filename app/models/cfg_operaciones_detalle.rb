class CfgOperacionesDetalle < ApplicationRecord
	# Llamado a método para cambiar contenido a mayúsculas
    #before_validation :cambiar_mayusculas, on: [:create, :update] 

    # Relaciones con otros modelos
	belongs_to :cfg_operaciones_opciones_menu

	# Validaciones de campos obligatorios    
    validates :cfg_operaciones_opciones_menu_id, presence: {message: "Operación: debe seleccionar el valor"}
    validates :controlador, presence: {message: "Nombre del controlador: debe ingresar el valor"}
    validates :accion, presence: {message: "Nombre de la acción: debe ingresar el valor"}

    # Retornar el valor SI / NO dependiendo del valor
	def estatus?(valor)
	      valor == true ?'SI':'NO'
	end

	protected
	    # Cambiar a mayusculas el contenido de los campos texto
	  	#def cambiar_mayusculas
	    #  self.operacion = operacion.upcase if !operacion.blank?
	  	#end

end
