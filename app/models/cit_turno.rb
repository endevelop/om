class CitTurno < ApplicationRecord 
	
  #has_calendar attribute: :start_time
  # Relaciones con otros modelos
  belongs_to :cfg_consultorio, optional: true
  belongs_to :cfg_horario, optional: true
  belongs_to :cfg_usuario, optional: true
  has_many :cit_citas

  def start_time
  	self.fecha
  end

  def nombre_completo
    "#{primer_nombre} #{segundo_nombre} #{primer_apellido} #{segundo_apellido}"
  end
end
