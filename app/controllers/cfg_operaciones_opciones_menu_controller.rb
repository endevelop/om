class CfgOperacionesOpcionesMenuController < ApplicationController
  before_action :set_cfg_opciones_menu
  before_action :set_cfg_operaciones_opciones_menu, only: [:show, :edit, :update, :estatus_operacion]

  # GET cfg_opciones_menu/1/cfg_operaciones_opciones_menu
  def index
    @cfg_operaciones_opciones_menu = @cfg_opciones_menu.cfg_operaciones_opciones_menu.order('operacion').page(params[:page])
  end

  # GET cfg_opciones_menu/1/cfg_operaciones_opciones_menu/1
  def show
    # Buscar el detalle de la operacion
    @detalles = CfgOperacionesDetalle.select("id, cfg_operaciones_opciones_menu_id, controlador, accion, activo")
    .order("controlador, accion")
    .where(["cfg_operaciones_opciones_menu_id = ?", @cfg_operaciones_opciones_menu.id ])
  end

  # GET cfg_opciones_menu/1/cfg_operaciones_opciones_menu/new
  def new
    #@cfg_operaciones_opciones_menu = @cfg_opciones_menu.cfg_operaciones_opciones_menu.build
    @cfg_operaciones_opciones_menu = CfgOperacionesOpcionesMenu.new
    @operaciones_detalle = CfgOperacionesDetalle.new
  end

  # GET cfg_opciones_menu/1/cfg_operaciones_opciones_menu/1/edit
  def edit
    # Obtener los detalles
    @detalles = CfgOperacionesDetalle.select("id, cfg_operaciones_opciones_menu_id, controlador, accion, activo")
          .order("controlador, accion")
          .where(["cfg_operaciones_opciones_menu_id = ?", @cfg_operaciones_opciones_menu.id ])
  end

  # POST cfg_opciones_menu/1/cfg_operaciones_opciones_menu
  def create
    begin
      @cfg_operaciones_opciones_menu = CfgOperacionesOpcionesMenu.new(cfg_operaciones_opciones_menu_params)
      # Guardar los datos en las tablas cfg_operaciones_opciones_menu y cfg_operaciones_detalle  
      CfgOperacionesOpcionesMenu.transaction do
        # Validar la cantidad de registros detalle, al menos uno
        cantidad = 0
        if !params[:controlador].nil? and params[:controlador].size > 0
          cantidad = 0
          params[:controlador].each do |c|
            if !c.blank?
              cantidad = cantidad + 1              
            end
          end          
        end
        if cantidad == 0
          @operaciones_detalle = CfgOperacionesDetalle.new
          @operaciones_detalle.errors.add(:cantidad, 'Al menos debe registrar un detalle de la operación')
          flash[:alert] = "Error al guardar la operación"
          render :new
        else
          # Crear la operacion y sus detalles
          
          @cfg_operaciones_opciones_menu.cfg_opciones_menu_id = @cfg_opciones_menu.id
          @cfg_operaciones_opciones_menu.save!
          # Crear el detalle de la operacion 
          if !params[:controlador].nil? and params[:controlador].size > 0
            i = 0
            params[:controlador].each do |c|
              if !c.blank?
                operaciones_detalle = CfgOperacionesDetalle.new
                operaciones_detalle.cfg_operaciones_opciones_menu_id = @cfg_operaciones_opciones_menu.id
                operaciones_detalle.controlador = c
                operaciones_detalle.accion = params[:accion][i]
                operaciones_detalle.activo = true
                operaciones_detalle.save!
                i = i + 1              
              end
            end
          end  
          # Redireccionar al consultar 
          redirect_to cfg_opciones_menu_cfg_operaciones_opciones_menu_path(@cfg_opciones_menu, @cfg_operaciones_opciones_menu), notice: 'La operación fue registrada satisfactoriamente'               
        end  
      end  # de la transaccion  
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la operación"
      render :new
    rescue ActiveRecord::RecordNotUnique =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la operación - id no es único"
      render :new  
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la operación"
      render :new
    end
  end

  # PUT cfg_opciones_menu/1/cfg_operaciones_opciones_menu/1
  def update
    begin
      # Guardar los datos en las tablas cfg_operaciones_opciones_menu y cfg_operaciones_detalle  
      CfgOperacionesOpcionesMenu.transaction do
        # Validar la cantidad de registros detalle, al menos uno
        cantidad = 0
        if !params[:controlador].nil? and params[:controlador].size > 0
          cantidad = 0
          params[:controlador].each do |c|
            if !c.blank?
              cantidad = cantidad + 1              
            end
          end          
        end
        if cantidad == 0
          @operaciones_detalle = CfgOperacionesDetalle.new
          @operaciones_detalle.errors.add(:cantidad, 'Al menos debe registrar un detalle de la operación')
          flash[:alert] = "Error al guardar la operación"
          render :edit
        else
          # Actualizar los datos de la operacion    
          @cfg_operaciones_opciones_menu.update_attributes!(cfg_operaciones_opciones_menu_params)
          # Borrar los detalles ya registrados
          detalle_operaciones = CfgOperacionesDetalle.select("id")
              .where(["cfg_operaciones_opciones_menu_id = ?", @cfg_operaciones_opciones_menu.id])
          detalle_operaciones.each do |d|
            d.destroy
          end
          # Crear los detalles de la operacion 
          if !params[:controlador].nil? and params[:controlador].size > 0
            i = 0
            params[:controlador].each do |c|
              if !c.blank?
                operaciones_detalle = CfgOperacionesDetalle.new
                operaciones_detalle.cfg_operaciones_opciones_menu_id = @cfg_operaciones_opciones_menu.id
                operaciones_detalle.controlador = c
                operaciones_detalle.accion = params[:accion][i]
                operaciones_detalle.activo = true
                operaciones_detalle.save!
                i = i + 1              
              end
            end
          end  
          # Redireccionar al index
          redirect_to cfg_opciones_menu_cfg_operaciones_opciones_menu_index_url(@cfg_opciones_menu), notice: "Se actualizó la operación satisfactoriamente"
        end  
      end  # de la transaccion  
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al editar la operación"
      render :new
    rescue ActiveRecord::RecordNotUnique =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al editar la operación - id no es único"
      render :new  
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al editar la operación"
      render :new
    end
        
  end

  # DELETE cfg_opciones_menu/1/cfg_operaciones_opciones_menu/1
  def estatus_operacion
    begin
      CfgOperacionesOpcionesMenu.transaction do
        # Inactivar los detalles de la operacion
        @detalles = CfgOperacionesDetalle.select("id, cfg_operaciones_opciones_menu_id, controlador, accion, activo")
          .order("controlador, accion")
          .where(["cfg_operaciones_opciones_menu_id = ?", @cfg_operaciones_opciones_menu.id ])
        @detalles.each do |d|
          d.update_attributes!(activo: !devolver_valor_activo(params[:activo]))
        end       
        # Inactivar la operacion
        @cfg_operaciones_opciones_menu.update_attributes!(activo: !devolver_valor_activo(params[:activo]))
        # Redireccionar al index
        redirect_to cfg_opciones_menu_cfg_operaciones_opciones_menu_index_url(@cfg_opciones_menu), notice: "Se activó / inactivó la operación satisfactoriamente"
      end # de la transaccion
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al activar / inactivar la operación"
      index
      render :index
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_opciones_menu
      @cfg_opciones_menu = CfgOpcionesMenu.find(params[:cfg_opciones_menu_id])
    end

    def set_cfg_operaciones_opciones_menu
      @cfg_operaciones_opciones_menu = @cfg_opciones_menu.cfg_operaciones_opciones_menu.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cfg_operaciones_opciones_menu_params
      params.require(:cfg_operaciones_opciones_menu).permit(:cfg_opciones_menu_id, :operacion, :activo)
    end
end
