class CfgDiasNoLaboralesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cfg_dias_no_laborales, only: [:destroy]

  def index
  	@cfg_dias_no_laborales = CfgDiasNoLaborales.select("cfg_dias_no_laborales.*, s.nombre_sede").joins("LEFT JOIN cfg_sedes s ON cfg_dias_no_laborales.cfg_sede_id = s.id").
  	order("fecha_no_laboral")
  end

  def new
  	@cfg_dias_no_laborales = CfgDiasNoLaborales.new  	
  end

  def create
  	@cfg_dias_no_laborales = CfgDiasNoLaborales.new(cfg_dias_no_laborales_params)
  	# Cuando el usuario no seleccione sede o tome la opción FERIADO NACIONAL, el valor de cfg_sede_id será cero. En caso contrario, tomará la sede seleccionada
  	@cfg_dias_no_laborales.cfg_sede_id = (@cfg_dias_no_laborales.cfg_sede_id.blank?) ? 0 : @cfg_dias_no_laborales.cfg_sede_id
  	if @cfg_dias_no_laborales.save
  	  redirect_to cfg_dias_no_laborales_index_path, notice: 'Dia No Laboral registrado satisfactoriamente'
  	else
  	  render :new
  	end
  end

  def destroy
  	if @cfg_dias_no_laborales.destroy
  	  redirect_to cfg_dias_no_laborales_index_path, notice: 'Dia No Laboral eliminado satisfactoriamente'	
  	else
  	  redirect_to cfg_dias_no_laborales_index_path, alert: 'Hay un error al intentar eliminar el Dia No Laboral seleccionado'
  	end
  end

  private
  	def cfg_dias_no_laborales_params
  	  params.require(:cfg_dias_no_laborales).permit(:cfg_sede_id, :fecha_no_laboral)
  	end

  	def set_cfg_dias_no_laborales
  	  @cfg_dias_no_laborales = CfgDiasNoLaborales.find(params[:id])
  	end
end
