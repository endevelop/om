class CfgPerfilesUsuariosController < ApplicationController
  before_action :set_cfg_perfiles_usuario, only: [:show, :edit, :update, :estatus_perfil, :asociar_opciones_menu, :guardar_asociar_opciones_menu] # , :actualizar_operaciones

  # GET /cfg_perfiles_usuarios
  # GET /cfg_perfiles_usuarios.json
  def index
    query_listar_perfiles
  end

  # Buscar usuarios segun la ventana de criterios de busqueda
  # Y la paginacion, despues de haber filtrado por algun criterio 
  # de busqueda
  def buscar
    # Ejecuta el query
    query_listar_perfiles
    # Si la accion es buscar por los criterios de busqueda, va al js
    # De lo contrario va al index    
    if !params[:commit].nil? and params[:commit].strip == "Buscar"
      render "buscar.js"
    else
      render :index
    end  
  end

  def query_listar_perfiles
    # Variables para la condicion y valores del query
    condiciones = []
    argumentos = Hash.new 
 
    # Criterio de busqueda: nombre 
    unless params[:nombre].blank?
      condiciones << 'compara_ascii(UPPER(nombre_perfil)) ILIKE compara_ascii(UPPER(:nombre))'
      argumentos[:nombre] = "%"+params[:nombre].strip+"%"
    end
    # Criterio de búsqueda: activo
    unless params[:activo].blank?
      condiciones << 'activo = :activo'
      argumentos[:activo] = params[:activo]
    end
    # Criterio de búsqueda: asociado a empresa
    unless params[:asociado_empresa].blank?
      condiciones << 'asociado_empresa = :asociado_empresa'
      argumentos[:asociado_empresa] = params[:asociado_empresa]
    end

    # Query
    @cfg_perfiles_usuarios = CfgPerfilesUsuario.select("id, UPPER(nombre_perfil) as nombre_perfil, activo, asociado_empresa, observaciones")   
    .order('nombre_perfil').page(params[:page])
    .where(condiciones.join(" AND "),argumentos)

  end

  # GET /cfg_perfiles_usuarios/1
  # GET /cfg_perfiles_usuarios/1.json
  def show
    @readonly = true
  end

  # GET /cfg_perfiles_usuarios/new
  def new
    @cfg_perfiles_usuario = CfgPerfilesUsuario.new
  end

  # POST /cfg_perfiles_usuarios
  # POST /cfg_perfiles_usuarios.json
  def create
    begin
      @cfg_perfiles_usuario = CfgPerfilesUsuario.new(cfg_perfiles_usuario_params)  
      @cfg_perfiles_usuario.save!
      # Redireccionar al consultar empresa
      redirect_to @cfg_perfiles_usuario, notice: 'El perfil de usuario fue creado satisfactoriamente'               
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar el perfil de usuario"
      render :new
    rescue ActiveRecord::RecordNotUnique =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar el perfil de usuario - id no es único"
      render :new  
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar el perfil de usuario"
      render :new
    end
  end

  # GET /cfg_perfiles_usuarios/1/edit
  def edit
  end

  # PATCH/PUT /cfg_perfiles_usuarios/1
  # PATCH/PUT /cfg_perfiles_usuarios/1.json
  def update
    begin
      # Actualizar información del perfil del usuario
      @cfg_perfiles_usuario.update_attributes!(cfg_perfiles_usuario_params)
      # Redireccionar al consultar 
      redirect_to @cfg_perfiles_usuario, notice: 'El perfil de usuario fue actualizado satisfactoriamente'         
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar el perfil de usuario - registro inválido"
      render :edit
    rescue ActiveRecord::RecordNotUnique =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar el perfil de usuario - id no es único"
      render :edit    
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      render :edit      
    end
  end

  # METODO QUE ACTUALIZA EL ESTATUS ACTIVO DEL PERFIL
  def estatus_perfil
    if @cfg_perfiles_usuario.update(activo: !devolver_valor_activo(params[:activo]))
      flash[:notice] = "Estatus del perfil de usuario fue actualizado satisfactoriamente"      
    else
      flash[:alert] = "Estatus del perfil de usuario no pudo ser actualizado"  
      # Mostrar junto al mensaje de alert, los errores del modelo
      @cfg_perfiles_usuario.errors.each do |campo,msg| 
        flash[:alert] = flash[:alert]  + "<BR>"
        flash[:alert] = flash[:alert]  + msg 
      end
    end
    redirect_to cfg_perfiles_usuarios_url  
  end 
 
  def asociar_opciones_menu
    @opcion_id = nil
    @perfil_id = ""
    @nombre_perfil = ""
    @nombre_opcion = ""
    @opciones_marcadas_perfil = Array.new
    opciones_marcadas_perfil = CfgOpcionesMenuPerfilUsuario.select("cfg_opciones_menu_id")
      .where("cfg_perfiles_usuario_id = ?", @cfg_perfiles_usuario.id)
    if !opciones_marcadas_perfil.nil?  and opciones_marcadas_perfil.size > 0
      opciones_marcadas_perfil.each do |opcion|
        @opciones_marcadas_perfil << opcion.cfg_opciones_menu_id
      end
    end
  end

  def guardar_asociar_opciones_menu
    guardo = 0
    flash[:alert] = ""
    #"opcion_id"=>"29", "marcado"=>"0"
    opcion_id = params[:opcion_id]
    marcado   = params[:marcado]
    begin
    CfgOpcionesMenuPerfilUsuario.transaction do
    registro = CfgOpcionesMenuPerfilUsuario.find_by("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu_id = ?",
      @cfg_perfiles_usuario.id, opcion_id)
    if registro.nil? # No existe 
      if marcado == "0"    # No se realiza ningun cambio en bd
      elsif marcado == "1" # Se debe crear en la tabla
         cfg = CfgOpcionesMenuPerfilUsuario.new
         cfg.cfg_perfiles_usuario_id = @cfg_perfiles_usuario.id
         cfg.cfg_opciones_menu_id = opcion_id
         cfg.save!
         guardo = 1
         @mensaje  = "La opción de menú fue asociada satisfactoriamente"
         # Buscar el id de la opcion superior de opcion_id
         opcion_sup1 = CfgOpcionesMenu.find(opcion_id)
         if !opcion_sup1.nil?
          puts "Opcion superior id: ", opcion_sup1.id_opcion_padre
          # Crear la opcion superior para que aparezca en el menu,
          # si ya no esta registrada para el perfil
          cfg_opc_menusup_perfil = CfgOpcionesMenuPerfilUsuario.find_by("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu_id = ?",
          @cfg_perfiles_usuario.id, opcion_sup1.id_opcion_padre)
          if cfg_opc_menusup_perfil.nil?
            puts "No esta registrado el superior...."
            cfg2 = CfgOpcionesMenuPerfilUsuario.new
            cfg2.cfg_perfiles_usuario_id = @cfg_perfiles_usuario.id
            cfg2.cfg_opciones_menu_id = opcion_sup1.id_opcion_padre
            cfg2.save!
            # Buscar el id de la opcion padre de la opcion superior
            opcion_sup2 = CfgOpcionesMenu.find(opcion_sup1.id_opcion_padre)
            if !opcion_sup2.nil?
              puts "Opcion superior2 id: ", opcion_sup2.id_opcion_padre
              # Crear la opcion superior para que aparezca en el menu,
              # si ya no esta registrada para el perfil
              cfg_opc_menusup_perfil2 = CfgOpcionesMenuPerfilUsuario.find_by("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu_id = ?",
              @cfg_perfiles_usuario.id, opcion_sup2.id_opcion_padre)
              if cfg_opc_menusup_perfil2.nil?
                puts "No esta registrado el padre...."
                cfg3 = CfgOpcionesMenuPerfilUsuario.new
                cfg3.cfg_perfiles_usuario_id = @cfg_perfiles_usuario.id
                cfg3.cfg_opciones_menu_id = opcion_sup2.id_opcion_padre
                cfg3.save!
              else
                puts "Si esta registrado el padre...."
              end    
            end
          else
            puts "Si esta registrado el superior...."
          end  
          
         end
      end          
    else # Si existe el registro
      if marcado == "0"    # Se debe eliminar de la tabla
         registro.destroy
         @mensaje = "La opción de menú fue eliminada satisfactoriamente"
         # Buscar la opcion superior de opcion_id para removerla
         # en caso que no tenga opciones hijas asociadas al perfil
         opcion_sup1 = CfgOpcionesMenu.find(opcion_id)
         if !opcion_sup1.nil?
          puts "Opcion superior id: ", opcion_sup1.id_opcion_padre
          # Buscar las opciones hijas en el perfil
          opciones_superior = CfgOpcionesMenuPerfilUsuario.select("cfg_opciones_menu.id").
            left_outer_joins(:cfg_opciones_menu).
            where("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu.id_opcion_padre = ? AND cfg_opciones_menu.activo IS TRUE", 
              @cfg_perfiles_usuario.id, opcion_sup1.id_opcion_padre)
          if opciones_superior.size == 0 # No tiene opciones más opciones asociadas al perfil, se elimina
            registro1 = CfgOpcionesMenuPerfilUsuario.find_by("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu_id = ?",
              @cfg_perfiles_usuario.id, opcion_sup1.id_opcion_padre)
            registro1.destroy
            # Buscar la opcion superior de opcion_sup1.id_opcion_padre para removerla
            # en caso que no tenga opciones hijas asociadas al perfil
            opcion_sup2 = CfgOpcionesMenu.find(opcion_sup1.id_opcion_padre)
            if !opcion_sup2.nil?
              puts "Opcion superior id: ", opcion_sup2.id_opcion_padre
              # Buscar las opciones hijas en el perfil
              opciones_superior2 = CfgOpcionesMenuPerfilUsuario.select("cfg_opciones_menu.id").
                left_outer_joins(:cfg_opciones_menu).
                where("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu.id_opcion_padre = ? AND cfg_opciones_menu.activo IS TRUE", 
                  @cfg_perfiles_usuario.id, opcion_sup2.id_opcion_padre)
              if opciones_superior2.size == 0 # No tiene opciones más opciones asociadas al perfil, se elimina
                registro2 = CfgOpcionesMenuPerfilUsuario.find_by("cfg_perfiles_usuario_id = ? AND cfg_opciones_menu_id = ?",
              @cfg_perfiles_usuario.id, opcion_sup2.id_opcion_padre)
                registro2.destroy
              end  
            end

          end 
         end

         
      elsif marcado == "1" # No se realiza ningun cambio en bd         
      end  
    end
    end # Fin de la transaccion 
    rescue => exception
        imprimir_mensajes_excepcion(exception)
        flash[:alert] = "Error al guardar la opción"
      end  
  end

  
  def actualizar_operaciones
    if !params[:opcion_id].blank?
      @opcion_id = params[:opcion_id]
      opcion = CfgOpcionesMenu.find(@opcion_id)
      @nombre_opcion = opcion.nombre_opcion if !opcion.nil?
    end 
    @nombre_perfil = params[:nombre_perfil]
    @perfil_id = params[:perfil_id]
  end

  def guardar_operacion_opcion
    @mensaje = ""
    opcion_menu_id = params[:opcion_menu_id]
    @opcion_id = opcion_menu_id
    operacion_id = params[:operacion_id]
    perfil_id = params[:perfil_id]
    @perfil_id = perfil_id
    marcado   = params[:marcado]
    begin
      CfgOperacionesPerfil.transaction do
        registro = CfgOperacionesPerfil.find_by("cfg_perfiles_usuario_id = ? AND cfg_operaciones_opciones_menu_id = ?",
          perfil_id, operacion_id)
        if registro.nil? # No existe 
          if marcado == "0"    # No se realiza ningun cambio en bd
          elsif marcado == "1" # Se debe crear en la tabla
             cfg_op_perfil = CfgOperacionesPerfil.new
             cfg_op_perfil.cfg_perfiles_usuario_id = perfil_id
             cfg_op_perfil.cfg_operaciones_opciones_menu_id = operacion_id
             cfg_op_perfil.save!
             @mensaje = "La operación fue actualizada satisfactoriamente"
          end          
        else # Si existe el registro
          if marcado == "0"    # Se debe eliminar de la tabla
            registro.destroy         
            @mensaje = "La operación fue actualizada satisfactoriamente"
          elsif marcado == "1" # No se realiza ningun cambio en bd         
          end  
        end
      end # Fin de la transaccion 
    rescue => exception
        imprimir_mensajes_excepcion(exception)
        @mensaje = "Error al guardar la operación"
    end
    # Se llama al js.erb  
  end

  def guardar_todas_operacion_opcion
    @mensaje = ""
    opcion_menu_id = params[:opcion_menu_id]
    @opcion_id = opcion_menu_id
    operaciones_id = params[:operaciones_id] 
    perfil_id = params[:perfil_id]
    @perfil_id = perfil_id
    marcado   = params[:marcado]

    begin
      CfgOperacionesPerfil.transaction do
        # Recorrer el arreglo de operaciones
        operaciones_id.each do |operacion_id|
          # Verificar si la operación está registrada para el perfil de usuario
          registro = CfgOperacionesPerfil.find_by("cfg_perfiles_usuario_id = ? AND cfg_operaciones_opciones_menu_id in (?)", 
          perfil_id, operacion_id)
          if registro.nil? # No existe 
            if marcado == "0" # No se realiza ningun cambio en bd
            elsif marcado == "1" # Se debe crear en la tabla
              cfg_op_perfil = CfgOperacionesPerfil.new
              cfg_op_perfil.cfg_perfiles_usuario_id = perfil_id
              cfg_op_perfil.cfg_operaciones_opciones_menu_id = operacion_id
              cfg_op_perfil.save!
              @mensaje = "La operaciones fueron actualizadas satisfactoriamente"
            end  
          else # Si existe el registro
            if marcado == "0"    # Se debe eliminar de la tabla
               registro.destroy     
               @mensaje = "La operaciones fueron actualizadas satisfactoriamente"
            elsif marcado == "1" # No se realiza ningun cambio en bd         
            end  
          end  
        end
      end # Fin de la transaccion 
    rescue => exception
        imprimir_mensajes_excepcion(exception)
        @mensaje = "Error al guardar las operaciones"
    end
    # Se llama al js.erb 
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_perfiles_usuario
      @cfg_perfiles_usuario = CfgPerfilesUsuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cfg_perfiles_usuario_params
      params.require(:cfg_perfiles_usuario).permit(:nombre_perfil, :activo, :asociado_empresa, :observaciones)
    end
end
