class CfgConsultoriosController < ApplicationController
  before_action :set_cfg_consultorios
  before_action :set_cfg_consultorio, only: [:show, :edit, :update, :destroy, :estatus_consultorio]

  # GET cfg_sedes/1/cfg_consultorios
  def index
    #@cfg_consultorios = @cfg_sede.cfg_consultorios
    #@cfg_consultorios = CfgConsultorio.select("cfg_consultorios.id, cod_consultorio, nom_consultorio, piso_consultorio, cfg_clasificaciones.descripcion AS descripcion_especialidad, cfg_consultorios.cfg_sede_id").joins(:cfg_clasificacion).where(activo: true, cfg_sede_id: @cfg_sede.id).order('nom_consultorio')
  end

  # GET cfg_sedes/1/cfg_consultorios/1
  def show
  end

  # GET cfg_sedes/1/cfg_consultorios/new
  def new
    @cfg_consultorio = @cfg_sede.cfg_consultorios.build
  end

  # GET cfg_sedes/1/cfg_consultorios/1/edit
  def edit
  end

  # POST cfg_sedes/1/cfg_consultorios
  def create
    @cfg_consultorio = @cfg_sede.cfg_consultorios.build(cfg_consultorio_params)

    if @cfg_consultorio.save
      redirect_to(cfg_empresa_cfg_sede_cfg_consultorio_path(@cfg_empresa, @cfg_sede, @cfg_consultorio), notice: 'Datos del consultorio creados satisfactoriamente.')
    else
      render action: 'new'
    end
  end

  # PUT cfg_sedes/1/cfg_consultorios/1
  def update
    if @cfg_consultorio.update_attributes(cfg_consultorio_params)
      redirect_to(cfg_empresa_cfg_sede_cfg_consultorio_path(@cfg_empresa, @cfg_sede, @cfg_consultorio), notice: 'Datos del consultorio actualizados satisfactoriamente.')
      #redirect_to([@cfg_consultorio.cfg_sede, @cfg_consultorio], notice: 'Datos del consultorio actualizados satisfactoriamente.')
    else
      render action: 'edit'
    end
  end

  # DELETE cfg_sedes/1/cfg_consultorios/1
  def destroy
    @cfg_consultorio.update(activo: false)

    redirect_to cfg_empresa_cfg_sede_cfg_consultorios_url(@cfg_empresa,@cfg_sede)
  end

  def estatus_consultorio
    @cfg_consultorio.update(activo: !devolver_valor_activo(params[:activo]))
    respond_to do |format|
      format.html { redirect_to cfg_empresa_cfg_sede_cfg_consultorios_url(@cfg_empresa,@cfg_sede), notice: 'Estatus del Consultorio actualizado.' }
      format.json { head :no_content }
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_consultorios
      #@cfg_sede    = CfgSede.where(id: params[:cfg_sede_id], activo: true).first
      #@cfg_empresa = CfgEmpresa.where(id: params[:cfg_empresa_id], activo: true).first
      @cfg_sede    = CfgSede.find(params[:cfg_sede_id])
      @cfg_empresa = CfgEmpresa.find(params[:cfg_empresa_id])
    end

    def set_cfg_consultorio
      #@cfg_consultorio = @cfg_sede.cfg_consultorios.find(params[:id])
      #@cfg_consultorio = CfgConsultorio.where(id: params[:id], activo: true).first
      @cfg_consultorio = CfgConsultorio.find(params[:id]) 
    end

    # Only allow a trusted parameter "white list" through.
    def cfg_consultorio_params
      params.require(:cfg_consultorio).permit(:cod_consultorio, :nom_consultorio, :piso_consultorio, :cod_especialidad, :is_consultorio_urgencia, :cfg_sede_id)
    end
end
