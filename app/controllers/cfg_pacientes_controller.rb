class CfgPacientesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cfg_paciente, only: [:show, :edit, :update, :destroy]
  protect_from_forgery except: ['buscar_paciente','buscar_pacientes']

  # GET /cfg_pacientes
  # GET /cfg_pacientes.json
  def index
    @cfg_pacientes = CfgPaciente.all
    #@cfg_paciente = CfgPaciente.new    
  end

  # GET /cfg_pacientes/1
  # GET /cfg_pacientes/1.json
  def show
  end

  # GET /cfg_pacientes/new
  def new    
    @cfg_paciente = CfgPaciente.new
  end

  # GET /cfg_pacientes/1/edit
  def edit
  end

  # POST /cfg_pacientes
  # POST /cfg_pacientes.json
  def create
    @cfg_paciente = CfgPaciente.new(cfg_paciente_params)

    respond_to do |format|
      if @cfg_paciente.save
        format.html { redirect_to @cfg_paciente, notice: 'Cfg paciente was successfully created.' }
        format.json { render :show, status: :created, location: @cfg_paciente }
      else
        format.html { render :new }
        format.json { render json: @cfg_paciente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cfg_pacientes/1
  # PATCH/PUT /cfg_pacientes/1.json
  def update
    respond_to do |format|
      if @cfg_paciente.update(cfg_paciente_params)
        format.html { redirect_to @cfg_paciente, notice: 'Cfg paciente was successfully updated.' }
        format.json { render :show, status: :ok, location: @cfg_paciente }
      else
        format.html { render :edit }
        format.json { render json: @cfg_paciente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cfg_pacientes/1
  # DELETE /cfg_pacientes/1.json
  def destroy
    @cfg_paciente.destroy
    respond_to do |format|
      format.html { redirect_to cfg_pacientes_url, notice: 'Cfg paciente was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Para buscar datos del paciente por diversos criterios. Deriva en un arreglo de cfg_pacientes en un javascript
  def buscar_pacientes   
    # Variable para incluir los criterios de búsqueda que están en el modal, si el usuario los suministró por la aplicación
    @cfg_pacientes = nil
    if !params[:identificacion].blank? or !params[:nombres].blank?
      cfg_paciente = CfgPaciente
      cfg_paciente = cfg_paciente.identifica(params[:identificacion]) unless params[:identificacion].blank?
      cfg_paciente = cfg_paciente.nombre_ilike(params[:nombres]) unless params[:nombres].blank?
      @cfg_pacientes = cfg_paciente
    end    
  end

  # Buscar registro de paciente bajo un criterio único (identificacion)
  def buscar_paciente        
    @cfg_paciente = CfgPaciente.find_by(identificacion: params[:identificacion])    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_paciente
      @cfg_paciente = CfgPaciente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cfg_paciente_params
      params.require(:cfg_paciente).permit(:identificacion, :tipo_identificacion, :lugar_expedicion, :genero, :fecha_nacimiento, :primer_apellido, :segundo_apellido, :primer_nombre, :segundo_nombre, :zona, :regimen, :nivel, :activo, :foto, :firma, :estado_civil, :grupo_sanguineo, :etnia, :escolaridad, :ocupacion, :direccion, :telefono_residencia, :telefono_oficina, :celular, :tipo_afiliado, :fecha_afiliacion, :fecha_sisben, :carnet, :cfg_departamento_id, :cfg_municipio_id, :barrio, :responsable, :acompanante, :telefono_acompanante, :historia, :fechainsc, :facturarlo, :dircatastrofe, :municipiocatastrofe, :departamentocatastrofe, :zonacatastrofe, :fechacatastrofe, :fechaop, :grado, :parentesco, :pensionado, :fac_administradora_id, :dep_nacimiento, :mun_nacimiento, :email, :categoria_paciente, :numero_autorizacion, :telefono_responsable, :fecha_vence_carnet, :observaciones, :id_discapacidad, :id_gestacion, :poblacion_lbgt, :desplazado, :id_religion, :victima_maltrato, :victima_conflicto_armado, :id_contrato, :parentesco_a)
    end
end
