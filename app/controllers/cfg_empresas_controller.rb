class CfgEmpresasController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cfg_empresa, only: [:show, :edit, :update, :destroy, :estatus_empresa]

  # GET /cfg_empresas
  # GET /cfg_empresas.json
  def index
    @cfg_empresas = CfgEmpresa.all.order("razon_social").page(params[:page])
  end

  # GET /cfg_empresas/1
  # GET /cfg_empresas/1.json
  def show
    #send_data(@cfg_empresa.file_contents, type: @cfg_empresa.content_type, filename: @cfg_empresa.filename, :disposition => 'inline')    
  end

  # GET /cfg_empresas/new
  def new
    @cfg_empresa = CfgEmpresa.new    
  end

  # GET /cfg_empresas/1/edit
  def edit
    
  end

  # POST /cfg_empresas
  # POST /cfg_empresas.json
  def create   
    begin
      @cfg_empresa = CfgEmpresa.new(cfg_empresa_params)      
      @cfg_empresa.transaction do
        # Guardar información relacionada a la imagen en tabla maestra
        if !cfg_imagen_params.nil?
          @cfg_imagen = CfgImagen.new(cfg_imagen_params)
          @cfg_imagen.save! 
          # Atributo vinculado
          @cfg_empresa.cfg_imagen_id = @cfg_imagen.id  
        end        
             
        # Información vinculada a la empresa
        @cfg_empresa.save!
        # Crear automaticamente una sede
        @cfg_sede = CfgSede.new(codigo_sede: 1,  cfg_empresa_id: @cfg_empresa.id, nombre_sede: "SEDE PRINCIPAL", cfg_municipio_id: @cfg_empresa.cfg_municipio_id)
        @cfg_sede.save!
        # Redireccionar al consultar empresa
        redirect_to @cfg_empresa, notice: 'La empresa fue creada satisfactoriamente'           
        #end
      end    
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      render :new
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      render :new
    end
  end        

  def create_ORIGINAL   
      @cfg_empresa = CfgEmpresa.new(cfg_empresa_params)
      respond_to do |format|
        if @cfg_empresa.save
          # Crear automaticamente una sede
          @cfg_sede = CfgSede.new(codigo_sede: 1, nombre_sede: "SEDE PRINCIPAL", cfg_empresa_id: @cfg_empresa.id, cfg_municipio_id: @cfg_empresa.cfg_municipio_id)
          if @cfg_sede.save
            # Crear automaticamente un consultorio
            #@cfg_consultorio = CfgConsultorio.new(cod_consultorio: , nom_consultorio: , piso_consultorio: , cod_especialidad: , cfg_sede_id: )
            #if @cfg_consultorio.save            
              format.html { redirect_to @cfg_empresa, notice: 'La empresa fue creada satisfactoriamente' }
              format.json { render :show, status: :created, location: @cfg_empresa }
            else              
              format.html { render :new }
              format.json { render json: @cfg_empresa.errors, status: :unprocessable_entity }          
            #end  
          end
        else
          format.html { render :new }
          format.json { render json: @cfg_empresa.errors, status: :unprocessable_entity }            
        end
      end 
  end

  # PATCH/PUT /cfg_empresas/1
  # PATCH/PUT /cfg_empresas/1.json
  def update
    begin
      @cfg_empresa.transaction do
        # Guardar información relacionada a la imagen en tabla maestra
        if !cfg_imagen_params.nil? and @cfg_empresa.cfg_imagen.nil?
          @cfg_imagen = CfgImagen.new(cfg_imagen_params)
          @cfg_imagen.save! 
          # Atributo vinculado
          @cfg_empresa.cfg_imagen_id = @cfg_imagen.id  
        elsif !cfg_imagen_params.nil? and !@cfg_empresa.cfg_imagen.nil?
          @cfg_imagen.update(cfg_imagen_params)  
        end 
        # Actualizar información de empresa
        @cfg_empresa.update(cfg_empresa_params)
        # Redireccionar al consultar empresa
        redirect_to @cfg_empresa, notice: 'La empresa fue actualizada satisfactoriamente'         
      end
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      render :edit
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      render :edit      
    end
    #respond_to do |format|
    #  if @cfg_empresa.update(cfg_empresa_params)
    #    @cfg_imagen.update(cfg_imagen_params) if !cfg_imagen_params.nil?
    #    format.html { redirect_to @cfg_empresa, notice: 'Datos de la empresa actualizados satisfactoriamente.' }
    #    format.json { render :show, status: :ok, location: @cfg_empresa }
    #  else
    #    format.html { render :edit }
    #    format.json { render json: @cfg_empresa.errors, status: :unprocessable_entity }
    #  end
    #end
  end

  # DELETE /cfg_empresas/1
  # DELETE /cfg_empresas/1.json
  #def destroy
  #  #@cfg_empresa.destroy
  #  @cfg_empresa.update(activo: false)
  #  respond_to do |format|
  #    format.html { redirect_to cfg_empresas_url, notice: 'Empresa inactiva.' }
  #    format.json { head :no_content }
  #  end
  #end

  def estatus_empresa
    @cfg_empresa.update(activo: !devolver_valor_activo(params[:activo]))
    respond_to do |format|
      format.html { redirect_to cfg_empresas_url, notice: 'Estatus de la Empresa actualizado.' }
      format.json { head :no_content }
    end    
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_empresa
      @cfg_empresa = CfgEmpresa.find(params[:id])
      @cfg_imagen = @cfg_empresa.cfg_imagen
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cfg_empresa_params
      params.require(:cfg_empresa).permit(:cod_departamento, :cod_municipio, :tipo_doc, :num_identificacion, :dv, :razon_social, 
        :tipo_doc_rep_legal, :num_doc_rep_legal, :nom_rep_legal, :direccion, :telefono_1, :telefono_2, :website, :logo, 
        :observaciones, :codigo_empresa, :regimen, :razon_rip, :nivel, :cfg_municipio_id, :image, :file)
    end
    def cfg_imagen_params
      params[:cfg_imagen].permit(:image, :file) if !params[:cfg_imagen].nil?
    end
end
