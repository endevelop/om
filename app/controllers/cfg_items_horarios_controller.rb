class CfgItemsHorariosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cfg_items_horario, only: [:destroy]
  before_action :set_cfg_horario

  def destroy
  	@cfg_items_horario.destroy
  	flash[:notice] = 'Item relacionado al horario eliminado satisfactoriamente'    
  end

  private
    def set_cfg_items_horario
      @cfg_items_horario = CfgItemsHorario.find(params[:id])
    end

    def set_cfg_horario
      @cfg_horario = CfgHorario.find(@cfg_items_horario.cfg_horario_id)
      @horarios = CfgItemsHorario.where(cfg_horario_id: @cfg_horario).order("dia")
    end
end
