class CitCitasController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cit_turno, only: [:new, :create]

  def index
    @cit_citas = CitCita.joins(:cfg_paciente, :cit_turno).where("cit_turnos.fecha >= current_date")
  end

  def new
    @cit_cita = CitCita.new
    @cit_cita.tipo_cita = 475 # Valor por defecto
  end

  def create  	
  	begin
  	  @cit_cita = CitCita.new(cit_cita_params)
  	  # Buscar usuario prestador de acuerdo al turno seleccionado
  	  #@event = CitTurno.find(@cit_cita.cit_turno_id)
  	  @cit_cita.cfg_usuario_id = @event.cfg_usuario_id
  	  @cit_cita.save!
  	  flash[:notice] = 'La cita fue registrada satisfactoriamente'
      redirect_to cit_turno_cit_citas_path(@event)
    rescue ActiveRecord::RecordInvalid => exception
      imprimir_mensajes_excepcion(exception) 
      render :new     
    rescue ActiveRecord::RecordNotUnique => exception      
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = exception.message 
      render :new       
    rescue => exception
      imprimir_mensajes_excepcion(exception)      
      render :new
    end 
  end

  private
    def cit_cita_params
      params.require(:cit_cita).permit(:cit_turno_id, :cfg_paciente_id, :fac_servicio_id, :tipo_cita)
    end

    def set_cit_turno
      @event = CitTurno.find(params[:cit_turno_id])
    end

end
