# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  #layout "layout_sin_menu"
  layout "layout_login"# => , :only => ["new"]
  before_action :configure_sign_in_params, only: [:create]
  skip_before_action :verificar_autorizacion

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
   def create
     super
     #redirect_to bienvenida_index_path
   end

  # DELETE /resource/sign_out
  def destroy
     super
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
   def configure_sign_in_params
     #devise_parameter_sanitizer.permit(:username) #keys: [:attribute]
     added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
   end

   def after_sign_in_path_for(resource_or_scope)   
    bienvenida_index_url
  end

  def after_sign_in_path_for(resource)
    # Obtener la informacion adicional del usuario
    cfg_usuario = current_user.cfg_usuario
    session[:nombre_usuario] = cfg_usuario.primer_nombre + " " + cfg_usuario.primer_apellido
    cfg_perfiles_usuario = cfg_usuario.cfg_perfiles_usuario
    session[:perfil_usuario_id] = cfg_usuario.cfg_perfiles_usuario_id
    session[:perfil_usuario] = cfg_perfiles_usuario.nombre_perfil
    # Si el perfil de usuario esta asociado a empresa-sede
    if cfg_perfiles_usuario.asociado_empresa == true
      session[:usuario_asociado_empresa] = true
      usuario_sede = cfg_usuario.cfg_sede
      if !usuario_sede.nil?
        session[:sede_id]      = usuario_sede.id 
        session[:sede_nombre]  = usuario_sede.nombre_sede 
        usuario_empresa = usuario_sede.cfg_empresa
        if !usuario_empresa.nil?
           session[:empresa_id]     = usuario_empresa.id
           session[:empresa_nombre] = usuario_empresa.razon_social
        end
      end
    end
    # Setear los perfiles de usuario asociados a empresas
    session[:perfiles_asociados_empresa] = []
    perfiles = CfgPerfilesUsuario.select("id")
               .where(asociado_empresa: :true)
               .where(activo: :true)
    perfiles.each do |perfil|
       session[:perfiles_asociados_empresa] << perfil.id
    end
    # Setear la variable de sesion para el manejo de mostrar 
    # el menu de opciones del usuario 
    session[:muestra_menu] = false
    # Setear los controladores y acciones a los cuales tiene permiso
    # el perfil del usuario
    setear_permisos(cfg_usuario.cfg_perfiles_usuario_id) 
    # Redireccionar a la pagina principal del sistema
    return root_path
  end

  def setear_permisos(perfil_id)
    controladores = Array.new
    operaciones = CfgOperacionesPerfil.select("controlador, accion")
    .joins("LEFT JOIN cfg_operaciones_opciones_menu 
              ON cfg_operaciones_perfiles.cfg_operaciones_opciones_menu_id = cfg_operaciones_opciones_menu.id 
            LEFT JOIN cfg_operaciones_detalle 
              ON cfg_operaciones_opciones_menu.id = cfg_operaciones_detalle.cfg_operaciones_opciones_menu_id")
    .where("cfg_perfiles_usuario_id = ?", perfil_id).order("controlador")

    nombre = ""
    controlador = {}
    operaciones.each do |o|
      if nombre != o.controlador
        controlador[o.controlador] = Array.new
        controlador[o.controlador] << o.accion
        #controladores << o.controlador if !controladores.include?(o.controlador)
        #controladores
      else
        controlador[o.controlador] << o.accion
      end  
      nombre = o.controlador
    end
    session[:permisos] = controlador
    #
    session[:permisos].each do |controlador, acciones|
      puts "-----"
      puts controlador 
      puts acciones
      puts "-----"
    end

    #    
  end

  

end
