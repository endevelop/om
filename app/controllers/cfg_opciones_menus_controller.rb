class CfgOpcionesMenusController < ApplicationController
  before_action :set_cfg_opciones_menu, only: [:show, :edit, :update, :destroy, :estatus_opcion]

  # GET /cfg_opciones_menu
  # GET /cfg_opciones_menu.json
  def index
    query_listar_opciones
  end

  # Buscar segun la ventana de criterios de busqueda
  # Y la paginacion, despues de haber filtrado por algun criterio 
  # de busqueda
  def buscar
    # Ejecuta el query
    query_listar_opciones
    # Si la accion es buscar por los criterios de busqueda, va al js
    # De lo contrario va al index    
    if !params[:commit].nil? and params[:commit].strip == "Buscar"
      render "buscar.js"
    else
      render :index
    end  
  end

  def query_listar_opciones
    # Variables para la condicion y valores del query
    condiciones = []
    argumentos = Hash.new 

    # Criterio de busqueda: nombre 
    unless params[:nombre].blank?
      condiciones << 'compara_ascii(UPPER(cfg_opciones_menu.nombre_opcion)) ILIKE compara_ascii(UPPER(:nombre))'
      argumentos[:nombre] = "%"+params[:nombre].strip+"%"
    end
    # Criterio de busqueda: nombre_padre
    unless params[:nombre_padre].blank?
      #compara_ascii(UPPER(cfg_opciones_menu.nombre_opcion)) ILIKE compara_ascii(UPPER(:nombre_padre))                    OR 
      condiciones << 'compara_ascii(UPPER(cfg_opciones_menu_2.nombre_opcion)) ILIKE compara_ascii(UPPER(:nombre_padre))
                     OR compara_ascii(UPPER(cfg_opciones_menu_3.nombre_opcion)) ILIKE compara_ascii(UPPER(:nombre_padre))'
      argumentos[:nombre_padre] = "%"+params[:nombre_padre].strip+"%"
    end
    # Criterio de busqueda: url
    unless params[:url].blank?
      condiciones << 'compara_ascii(UPPER(cfg_opciones_menu.url_opcion)) ILIKE compara_ascii(UPPER(:url))'
      argumentos[:url] = "%"+params[:url].strip+"%"
    end
    # Criterio de búsqueda: activo
    unless params[:activo].blank?
      condiciones << 'cfg_opciones_menu.activo = :activo'
      argumentos[:activo] = params[:activo]
    end

    # Query original
    #@cfg_opciones_menu = CfgOpcionesMenu.select("id, id_opcion_padre, orden, nombre_opcion, url_opcion, activo")   
    #.order('nombre_opcion').page(params[:page])
    #.where(condiciones.join(" AND "),argumentos)

    @cfg_opciones_menu = CfgOpcionesMenu.select("cfg_opciones_menu.id, cfg_opciones_menu.id_opcion_padre, cfg_opciones_menu.orden, 
                         cfg_opciones_menu.nombre_opcion, cfg_opciones_menu.url_opcion, cfg_opciones_menu.activo,
                         cfg_opciones_menu_2.nombre_opcion AS nombre_opcion_sup1, cfg_opciones_menu_3.nombre_opcion AS nombre_opcion_sup2   ")   
    .joins("JOIN cfg_opciones_menu cfg_opciones_menu_2 ON cfg_opciones_menu.id_opcion_padre = cfg_opciones_menu_2.id
            JOIN cfg_opciones_menu cfg_opciones_menu_3 ON cfg_opciones_menu_2.id_opcion_padre = cfg_opciones_menu_3.id ")
    .order('cfg_opciones_menu.nombre_opcion').page(params[:page])
    .where(condiciones.join(" AND "),argumentos)


  end

  # GET /cfg_opciones_menu/1
  # GET /cfg_opciones_menu/1.json
  def show
  end

  # GET /cfg_opciones_menu/new
  def new
    @cfg_opciones_menu = CfgOpcionesMenu.new
  end

  # GET /cfg_opciones_menu/1/edit
  def edit
  end

  # POST /cfg_opciones_menu
  # POST /cfg_opciones_menu.json
  def create
    begin
      @cfg_opciones_menu = CfgOpcionesMenu.new(cfg_opciones_menu_params)
      @cfg_opciones_menu.save!
      # Redireccionar al consultar 
      redirect_to @cfg_opciones_menu, notice: 'La opción de menú fue creada satisfactoriamente'               
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la opción de menú"
      render :new
    rescue ActiveRecord::RecordNotUnique =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la opción de menú - id no es único"
      render :new  
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la opción de menú"
      render :new
    end
  end

  # PATCH/PUT /cfg_opciones_menu/1
  # PATCH/PUT /cfg_opciones_menu/1.json
  def update
    begin
      # Actualizar información
      @cfg_opciones_menu.update_attributes!(cfg_opciones_menu_params)
      puts "si actualizo..."
      # Redireccionar al consultar 
      redirect_to @cfg_opciones_menu, notice: 'La opción de menú fue actualizada satisfactoriamente'         
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la opción de menú - registro inválido"
      render :edit
    rescue ActiveRecord::RecordNotUnique =>  exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = "Error al guardar la opción de menú - id no es único"
      render :edit    
    rescue => exception
      imprimir_mensajes_excepcion(exception)
      render :edit      
    end
  end

  # METODO QUE ACTUALIZA EL ESTATUS ACTIVO DE LA OPCION
  def estatus_opcion
    if @cfg_opciones_menu.update(activo: !devolver_valor_activo(params[:activo]))
      flash[:notice] = "Estatus de la opción de menú fue actualizado satisfactoriamente"      
    else
      flash[:alert] = "Estatus de la opción de menú no pudo ser actualizado"  
      # Mostrar junto al mensaje de alert, los errores del modelo
      @cfg_opciones_menu.errors.each do |campo,msg| 
        flash[:alert] = flash[:alert]  + "<BR>"
        flash[:alert] = flash[:alert]  + msg 
      end
    end
    redirect_to cfg_opciones_menus_url  
  end 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_opciones_menu
      @cfg_opciones_menu = CfgOpcionesMenu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cfg_opciones_menu_params
      params.require(:cfg_opciones_menu).permit(:nombre_opcion, :id_opcion_padre, :url_opcion, :orden, :activo, :observaciones)
    end
end
