class EspecialidadesController < ApplicationController

    def index       
       @especialidades = CfgClasificacion.where(maestro: 'Especialidad').order(descripcion: :asc)
    end	

    def new
    end	

end
