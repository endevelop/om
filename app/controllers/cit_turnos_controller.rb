class CitTurnosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_dias_no_laborales, only: [:index, :buscar_agenda, :create]

  def index    
    @cit_turno = CitTurno.new
    @cit_turnos = Array.new   
    # Llamando al método que lista turnos por especialista 
    buscar_turnos
  end

  def create
    # Ubicar el numero del día junto con el horario_id 
    o = CitTurno.new(cit_turno_params)    
  	begin
  	  CitTurno.transaction do 
  	  	duracion = params[:duracion].to_i * 60         
  	  	finicial = Date.parse(params[:fecha_inicial])
  	  	ffinal = Date.parse(params[:fecha_final])
        count_horario = 0  	
        if !finicial.nil? and !ffinal.nil? and finicial <= ffinal 
          # Consultar cada fecha contra la lista de días feriados. Por cada coincidencia, se pasará por alto la misma
          feriados = CfgDiasNoLaborales.where(cfg_sede_id: 0).order("fecha_no_laboral").map{|c| c.fecha_no_laboral}
          # Iterar sobre fechas definidas en el formulario         
          (finicial .. ffinal).each do |r| 
            # Si no está en la lista de feriados, avanza 
            if !feriados.include?(r.to_date)
              horario = CfgItemsHorario.where("cfg_horario_id = ? AND dia = ?", o.cfg_horario_id, r.wday).order("cfg_horario_id, dia, hora_inicio")
              # Iterar sobre horarios definidos en la tabla maestra
              if !horario.nil?
                horario.each do |h|
                  hora_inicio = get_date_time(r, h.hora_inicio)                                
                  # Hora final menos la duración en minutos, para que no cree registro del último turno
                  hora_final = get_date_time(r, h.hora_final) - duracion                
                  (hora_inicio .. hora_final).step(duracion) do |m|                  
                    @cit_turno = CitTurno.new(cit_turno_params)
                    @cit_turno.fecha = r
                    @cit_turno.hora_ini = Time.at(m+1).localtime                  
                    @cit_turno.hora_fin = Time.at(m+duracion).localtime 
                    turno = CitTurno.where("cfg_usuario_id = ? AND fecha = ? AND 
                      (? BETWEEN hora_ini::time AND hora_fin::time OR ? BETWEEN hora_ini::time AND hora_fin::time)",
                      @cit_turno.cfg_usuario_id, @cit_turno.fecha, @cit_turno.hora_ini, @cit_turno.hora_fin)                  
                    raise ArgumentError.new("Horario dentro del rango guardado previamente. Revise y modifique") if turno.size > 0
                    @cit_turno.save!                   
                    count_horario += 1           
                  end # fin de la iteración en rango de horarios
                end # fin de la iteración de horarios             
              end # fin de la condición para consultar la coincidencia de horario           
            end # fin de la condición para consultar la coincidencia de feriado
          end # fin de la iteración de fechas
          raise ArgumentError.new("No hay registro guardado porque las fechas seleccionadas no coinciden con el horario") if count_horario == 0 
        end  	  	
  	  	flash[:notice] = 'Turnos guardados satisfactoriamente'  	  	
  	  end
  	rescue ActiveRecord::RecordInvalid => exception
  	  imprimir_mensajes_excepcion(exception)  	  
    rescue ArgumentError => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = exception.message      
    rescue => exception
      imprimir_mensajes_excepcion(exception)      
    ensure      
      @cit_turnos = CitTurno.where("cfg_usuario_id = ? AND fecha >= current_date", o.cfg_usuario_id)
      if @cit_turno and !@cit_turno.new_record?
        @cit_turno = CitTurno.new
      else
        @cit_turno = o
      end
      render :index
  	end
  end

  def get_date_time(d,t)    
    y = Time.parse(t)
    DateTime.new(d.year, d.month, d.day, y.hour, y.min, y.sec).to_i
  end

  def buscar_horarios
    @horarios = CfgItemsHorario.where(cfg_horario_id: params[:horario_id])
  end

  def buscar_agenda    
    buscar_turnos
  end

  def buscar_evento
    @event = CitTurno.find(params[:evento_id])
  end

  def search
    @cit_turnos = CitTurno.select("cit_turnos.id AS id, u.primer_nombre, u.segundo_nombre, u.primer_apellido, 
      u.segundo_apellido, c.nom_consultorio, cit_turnos.fecha, cit_turnos.hora_ini, cit_turnos.cfg_usuario_id, 
      esp.descripcion especialidad, cit_turnos.concurrencia").where("cit_turnos.fecha >= current_date")
    .joins("LEFT JOIN cfg_usuarios u ON cit_turnos.cfg_usuario_id = u.id 
      LEFT JOIN cfg_consultorios c ON cit_turnos.cfg_consultorio_id = c.id LEFT JOIN cfg_especialidades esp ON u.cfg_especialidad_id = esp.id")
      .order("cit_turnos.id, cit_turnos.fecha, cit_turnos.hora_ini").last(5)
  end

  def buscar
    h = Hash.new
    conditions = []
    conditions << "cit_turnos.fecha >= current_date"    
    unless params[:cfg_especialidad_id].blank?
      conditions << "u.cfg_especialidad_id = :cfg_especialidad_id" 
      h[:cfg_especialidad_id] = params[:cfg_especialidad_id]
    end
    unless params[:cfg_usuario_id].blank?
      conditions << "cit_turnos.cfg_usuario_id = :cfg_usuario_id" 
      h[:cfg_usuario_id] = params[:cfg_usuario_id]
    end
    unless params[:hora_inicio].blank? and params[:hora_final].blank?
      conditions << "cit_turnos.hora_ini >= :hora_inicio AND cit_turnos.hora_fin <= :hora_final"
      h[:hora_inicio] = params[:hora_inicio]
      h[:hora_final] = params[:hora_final]
    end
    unless params[:fecha_inicial].blank? and params[:fecha_final].blank?
      conditions << "cit_turnos.fecha BETWEEN :fecha_inicial AND :fecha_final"
      h[:fecha_inicial] = params[:fecha_inicial]
      h[:fecha_final] = params[:fecha_final]
    end
    @cit_turnos = CitTurno.select("cit_turnos.id AS id, u.primer_nombre, u.segundo_nombre, u.primer_apellido, 
      u.segundo_apellido, c.nom_consultorio, cit_turnos.fecha, cit_turnos.hora_ini, cit_turnos.cfg_usuario_id, 
      esp.descripcion especialidad, cit_turnos.concurrencia").joins("LEFT JOIN cfg_usuarios u ON cit_turnos.cfg_usuario_id = u.id 
      LEFT JOIN cfg_consultorios c ON cit_turnos.cfg_consultorio_id = c.id LEFT JOIN cfg_especialidades esp ON u.cfg_especialidad_id = esp.id")
      .where([conditions.join(' AND '), h].flatten).order("cit_turnos.id, cit_turnos.fecha, cit_turnos.hora_ini")    
  end

  def buscar_turnos
    # Cuando venga de actualizar el usuario
    cfg_usuario_id = nil
    if !cit_turno_params.nil?
      x = CitTurno.new(cit_turno_params)
      cfg_usuario_id = x.cfg_usuario_id
    elsif !params[:usuario_id].blank?
      cfg_usuario_id = params[:usuario_id]
    end
    @cit_turnos = CitTurno.where("cfg_usuario_id = ? AND fecha >= current_date", cfg_usuario_id)    
  end

  private
  	def cit_turno_params
  	  params.require(:cit_turno).permit(:cfg_consultorio_id, :concurrencia, :cfg_horario_id, :cfg_usuario_id) if !params[:cit_turno].nil?
  	end

    def set_dias_no_laborales
      @cfg_dias_no_laborales = CfgDiasNoLaborales.all # Atento, porque debería contemplarse en el calendario
    end
end
