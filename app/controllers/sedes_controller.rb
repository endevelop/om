class SedesController < ApplicationController
  before_action :set_CfgSede, only: [:show, :edit, :update, :destroy]

  # GET /CfgSedes
  # GET /CfgSedes.json
  def index
    @sedes = CfgSede.all
  end

  # GET /CfgSedes/1
  # GET /CfgSedes/1.json
  def show
  end

  # GET /CfgSedes/new
  def new
    @sede = CfgSede.new
  end

  # GET /CfgSedes/1/edit
  def edit
  end

  # POST /CfgSedes
  # POST /CfgSedes.json
  def create
    #puts CfgSede_params
    @sede = CfgSede.new(cfgSede_params)    

    respond_to do |format|
      if @sede.save
        format.html { redirect_to sede_path(@sede), notice: 'Sede was successfully created.' }
        format.json { render :show, status: :created, location: @CfgSede }
      else
        format.html { render :new }
        format.json { render json: @CfgSede.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /CfgSedes/1
  # PATCH/PUT /CfgSedes/1.json
  def update
    respond_to do |format|
      if @sede.update(CfgSede_params)
        format.html { redirect_to @CfgSede, notice: 'CfgSede was successfully updated.' }
        format.json { render :show, status: :ok, location: @CfgSede }
      else
        format.html { render :edit }
        format.json { render json: @CfgSede.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /CfgSedes/1
  # DELETE /CfgSedes/1.json
  def destroy
    @sede.destroy
    respond_to do |format|
      format.html { redirect_to CfgSedes_url, notice: 'CfgSede was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_CfgSede
      @sede = CfgSede.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cfgSede_params
      params.require(:cfg_sede).permit(:codigo_sede, :nombre_sede, :departamento, :municipio, :encargado, :direccion, :telefono1, :telefono2)
    end
end
