class CfgHorariosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cfg_horario, only: [:update, :destroy, :editar_horarios]

  def new
    @cfg_horario = CfgHorario.new
  end

  def create
    @cfg_horario = CfgHorario.new(cfg_horario_params)
    if @cfg_horario.save
      redirect_to cfg_horarios_path, notice: 'Datos de Horario guardados satisfactoriamente'
    else
      render :new
    end
  end

  def destroy
    begin
      @cfg_horario.destroy
      flash[:notice] = 'Horario eliminado satisfactoriamente'
    rescue ActiveRecord::DeleteRestrictionError => e
      imprimir_mensajes_excepcion(e)
      flash[:alert] = e.message
    rescue => e
      imprimir_mensajes_excepcion(e)
    ensure
      redirect_to cfg_horarios_path
    end
  end

  def index    
    @cfg_horario = CfgHorario.new
  	@cfg_horarios = CfgHorario.all
    @horarios = Array.new
  end

  def listar_horarios
  	@cfg_horarios = CfgItemsHorario.where(cfg_horario_id: params[:horario])
  end

  def create2  	
    begin      
      CfgItemsHorario.transaction do
        # Guardar la descripción del horario, para vincular posteriormente
        @cfg_horario = CfgHorario.new(cfg_horario_params)
        @cfg_horario.save!

        params[:dia].each do |k,v|          
          @cfg_items_horario = CfgItemsHorario.new(cfg_items_horario_params)          
          @cfg_items_horario.cfg_horario_id = @cfg_horario.id
          @cfg_items_horario.dia = k
          @cfg_items_horario.nombredia = CfgItemsHorario::DIAS.key(k.to_i)          
          @cfg_items_horario.save! 
        end        
        flash[:notice] = 'Horarios agregados satisfactoriamente'        
      end
    rescue ActiveRecord::RecordInvalid =>  exception
      imprimir_mensajes_excepcion(exception)      
    rescue => exception
      imprimir_mensajes_excepcion(exception)   
    ensure 
      @horarios = CfgItemsHorario.where(cfg_horario_id: @cfg_horario).order("dia")             
    end      
  end

  def update  
    begin
      CfgItemsHorario.transaction do    
        # Atributos del modelo hijo, cfg_items_horario
        cfg_items_horario = CfgItemsHorario.new(cfg_items_horario_params)
        
        # Modificar la descripción, el campo del modelo cfg_horario. Si aplica
        @cfg_horario.update(cfg_horario_params)
        
        # Iterar sobre los días seleccionados para agregar al horario existente
        if !params[:dia].nil?
          params[:dia].each do |k,v|
            @cfg_items_horario = CfgItemsHorario.new(cfg_items_horario_params) 
            c = CfgItemsHorario.where("cfg_horario_id = ? AND dia = ? AND 
            (? BETWEEN hora_inicio::time AND hora_final::time OR ? BETWEEN hora_inicio::time AND hora_final::time)",
            @cfg_horario.id, k, @cfg_items_horario.hora_inicio, @cfg_items_horario.hora_final)
            raise ArgumentError.new("Horario dentro del rango guardado previamente. Revise y modifique") if c.size > 0             
            @cfg_items_horario.cfg_horario_id = @cfg_horario.id     
            @cfg_items_horario.dia = k          
            @cfg_items_horario.nombredia = CfgItemsHorario::DIAS.key(k.to_i)          
            @cfg_items_horario.save!          
          end
        end            
        flash[:notice] = 'Horarios agregados/actualizados satisfactoriamente' 
      end
    rescue ActiveRecord::RecordInvalid => exception
      imprimir_mensajes_excepcion(exception)      
    rescue ArgumentError => exception
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = exception.message
    rescue => exception
      imprimir_mensajes_excepcion(exception)                  
    end
  end

  private
    def cfg_horario_params
      params[:cfg_horario].permit(:descripcion, :codigo) if !params[:cfg_horario].nil?
    end
    def cfg_items_horario_params
      params[:cfg_items_horario].permit(:hora_inicio, :hora_final) if !params[:cfg_items_horario].nil?
    end	
    def set_cfg_horario
      @cfg_horario = CfgHorario.find(params[:id])
      @horarios = CfgItemsHorario.where(cfg_horario_id: @cfg_horario).order("dia")
    end
end
