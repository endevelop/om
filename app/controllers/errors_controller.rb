class ErrorsController < ApplicationController
  skip_before_action :verificar_autorizacion

  # Previo llamado a la vista
  def not_found    
    render(:status => 404)
  end

  def internal_server_error
    render(:status => 500)
  end
end
