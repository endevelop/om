class CitasController < ApplicationController
  before_action :authenticate_user!
  before_action :set_dias_no_laborales, only: [:new, :buscar_agenda, :index, :create]
  before_action :set_cit_turnos, only: [:buscar_agenda, :index, :create]
  before_action :set_cita, only: [:cancelar_cita]  
  respond_to :html, :json

  def index
  	@cit_citas = CitCita.select("cit_citas.id id, cit_citas.tipo_cita, cit_citas.cancelada, cfg_pacientes.primer_nombre pprimer_nombre, cit_citas.cfg_usuario_id,
      cfg_pacientes.segundo_nombre psegundo_nombre, cfg_pacientes.primer_apellido pprimer_apellido, cfg_pacientes.segundo_apellido psegundo_apellido, 
      cfg_usuarios.primer_nombre uprimer_nombre, cfg_usuarios.segundo_nombre usegundo_nombre, cfg_usuarios.primer_apellido uprimer_apellido, 
      cfg_usuarios.segundo_apellido usegundo_apellido, cit_turnos.fecha, cit_turnos.hora_ini, fac_servicio.nombre_servicio, cfg_especialidades.descripcion especialidad")
    .joins([cfg_usuario: :cfg_especialidad], :cfg_paciente, :cit_turno, :fac_servicio).where("cit_turnos.fecha >= current_date").order("cit_turnos.fecha, 
      cit_turnos.hora_ini")
    @cit_cita = CitCita.new
  end

  def usuarios
    #u = CfgUsuario
    u = CfgUsuario.nombre_ilike(params[:search]).where("cfg_usuarios.tipo_usuario = 1774").joins(:cfg_especialidad) unless params[:search].blank?

    @u = u
    #@u = (params[:cfg_especialidad_id].blank?) ? CfgUsuario.where(tipo_usuario: 1774) : CfgUsuario.where("cfg_especialidad_id = ? AND tipo_usuario = 1774",params[:cfg_especialidad_id])
    respond_with @u
  end

  def new
    @cit_cita = CitCita.new
    @cit_cita.tipo_cita = 475 # Valor por defecto
    @cit_turnos = Array.new
  end

  def create
  	begin
  	  @cit_cita = CitCita.new(cit_cita_params)
  	  @cit_cita.save!
  	  flash[:notice] = 'La cita fue registrada satisfactoriamente'
      redirect_to citas_path
    rescue ActiveRecord::RecordInvalid => exception
      imprimir_mensajes_excepcion(exception) 
      render :new     
    rescue ActiveRecord::RecordNotUnique => exception      
      imprimir_mensajes_excepcion(exception)
      flash[:alert] = exception.message 
      render :new       
    rescue => exception
      imprimir_mensajes_excepcion(exception)      
      render :new
    end 
  end

  def destroy
    @cit_cita = CitCita.find(params[:id_cita])
    @cit_cita.cancelada = true
    @cit_cita.fecha_cancelacion = Date.today
    if @cit_cita.update(cit_cita_destroy_params)
      redirect_to citas_path, notice: 'Cita cancelada satisfactoriamente'
    else 
      redirect_to citas_path, alert: 'Cita no pudo ser cancelada. Inténtelo una vez más'
    end
  end

  private
    def set_dias_no_laborales
      @cfg_dias_no_laborales = CfgDiasNoLaborales.all # Atento, porque debería contemplarse en el calendario
    end 

    def set_cit_turnos
      cfg_usuario_id = nil
      if !cit_cita_params.nil?
        x = CitCita.new(cit_cita_params)
        cfg_usuario_id = x.cfg_usuario_id
      elsif !params[:usuario_id].blank?
        cfg_usuario_id = params[:usuario_id]
      end
      @cit_turnos = CitTurno.where("cfg_usuario_id = ? AND fecha >= current_date", cfg_usuario_id)     	      
    end

    def cit_cita_params
      params.require(:cit_cita).permit(:cit_turno_id, :cfg_paciente_id, :fac_servicio_id, :tipo_cita, :cfg_usuario_id) if !params[:cit_cita].nil?
    end 

    def cit_cita_destroy_params
      params.require(:cit_cita).permit(:motivo_cancelacion, :desc_cancelacion) if !params[:cit_cita].nil?
    end

    def set_cita
      @cit_cita = CitCita.find(params[:id])
    end   

end
