class CfgSedesController < ApplicationController
  before_action :set_cfg_sedes
  before_action :set_cfg_sede, only: [:show, :edit, :update, :destroy, :estatus_sede]

  # GET cfg_empresas/1/cfg_sedes
  def index
    #@cfg_sedes = @cfg_empresa.cfg_sedes
    #@cfg_sedes = CfgSede.select("cfg_sedes.id, cfg_sedes.codigo_sede, cfg_sedes.nombre_sede, cfg_sedes.activo, cfg_sedes.cfg_empresa_id, cfg_sedes.cfg_municipio_id, cfg_departamentos.nombre || ' - ' || cfg_municipios.nombre AS descripcion_departamento_municipio").joins(:cfg_municipio => :cfg_departamento).where(activo: true, cfg_empresa_id: @cfg_empresa.id).order('nombre_sede')
  end

  # GET cfg_empresas/1/cfg_sedes/1
  def show
  end

  # GET cfg_empresas/1/cfg_sedes/new
  def new
    @cfg_sede = @cfg_empresa.cfg_sedes.build
  end

  # GET cfg_empresas/1/cfg_sedes/1/edit
  def edit
  end

  # POST cfg_empresas/1/cfg_sedes
  def create
    @cfg_sede = @cfg_empresa.cfg_sedes.build(cfg_sede_params)
    
    if @cfg_sede.save
      redirect_to([@cfg_sede.cfg_empresa, @cfg_sede], notice: 'Datos de la sede creados satisfactoriamente.')
    else
      render action: 'new'
    end
  end

  # PUT cfg_empresas/1/cfg_sedes/1
  def update
    if @cfg_sede.update_attributes(cfg_sede_params)
      redirect_to([@cfg_sede.cfg_empresa, @cfg_sede], notice: 'Datos de la sede actualizados satisfactoriamente.')
    else
      render action: 'edit'
    end
  end

  # DELETE cfg_empresas/1/cfg_sedes/1
  def destroy
    @cfg_sede.update(activo: false)
    respond_to do |format|
      format.html { redirect_to cfg_empresa_cfg_sedes_url(@cfg_empresa), notice: 'Sede inactiva.' }
      format.json { head :no_content }
    end         
  end

  def estatus_sede
    @cfg_sede.update(activo: !devolver_valor_activo(params[:activo]))
    respond_to do |format|
      format.html { redirect_to cfg_empresa_cfg_sedes_url(@cfg_empresa), notice: 'Estatus de la Sede actualizado.' }
      format.json { head :no_content }
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_sedes
      #@cfg_empresa = CfgEmpresa.where(id: params[:cfg_empresa_id], activo: true).first      
      @cfg_empresa = CfgEmpresa.find(params[:cfg_empresa_id])
    end

    def set_cfg_sede
      #@cfg_sede = @cfg_empresa.cfg_sedes.where(id: params[:id], activo: true).first
      #@cfg_sede = @cfg_empresa.cfg_sedes.where(id: params[:id]).first
      @cfg_sede = @cfg_empresa.cfg_sedes.find(params[:id])
    end
    
    #def set_cfg_empresa
    #  @cfg_empresa = CfgEmpresa.find(params[:cfg_empresa_id])
    #end

    # Only allow a trusted parameter "white list" through.
    def cfg_sede_params
      params.require(:cfg_sede).permit(:codigo_sede, :nombre_sede, :departamento, :municipio, :encargado, :direccion, :telefono1, :telefono2, :cfg_empresa_id, :cfg_municipio_id)
    end
end
