class MyDevise::RegistrationsController < Devise::RegistrationsController
	#before_action :allowed_params
	#prepend_before_action :require_no_authentication, only: [:new, :create, :cancel]
	#before_action :authenticate_user!
    #layout 'admin'
  prepend_before_action :require_no_authentication, :only => []
  prepend_before_action :authenticate_scope!

   def new
   	if user_signed_in?
     #super
     build_resource
     # Instanciando modelo de cfg_usuarios
     @cfg_usuario = CfgUsuario.new
     @cfg_imagen = CfgImagen.new
    else
     redirect_to new_user_session_path
    end 
   end

	def create
	  begin	
	  	flash[:alert] = ""
	  #
	    @cfg_usuario = CfgUsuario.new#(params[:cfg_usuario]).
	    @cfg_usuario.perfiles_asociados_empresa = session[:perfiles_asociados_empresa]
	    # Datos obligatorios
	    @cfg_usuario.tipo_identificacion = params[:cfg_usuario][:tipo_identificacion]
	    @cfg_usuario.identificacion = params[:cfg_usuario][:identificacion]
	    @cfg_usuario.genero = params[:cfg_usuario][:genero]	    
	    @cfg_usuario.primer_nombre = params[:cfg_usuario][:primer_nombre]
	    @cfg_usuario.primer_apellido = params[:cfg_usuario][:primer_apellido]
	    @cfg_usuario.tipo_usuario = params[:cfg_usuario][:tipo_usuario]
	    @cfg_usuario.cfg_perfiles_usuario_id = params[:cfg_usuario][:cfg_perfiles_usuario_id]
	    # Demas campos
	    @cfg_usuario.segundo_nombre = params[:cfg_usuario][:segundo_nombre]
	    @cfg_usuario.segundo_apellido = params[:cfg_usuario][:segundo_apellido]
	    @cfg_usuario.cfg_municipio_id = params[:cfg_usuario][:cfg_municipio_id]
	    @cfg_usuario.direccion = params[:cfg_usuario][:direccion]
	    @cfg_usuario.telefono_residencia = params[:cfg_usuario][:telefono_residencia]
	    @cfg_usuario.telefono_oficina = params[:cfg_usuario][:telefono_oficina]
	    @cfg_usuario.celular = params[:cfg_usuario][:celular]
	    @cfg_usuario.cargo_actual = params[:cfg_usuario][:cargo_actual]
	    @cfg_usuario.observacion = params[:cfg_usuario][:observacion]	    
	    @cfg_usuario.estado_cuenta = params[:cfg_usuario][:estado_cuenta]	    
	    @cfg_usuario.visible = params[:cfg_usuario][:visible]	    
	    @cfg_usuario.mostrar_en_historias = params[:cfg_usuario][:mostrar_en_historias]
	    # Campos cuando se selecciona el tipo de usuario Prestador
	    @cfg_usuario.cfg_especialidad_id = params[:cfg_usuario][:cfg_especialidad_id]	    
	    @cfg_usuario.registro_profesional = params[:cfg_usuario][:registro_profesional]	    
	    @cfg_usuario.personal_atiende = params[:cfg_usuario][:personal_atiende]	 
	    @cfg_usuario.unidad_funcional = params[:cfg_usuario][:unidad_funcional]	  
	    @cfg_usuario.porcentaje_honorario = params[:cfg_usuario][:porcentaje_honorario]	  
	    # Campos cuando se selecciona el tipo de perfil asociado a una empresa
	    #@cfg_usuario.cfg_empresa_id = params[:cfg_usuario][:cfg_empresa_id]	   
	    @cfg_usuario.cfg_sede_id = params[:cfg_usuario][:cfg_sede_id]	   
	    #
        
        # Guardar información relacionada a la imagen en tabla maestra
        #@cfg_imagen = CfgImagen.new(cfg_imagen_params)
        
         

	  # Guardar los datos en las tablas users y cfg_usuarios 	
	  CfgUsuario.transaction do
		build_resource(sign_up_params)
	    resource.save
	    yield resource if block_given?
	    if resource.persisted?      
			# Guardar imagen de la foto en la tabla maestra cfg_imagen
        	if !cfg_imagen_params.nil?
          		@cfg_imagen = CfgImagen.new(cfg_imagen_params)
          		@cfg_imagen.save! 
          		# Atributo vinculado
          		@cfg_usuario.cfg_imagen_id = @cfg_imagen.id  
        	end    
        	# Guardar imagen de la firma en la tabla maestra cfg_imagen
            if !imagen_firma_params.nil?
          		@imagen_firma = CfgImagen.new(imagen_firma_params)
          		@imagen_firma.save! 
          		# Atributo vinculado
          		@cfg_usuario.firma_id = @imagen_firma.id  
        	end 
	    	# Usuario
            @cfg_usuario.login_usuario = resource.username
            @cfg_usuario.estado_cuenta = true
            @cfg_usuario.visible = true
            @cfg_usuario.email = resource.email
            @cfg_usuario.user_id = resource.id
        	#@cfg_usuario.cfg_imagen_id = @cfg_imagen.id  
            @cfg_usuario.save!
            flash[:notice] = "Usuario creado satisfactoriamente"
            # Redireccionar al listar usuarios
            redirect_to cfg_usuarios_path
	      #if resource.active_for_authentication?
	      #  set_flash_message! :notice, :signed_up
	      #  sign_up(resource_name, resource)
	      #  respond_with resource, location: after_sign_up_path_for(resource)
	      #else
	      #  set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
	      #  expire_data_after_sign_in!
	      #  respond_with resource, location: after_inactive_sign_up_path_for(resource)
	      #end
	    else
	      clean_up_passwords resource
	      set_minimum_password_length
	      respond_with resource
	    end
	  end
	  rescue ActiveRecord::RecordInvalid =>  exception
	      imprimir_mensajes_excepcion(exception)
          flash[:alert] = "Error al guardar el usuario"
	      render :new
	  rescue ActiveRecord::RecordNotUnique => exception
	      imprimir_mensajes_excepcion(exception)
	      #mensaje = exception.message 
	      #restriccion_identificacion = "cfg_usuarios_identificacion_key"
	      #restriccion_login = "usuarios_login_usuario_key"
          #if mensaje.include?(restriccion_identificacion)
          #  flash[:alert] =  "Ya existe un usuario con la misma identificación"
          #elsif mensaje.include?(restriccion_login)
          #  flash[:alert] = "Ya existe un usuario con el mismo login"
          #else
          #  flash[:alert] = "Error al guardar el usuario"
          #end	
	      #flash[:alert] = exception.message 
	      #{}"Error al guardar el usuario. <<BR> Ya existe un usuario con la misma identificación"
	      flash[:alert] = "Error al guardar el usuario"
	      render :new
	  rescue => exception
	      imprimir_mensajes_excepcion(exception)
	      flash[:alert] = "Error al guardar el usuario"
	      render :new
      end
	end	

	def allowed_params
      params.require(:cfg_usuario).permit(:tipo_identificacion, :identificacion, :tipo_usuario, :primer_nombre, 
      	:segundo_nombre, :primer_apellido, :segundo_apellido, :genero, :email)
      params.require(:user).permit(:email)
    end 

    private
    #def cfg_imagen_params
    #  params(:cfg_imagen).permit(:image, :file)
    #end
    # Imagen de la foto  del usuario
    def cfg_imagen_params
      params[:cfg_imagen].permit(:image, :file) if !params[:cfg_imagen].nil?
    end
    # Imagen de la firma del usuario
    def imagen_firma_params
      params[:imagen_firma].permit(:image, :file) if !params[:imagen_firma].nil?
    end
end
