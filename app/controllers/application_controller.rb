class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :verificar_autorizacion, except: [:sign_in, :sign_out]
  #before_action :blanquear_mensajes

  # MANEJO DE EXCEPCIONES  
 	rescue_from ActiveRecord::RecordNotFound do |exception|
    redireccionar_not_found("Ha ocurrido un error", "No se encontró el registro", exception.message)
  end
  rescue_from ActiveRecord::RecordNotUnique do |exception|
    redireccionar_not_found("Ha ocurrido un error", "Llave duplicada viola restricción de unicidad", exception.message)
  end
  #rescue_from ActiveRecord::StatementInvalid  do |exception|
  #  redireccionar_not_found("Ha ocurrido un error", "Sentencia Inválida", exception.message)
  #end
  #rescue_from ActiveRecord::RecordInvalid  do |exception|
  #   redireccionar_not_found("Ha ocurrido un error", "Registro Inválido", exception.message)
  #end  
  rescue_from ActionView::MissingTemplate do |exception|
    redireccionar_not_found("Ha ocurrido un error", "No se encontró el recurso-template", exception.message)
  end

  def redireccionar_not_found(alert, tipo_error, mensaje)
    logger.error "**** EXCEPCION - REDIRECCIONAR ****"
    logger.error "Dia / Hora: " + Time.now.to_s   
    logger.error "Controlador: " + controller_name
    logger.error "Accion: " + action_name
    # Informacion de la excepcion
    logger.error mensaje
    # Redireccionar a la pagina de errores
    redirect_to errors_not_found_url, :status => 301, :flash => {:alert => alert, :tipo_error => tipo_error, :mensaje => mensaje }
  end  

  # Manejo de la excepción cuando la ruta no existe
  def routing_error
    redireccionar_not_found("Ha ocurrido un error", "No se encontró la ruta solicitada", "")
  end

 	# Error genérico arrojado por la aplicación
 	def render_500 		
 		redirect_to errors_internal_server_error_url
 	end

  def devolver_valor_activo(param)
    param == 'true' ? true:false
  end 

  # IMPRIMIR AL LOG LOS MENSAJES DE LA EXCEPCION 
  def imprimir_mensajes_excepcion(exception)
    logger.error "**** EXCEPCION ****"
    logger.error "Dia / Hora: " + Time.now.to_s
    # Información de la accion
    #logger.error "URL solicitado:" + request.request_uri    
    logger.error "URL origen: " 
    logger.error request.referer
    logger.error "Controlador: " + controller_name
    logger.error "Accion: " + action_name
    # Informacion de la excepcion
    logger.error exception
    logger.error exception.class
    logger.error exception.backtrace
  end

protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :email])
  end

  def after_sign_in_path_for(resource_or_scope)   
    puts "app controller - after_sign_in_path_for"  
    bienvenida_index_url
  end
  
  # Método que verifica la autorización del usuario al 
  # recurso que está accediendo
  def verificar_autorizacion 
  end
  def verificar_autorizacion1
    puts "********** verificar_autorizacion *****************"
    acceso = false
    session[:permisos].each do |controlador, acciones|
      #puts "-----"
      #puts controlador 
      #puts acciones
      #puts "-----"
      if controller_name == controlador
         acciones.each do |accion|
           if action_name == accion
             acceso = true
           end 
         end
      end  
    end
    if acceso == false   
      # Colocar en el log la informacion del usuario que esta accediendo
      # a un recurso no autorizado
      logger.error "\n" + "** Acceso denegado: recurso **"
      logger.error "Dia / Hora: " + Time.now.to_s
      logger.error "Ip: " + request.remote_ip       
      logger.error "Usuario: " + session[:nombre_usuario]          if !session[:nombre_usuario].nil?
      logger.error "Perfil Usuario: " + session[:perfil_usuario]   if !session[:perfil_usuario].nil?      
      logger.error "Empresa: " + session[:empresa_nombre]          if !session[:empresa_nombre].nil?
      logger.error "Sede: "    + session[:sede_nombre]             if !session[:sede_nombre].nil?
      logger.error "Controlador: " + controller_name
      logger.error "Accion: " + action_name
      redireccionar_not_found("Ha ocurrido un error", "No está autorizado para acceder al recurso solicitado", "")
    end  
  end

  def no_autorizado_registro(registro)
    # Colocar en el log la informacion del usuario que esta accediendo
    # a un recurso no autorizado
    logger.error "\n" + "** Acceso denegado: registro **"
    logger.error "Dia / Hora: " + Time.now.to_s
    logger.error "Ip: " + request.remote_ip       
    logger.error "Usuario: " + session[:nombre_usuario]          if !session[:nombre_usuario].nil?
    logger.error "Perfil Usuario: " + session[:perfil_usuario]   if !session[:perfil_usuario].nil?      
    logger.error "Empresa: " + session[:empresa_nombre]          if !session[:empresa_nombre].nil?
    logger.error "Sede: "    + session[:sede_nombre]             if !session[:sede_nombre].nil?
    logger.error "Controlador: " + controller_name
    logger.error "Accion: " + action_name
    logger.error registro.inspect if !registro.nil?
    redireccionar_not_found("Ha ocurrido un error", "No está autorizado para acceder al registro solicitado", "")
  end

  def opciones_menu(perfil_id)
    arreglo = []
    opciones = CfgOpcionesMenuPerfilUsuario.select("regexp_replace(url_opcion,'/', '') as url_opcion").
      left_outer_joins(:cfg_opciones_menu).
      where("cfg_perfiles_usuario_id = ?", perfil_id).
      order("orden asc")
    opciones.each do |opcion|
      arreglo << opcion.url_opcion if !opcion.url_opcion.blank? and !arreglo.include?(opcion.url_opcion)
    end
    return arreglo
  end 

  #def blanquear_mensajes
  #  flash[:notice] = ''    
  #  flash[:alert] = ''    
  #end
end
