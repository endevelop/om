class CfgUsuariosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cfg_usuario, only: [:show, :edit, :update, :estatus_usuario, 
  :reiniciar_clave, :guardar_reinicio_clave] #:destroy,
  
  # GET /cfg_usuarios
  # GET /cfg_usuarios.json
  def index
    query_listar_usuarios
  end

  # GET /cfg_usuarios/1
  # GET /cfg_usuarios/1.json
  def show
  end

  # GET /cfg_usuarios/new
  #def new
  #  @cfg_usuario = CfgUsuario.new
  #end

  # GET /cfg_usuarios/1/edit
  def edit
  end

  # POST /cfg_usuarios
  # POST /cfg_usuarios.json
  #def create
  #end

  # PATCH/PUT /cfg_usuarios/1
  # PATCH/PUT /cfg_usuarios/1.json
  def update
    begin 
      flash[:alert] = ""
      # Guardar los datos en las tablas users y cfg_usuarios  
      CfgUsuario.transaction do
        @cfg_usuario.login_usuario = params[:user][:username]        
        # Actualizar el modelo cfg_usuario 
        if @cfg_usuario.update(cfg_usuario_params)
          # Actualizar el modelo user
          if @user.update(user_params)
            # Actualizar la imagen de la foto en la tabla maestra cfg_imagen
            if !cfg_imagen_params.nil?             
              if @cfg_imagen.nil?
                # Crear la imagen
                @cfg_imagen = CfgImagen.new(cfg_imagen_params)
                @cfg_imagen.save! 
                # Atributo vinculado
                @cfg_usuario.cfg_imagen_id = @cfg_imagen.id 
                @cfg_usuario.save!
              else
                # Actualizar la imagen  
                @cfg_imagen.update_attributes!(cfg_imagen_params)
              end
            end    
            # Actualizar la imagen de la firma en la tabla maestra cfg_imagen
            if !imagen_firma_params.nil?
              if @imagen_firma.nil?
                # Crear la imagen
                @imagen_firma = CfgImagen.new(imagen_firma_params)
                @imagen_firma.save! 
                # Atributo vinculado
                @cfg_usuario.firma_id = @imagen_firma.id  
                @cfg_usuario.save!
              else
                # Actualizar la imagen  
                @imagen_firma.update_attributes!(imagen_firma_params)
              end
            end
            flash[:notice] = "Usuario actualizado satisfactoriamente"
            # Redireccionar al listar usuarios
            redirect_to cfg_usuarios_path
          else
            flash[:alert] = "Error al actualizar el usuario"
            render :edit
          end
        else
          flash[:alert] = "Error al actualizar el usuario"
          render :edit  
        end  
      end  # de la transaccion
    rescue ActiveRecord::RecordInvalid =>  exception
        imprimir_mensajes_excepcion(exception)
          flash[:alert] = "Error al guardar el usuario"
        render :edit
    rescue ActiveRecord::RecordNotUnique => exception
        imprimir_mensajes_excepcion(exception)
        flash[:alert] = "Error al guardar el usuario"
        render :edit
    rescue => exception
        imprimir_mensajes_excepcion(exception)
        flash[:alert] = "Error al guardar el usuario"
        render :edit
      end  
  end

 
  # METODO QUE ACTUALIZA EL ESTATUS DE LA CUENTA DE USUARIO
  def estatus_usuario
    if @cfg_usuario.update(estado_cuenta: !devolver_valor_activo(params[:activo]))
      flash[:notice] = "Estatus de la cuenta de usuario actualizado satisfactoriamente"      
    else
      flash[:alert] = "Estatus de la cuenta de usuario no pudo ser actualizado"  
      # Mostrar junto al mensaje de alert, los errores del modelo
      @cfg_usuario.errors.each do |campo,msg| 
        flash[:alert] = flash[:alert]  + "<BR>"
        flash[:alert] = flash[:alert]  + msg 
      end
    end
    redirect_to cfg_usuarios_url  
  end  

  # Buscar usuarios segun la ventana de criterios de busqueda
  # Y la paginacion, despues de haber filtrado por algun criterio 
  # de busqueda
  def buscar
    # Ejecuta el query
    query_listar_usuarios
    # Si la accion es buscar por los criterios de busqueda, va al js
    # De lo contrario va al index    
    if !params[:commit].nil? and params[:commit].strip == "Buscar"
      render "buscar.js"
    else
      render :index
    end  
  end

  # Método que ejecuta el query segun el perfil de usuario
  # y los criterios de busqueda 
  def query_listar_usuarios    
    # Variables para la condicion y valores del query
    condiciones = []
    argumentos = Hash.new 

    # Perfil Administrador de Empresa, solo 
    # filtrar los usuarios de sus sedes-Empresa
    if session[:usuario_asociado_empresa] == true
      condiciones << 'cfg_empresas.id = :empresa_id'
      argumentos[:empresa_id] = session[:empresa_id]
    end

    # Criterio de busqueda: login 
    unless params[:login].blank?
      condiciones << 'login_usuario = :login_usuario'
      argumentos[:login_usuario] = params[:login]
    end
    # Criterio de busqueda: nombre y apellidos 
    unless params[:nombres_apellidos].blank?
      condiciones << 'compara_ascii(UPPER(primer_nombre))    ILIKE compara_ascii(UPPER(:nombres_apellidos)) OR 
                      compara_ascii(UPPER(segundo_nombre))   ILIKE compara_ascii(UPPER(:nombres_apellidos)) OR 
                      compara_ascii(UPPER(primer_apellido))  ILIKE compara_ascii(UPPER(:nombres_apellidos)) OR 
                      compara_ascii(UPPER(segundo_apellido)) ILIKE compara_ascii(UPPER(:nombres_apellidos))'
      argumentos[:nombres_apellidos] = "%"+params[:nombres_apellidos].strip+"%"
    end
    # Criterio de búsqueda: perfil
    unless params[:perfil_id].blank?
      condiciones << 'cfg_perfiles_usuario_id = :perfil_id'
      argumentos[:perfil_id] = params[:perfil_id]
    end
    # Criterio de búsqueda: tipo de usuario
     unless params[:tipo_usuario_id].blank?
      condiciones << 'tipo_usuario = :tipo_usuario_id'
      argumentos[:tipo_usuario_id] = params[:tipo_usuario_id]
    end
    # Criterio de búsqueda: empresa
     unless params[:empresa_id].blank?
      condiciones << 'cfg_sedes.cfg_empresa_id = :empresa_id'
      argumentos[:empresa_id] = params[:empresa_id]
    end
    # Criterio de búsqueda: sede
     unless params[:sede_id].blank?
      condiciones << 'cfg_sede_id = :sede_id'
      argumentos[:sede_id] = params[:sede_id]
    end
    # Criterio de búsqueda: estatus de la cuenta
     unless params[:estado_cuenta].blank?
      condiciones << 'cfg_usuarios.estado_cuenta = :estado_cuenta'
      argumentos[:estado_cuenta] = params[:estado_cuenta]
    end

    # Query
    @cfg_usuarios = CfgUsuario.select("cfg_usuarios.id, cfg_usuarios.tipo_identificacion, cfg_usuarios.identificacion, cfg_usuarios.tipo_usuario,
      cfg_usuarios.primer_nombre, cfg_usuarios.segundo_nombre, cfg_usuarios.primer_apellido, cfg_usuarios.segundo_apellido,
      cfg_usuarios.estado_cuenta, cfg_empresas.razon_social, cfg_sedes.nombre_sede,
      cfg_usuarios.login_usuario, (cfg_usuarios.primer_nombre || cfg_usuarios.segundo_nombre) AS nombres,
      (cfg_usuarios.primer_apellido || cfg_usuarios.segundo_apellido) AS apellidos, cfg_usuarios.cfg_perfiles_usuario_id,
      UPPER(cfg_perfiles_usuarios.nombre_perfil) AS nombre_perfil, 
      UPPER(cfg_clasif_tipo_usuario.descripcion) AS nombre_tipo_usuario,
      UPPER(cfg_clasif_tipo_identificacion.descripcion) AS descripcion_tipo_identificacion")   
    .joins("INNER JOIN users ON cfg_usuarios.user_id = users.id 
            INNER JOIN cfg_perfiles_usuarios ON cfg_usuarios.cfg_perfiles_usuario_id = cfg_perfiles_usuarios.id
            LEFT JOIN cfg_clasificaciones cfg_clasif_tipo_usuario ON cfg_usuarios.tipo_usuario = cfg_clasif_tipo_usuario.id
            LEFT JOIN cfg_clasificaciones cfg_clasif_tipo_identificacion ON cfg_usuarios.tipo_identificacion = cfg_clasif_tipo_identificacion.id
            LEFT JOIN cfg_sedes ON cfg_usuarios.cfg_sede_id =  cfg_sedes.id
            LEFT JOIN cfg_empresas ON cfg_sedes.cfg_empresa_id = cfg_empresas.id")
    .order('login_usuario').page(params[:page])
    .where(condiciones.join(" AND "),argumentos)
    
  end

  def actualizar_sedes_por_empresa
  end

  # METODO QUE MUESTRA LA PAGINA PARA EL REINICIO DE CLAVE DEL USUARIO
  def reiniciar_clave

  end
  
  # METODO PARA GUARDAR LA NUEVA CLAVE DEL USUARIO
  def guardar_reinicio_clave
    if @user.update(user_params)
      redirect_to cfg_usuarios_path, notice: 'Se reinició la clave del usuario satisfactoriamente'      
    else
      render :reiniciar_clave
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cfg_usuario
      @cfg_usuario  = CfgUsuario.find(params[:id])
      @user = @cfg_usuario.user
      @cfg_imagen   = @cfg_usuario.cfg_imagen
      @imagen_firma = @cfg_usuario.imagen_firma
      # Verificar el acceso al registro, por la empresa
      # asociada al usuario de la sesion
      if session[:usuario_asociado_empresa] == true
        autorizado = @cfg_usuario.autorizado_por_empresa?(session[:empresa_id])
        if autorizado == false
          no_autorizado_registro(@cfg_usuario)
        end
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cfg_usuario_params
      params.require(:cfg_usuario).permit(:tipo_usuario, :integer, :identificacion, 
        :tipo_identificacion, :login_usuario, :estado_cuenta, :primer_nombre, 
        :segundo_nombre, :primer_apellido, :segundo_apellido, :genero, :cfg_municipio_id, 
        :direccion, :telefono_residencia, :telefono_oficina, :celular, :email, :cargo_actual, 
        :observacion, :cfg_perfiles_usuario_id, :visible, :mostrar_en_historias, :especialidad, 
        :registro_profesional, :personal_atiende, :unidad_funcional, :porcentaje_honorario, 
        :cfg_sede_id )
    end
    def user_params
      # NOTE: Using `strong_parameters` gem
      params[:user].permit(:username, :password, :password_confirmation) if !params[:user].nil?
    end
    # Imagen de la foto  del usuario
    def cfg_imagen_params
      params[:cfg_imagen].permit(:image, :file) if !params[:cfg_imagen].nil?
    end
    # Imagen de la firma del usuario
    def imagen_firma_params
      params[:imagen_firma].permit(:image, :file) if !params[:imagen_firma].nil?
    end
end
