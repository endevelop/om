json.extract! cfg_opciones_menu, :id, :nombre_opcion, :id_opcion_padre, :url_opcion, :orden, :created_at, :updated_at
json.url cfg_opciones_menu_url(cfg_opciones_menu, format: :json)
