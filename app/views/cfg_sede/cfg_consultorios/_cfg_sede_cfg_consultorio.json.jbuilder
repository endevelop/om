json.extract! cfg_sede_cfg_consultorio, :id, :cod_consultorio, :nom_consultorio, :piso_consultorio, :cod_especialidad, :is_consultorio_urgencia, :cfg_sede_id, :created_at, :updated_at
json.url cfg_sede_cfg_consultorio_url(cfg_sede_cfg_consultorio, format: :json)
