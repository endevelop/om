json.array! @u do |x|
  json.id x.id
  json.text x.nombre_especialidad
  json.imagen x.cfg_imagen.image.url(:small) if !x.cfg_imagen.nil?  
end