json.extract! cfg_opciones_menu_cfg_operaciones_opciones_menu, :id, :cfg_opciones_menu_id, :operacion, :activo, :created_at, :updated_at
json.url cfg_opciones_menu_cfg_operaciones_opciones_menu_url(cfg_opciones_menu_cfg_operaciones_opciones_menu, format: :json)
