json.extract! cfg_empresa_cfg_sede, :id, :codigo_sede, :nombre_sede, :departamento, :municipio, :encargado, :direccion, :telefono1, :telefono2, :cfg_empresa_id, :created_at, :updated_at
json.url cfg_empresa_cfg_sede_url(cfg_empresa_cfg_sede, format: :json)
