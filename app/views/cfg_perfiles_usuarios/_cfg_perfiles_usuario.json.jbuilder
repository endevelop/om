json.extract! cfg_perfiles_usuario, :id, :nombre_perfil, :activo, :asociado_empresa, :observaciones, :created_at, :updated_at
json.url cfg_perfiles_usuario_url(cfg_perfiles_usuario, format: :json)
