json.extract! cfg_usuario, :id, :tipo_usuario, :integer, :identificacion, :tipo_identificacion, :login_usuario, :estado_cuenta, :primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :created_at, :updated_at
json.url cfg_usuario_url(cfg_usuario, format: :json)
