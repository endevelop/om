json.extract! cfg_empresa, :id, :cod_departamento, :cod_municipio, :tipo_doc, :num_identificacion, :dv, :razon_social, :tipo_doc_rep_legal, :num_doc_rep_legal, :nom_rep_legal, :direccion, :telefono_1, :telefono_2, :website, :logo, :observaciones, :codigo_empresa, :regimen, :razon_rip, :nivel, :cfg_municipio_id, :created_at, :updated_at
json.url cfg_empresa_url(cfg_empresa, format: :json)
