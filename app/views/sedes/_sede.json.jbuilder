json.extract! sede, :id, :codigo_sede, :nombre_sede, :departamento, :municipio, :encargado, :direccion, :telefono1, :telefono2, :created_at, :updated_at
json.url sede_url(sede, format: :json)
