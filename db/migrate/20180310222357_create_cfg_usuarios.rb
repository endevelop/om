class CreateCfgUsuarios < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_usuarios do |t|
      t.string :tipo_usuario
      t.string :integer
      t.string :identificacion
      t.string :tipo_identificacion
      t.string :login_usuario
      t.boolean :estado_cuenta
      t.string :primer_nombre
      t.string :segundo_nombre
      t.string :primer_apellido
      t.string :segundo_apellido

      t.timestamps
    end
  end
end
