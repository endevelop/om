class CreateCfgConsultorios < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_consultorios do |t|
      t.references :cfg_sede, foreign_key: true
      t.string :cod_consultorio
      t.string :nom_consultorio
      t.integer :piso_consultorio
      t.integer :cod_especialidad
      t.boolean :is_consultorio_urgencia
      t.belongs_to :cfg_sede, foreign_key: true

      t.timestamps
    end
  end
end
