class CreateCfgHorarios < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_horarios do |t|
      t.text :codigo
      t.text :descripcion

      t.timestamps
    end
  end
end
