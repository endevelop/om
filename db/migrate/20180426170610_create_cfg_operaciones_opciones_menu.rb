class CreateCfgOperacionesOpcionesMenu < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_operaciones_opciones_menu do |t|
      t.references :cfg_opciones_menu, foreign_key: true
      t.string :operacion
      t.boolean :activo

      t.timestamps
    end
  end
end
