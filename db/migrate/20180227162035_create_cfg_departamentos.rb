class CreateCfgDepartamentos < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_departamentos do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
