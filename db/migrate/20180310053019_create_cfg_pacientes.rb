class CreateCfgPacientes < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_pacientes do |t|
      t.string, :identificacion
      t.integer, :tipo_identificacion
      t.string, :lugar_expedicion
      t.integer, :genero
      t.timestamp, :fecha_nacimiento
      t.string, :primer_apellido
      t.string, :segundo_apellido
      t.string, :primer_nombre
      t.string, :segundo_nombre
      t.integer, :zona
      t.integer, :regimen
      t.integer, :nivel
      t.boolean, :activo
      t.integer, :foto
      t.integer, :firma
      t.integer, :estado_civil
      t.integer, :grupo_sanguineo
      t.integer, :etnia
      t.integer, :escolaridad
      t.integer, :ocupacion
      t.string, :direccion
      t.string, :telefono_residencia
      t.string, :telefono_oficina
      t.string, :celular
      t.integer, :tipo_afiliado
      t.timestamp, :fecha_afiliacion
      t.timestamp, :fecha_sisben
      t.string, :carnet
      t.integer, :cfg_departamento_id
      t.integer, :cfg_municipio_id
      t.string, :barrio
      t.string, :responsable
      t.string, :acompanante
      t.string, :telefono_acompanante
      t.string, :historia
      t.timestamp, :fechainsc
      t.boolean, :facturarlo
      t.string, :dircatastrofe
      t.integer, :municipiocatastrofe
      t.integer, :departamentocatastrofe
      t.string, :zonacatastrofe
      t.timestamp, :fechacatastrofe
      t.timestamp, :fechaop
      t.string, :grado
      t.integer, :parentesco
      t.boolean, :pensionado
      t.integer, :id_administradora
      t.integer, :dep_nacimiento
      t.integer, :mun_nacimiento
      t.string, :email
      t.integer, :categoria_paciente
      t.string, :numero_autorizacion
      t.string, :telefono_responsable
      t.timestamp, :fecha_vence_carnet
      t.string, :observaciones
      t.integer, :id_discapacidad
      t.integer, :id_gestacion
      t.boolean, :poblacion_lbgt
      t.boolean, :desplazado
      t.integer, :id_religion
      t.boolean, :victima_maltrato
      t.boolean, :victima_conflicto_armado
      t.integer, :id_contrato
      t.integer :parentesco_a

      t.timestamps
    end
  end
end
