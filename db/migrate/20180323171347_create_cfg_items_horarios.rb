class CreateCfgItemsHorarios < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_items_horarios do |t|
      t.integer :dia
      t.timestamp :hora_inicio
      t.timestamp :hora_final
      t.integer :cfg_horario_id
      t.text :nombredia

      t.timestamps
    end
  end
end
