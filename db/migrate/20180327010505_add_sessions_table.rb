class AddSessionsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :sessiones do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps
    end

    add_index :sessiones, :session_id, :unique => true
    add_index :sessiones, :updated_at
  end
end
