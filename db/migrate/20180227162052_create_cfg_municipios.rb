class CreateCfgMunicipios < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_municipios do |t|
      t.string :nombre
      t.belongs_to :cfg_departamento, foreign_key: true

      t.timestamps
    end
  end
end
