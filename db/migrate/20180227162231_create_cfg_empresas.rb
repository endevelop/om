class CreateCfgEmpresas < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_empresas do |t|
      t.integer :cod_departamento
      t.integer :cod_municipio
      t.integer :tipo_doc
      t.string :num_identificacion
      t.string :dv
      t.string :razon_social
      t.integer :tipo_doc_rep_legal
      t.string :num_doc_rep_legal
      t.string :nom_rep_legal
      t.string :direccion
      t.string :telefono_1
      t.string :telefono_2
      t.string :website
      t.integer :logo
      t.string :observaciones
      t.string :codigo_empresa
      t.string :regimen
      t.string :razon_rip
      t.string :nivel
      t.integer :cfg_municipio_id

      t.timestamps
    end
  end
end
