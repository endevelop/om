class CreateCfgSedes < ActiveRecord::Migration[5.1]
  def change
    create_table :cfg_sedes do |t|
      t.references :cfg_empresa, foreign_key: true
      t.string :codigo_sede
      t.string :nombre_sede
      t.integer :departamento
      t.integer :municipio
      t.string :encargado
      t.string :direccion
      t.string :telefono1
      t.string :telefono2
      t.belongs_to :cfg_empresa, foreign_key: true

      t.timestamps
    end
  end
end
