# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180327010505) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cfg_cama", primary_key: "id_cama", id: :integer, default: -> { "nextval('cfg_cama_id_seq'::regclass)" }, force: :cascade, comment: "Entidad para almacenar el numero de camas que tiene una habitacion de urgencia" do |t|
    t.integer "id_habitacion"
    t.string "numero_cama"
    t.string "observacion"
    t.boolean "ocupado"
  end

  create_table "cfg_campos_archivos_paciente", primary_key: "id_campoarchivo", id: :integer, default: -> { "nextval('cfg_campos_archivos_paciente_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_administradora"
    t.string "nombre_campo", limit: 40
    t.integer "orden"
  end

  create_table "cfg_centro_costo", primary_key: "id_centro_costo", id: :serial, force: :cascade do |t|
    t.string "cod_centro_costo", limit: 50, comment: "CODIGO DEL CENTRO DE COSTOS"
    t.string "nom_centro_costo", limit: 100, comment: "NOMBRE DEL CENTRO DE COSTOS"
    t.string "cta_contable_ing", limit: 20, comment: "CODIGO DE LA CUENTA CONTABLE DE INGRESOS QUE VA A ESTAR ASOCIADO EL CENTRO DE COSTO"
    t.string "cta_contable_ing_ps", limit: 20, comment: "CODIGO DE LA CUENTA CONTABLE DE INGRESOS CUOTA/COPAGO/BONO"
    t.string "cta_devolucion", limit: 20, comment: "CODIGO DE LA CUENTA CONTABLE DE DEVOLUCIONES"
    t.string "conc_cartera", limit: 2, comment: "CONCEPTO DE CARTERA"
    t.string "conc_devolucion", limit: 2, comment: "DEVOLUCIONES EN VENTAS"
  end

  create_table "cfg_clasificaciones", id: :serial, force: :cascade, comment: "Tabla que contiene las categorias de:\n- Escolaridad\n- Regimenes\n..." do |t|
    t.string "codigo", limit: 20
    t.string "maestro", limit: 50
    t.text "descripcion"
    t.text "observacion"
    t.boolean "activo", default: true
  end

  create_table "cfg_configuraciones", primary_key: "id_configuracion", id: :serial, comment: "llave primaria", force: :cascade, comment: "Diferentes configuraciones necesarias para el manejo de la aplicacion" do |t|
    t.string "usuario", limit: 100
    t.string "clave", limit: 100
    t.string "nombre_db", limit: 100
    t.string "servidor", limit: 100
    t.string "ruta_imagenes", limit: 100
    t.string "ruta_copias_seguridad", limit: 100
    t.text "ruta_bin_postgres", comment: "ruta de la carpeta bin de postgres"
  end

  create_table "cfg_consultorios", id: :integer, default: -> { "nextval('cfg_consultorios_id_consultorio_seq'::regclass)" }, force: :cascade do |t|
    t.string "cod_consultorio", limit: 5, null: false
    t.text "nom_consultorio"
    t.integer "piso_consultorio"
    t.integer "cod_especialidad"
    t.integer "cfg_sede_id"
    t.boolean "is_consultorio_urgencia", default: false
    t.boolean "activo", default: true
  end

  create_table "cfg_copias_seguridad", primary_key: "id_copia", id: :serial, force: :cascade do |t|
    t.string "nombre", limit: 300
    t.datetime "fecha"
    t.text "ruta"
    t.string "tipo", limit: 10, comment: "AUTOMATICA o MANUAL"
  end

  create_table "cfg_correo", id: :integer, default: nil, force: :cascade do |t|
    t.string "host", limit: 50
    t.string "port", limit: 50
    t.string "email", limit: 50
    t.string "password", limit: 50
  end

  create_table "cfg_cups", primary_key: "cod_cups", id: :string, limit: 20, force: :cascade, comment: " CLASIFICACIÓN UNICA DE PROCEDIMIENTOS EN SALUD" do |t|
    t.text "subcategoria"
    t.string "observacion", limit: 300
    t.boolean "visible"
  end

  create_table "cfg_departamentos", id: :serial, force: :cascade do |t|
    t.text "nombre"
  end

  create_table "cfg_diagnostico", primary_key: "codigo_diagnostico", id: :string, limit: 10, comment: "CODIGO DEL DIAGNOSTICO DE ACUERDO AL CIE 10\n", force: :cascade do |t|
    t.text "nombre_diagnostico"
    t.string "codigo_especialidad", limit: 5
    t.string "codigo_evento", limit: 5
    t.string "simbolo", limit: 1
    t.string "sexo", limit: 1
    t.integer "edad", limit: 2, comment: "EDAD A PARTIR DE LA CUAL SE PUEDE APLICAR EL PROGRAMA"
    t.string "unidad_edad", limit: 1, comment: "UNIDAD DE MEDIDA DE LA EDAD D- DIAS M- MESES A-AÑOS"
    t.integer "edad_final", limit: 2
    t.string "unidad_edad_final", limit: 1
    t.boolean "atencion_obligatoria"
    t.boolean "vigilancia_epidemiologica"
    t.boolean "cent"
    t.boolean "pat_cron"
    t.boolean "dis_mental"
    t.boolean "dis_sensorial"
    t.boolean "dis_motriz"
  end

  create_table "cfg_diagnostico_principal", primary_key: "codigo_diagnostico", id: :string, limit: 10, comment: "CODIGO DEL DIAGNOSTICO DE ACUERDO AL CIE 10\n", force: :cascade do |t|
    t.text "nombre_diagnostico"
    t.string "codigo_especialidad", limit: 5
    t.string "codigo_evento", limit: 5
    t.string "simbolo", limit: 1
    t.string "sexo", limit: 1
    t.integer "edad", limit: 2, comment: "EDAD A PARTIR DE LA CUAL SE PUEDE APLICAR EL PROGRAMA"
    t.string "unidad_edad", limit: 1, comment: "UNIDAD DE MEDIDA DE LA EDAD D- DIAS M- MESES A-AÑOS"
    t.integer "edad_final", limit: 2
    t.string "unidad_edad_final", limit: 1
    t.boolean "atencion_obligatoria"
    t.boolean "vigilancia_epidemiologica"
    t.boolean "cent"
    t.boolean "pat_cron"
    t.boolean "dis_mental"
    t.boolean "dis_sensorial"
    t.boolean "dis_motriz"
  end

  create_table "cfg_dias_no_laborales", primary_key: ["id_sede", "fecha_no_laboral"], force: :cascade, comment: "guarda los dias no laborales por cada sede de la empresa" do |t|
    t.integer "id_sede", null: false
    t.date "fecha_no_laboral", null: false
  end

  create_table "cfg_empresas", id: :serial, force: :cascade do |t|
    t.integer "cod_departamento"
    t.integer "cod_municipio"
    t.integer "tipo_doc"
    t.string "num_identificacion", limit: 20
    t.string "dv", limit: 1, comment: "DIGITO DE VERIFICACION. DEBE SER SOLICITADO DE ACUERDO AL TIPO DE DOCUMENTO QUE SE SELECCIONE"
    t.string "razon_social", limit: 200, comment: "NOMBRE DE LA EMPRESA QUE COMPRA EL SOFTWARE"
    t.integer "tipo_doc_rep_legal", comment: "TIPO DE DOCUMENTO DEL REPRESENTANTE LEGALPARA ESTE CASO SERIA CC, CE, PASAPORTE.\n"
    t.string "num_doc_rep_legal", limit: 20
    t.string "nom_rep_legal", limit: 200, comment: "NOMBRE DEL REPRESENTANTE LEGAL DE LA EMPRESA\n"
    t.string "direccion", limit: 200
    t.string "telefono_1", limit: 10
    t.string "telefono_2", limit: 10
    t.string "website", limit: 100, comment: "PAGINA WEB DE LA EMPRESA"
    t.text "observaciones"
    t.string "codigo_empresa", limit: 50, comment: "codigo de la empresa dad por el ministerio"
    t.string "regimen", limit: 50
    t.text "razon_rip", comment: "razon social usada en los rips"
    t.string "nivel", limit: 100
    t.integer "cfg_municipio_id"
    t.boolean "activo", default: true
    t.text "filename"
    t.text "content_type"
    t.binary "file_contents"
    t.text "image_file_name"
    t.binary "image_content_type"
    t.text "image_file_size"
    t.datetime "image_updated_at"
    t.bigint "cfg_imagen_id"
  end

  create_table "cfg_evento", primary_key: "cod_evento", id: :string, limit: 5, comment: "CODIGO DEL EVENTO", force: :cascade do |t|
    t.string "nom_evento", limit: 150, comment: "NOMBRE DEL EVENTO"
    t.boolean "centinela", comment: "SI ES DE SEGUIMIENTO"
    t.boolean "enfermedad_salud_publica", comment: "INDICA SI ES ENFERMEDAD DE INTERES DE SALUD PUBLICA"
  end

  create_table "cfg_habitacion", primary_key: "id_habitacion", id: :integer, default: -> { "nextval('cfg_habitacion_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_sede"
    t.string "numero_habitacion"
    t.string "observacion"
    t.boolean "is_habitacion_urgencia"
  end

  create_table "cfg_horario", primary_key: "id_horario", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 5
    t.string "descripcion", limit: 150
  end

  create_table "cfg_imagenes", id: :serial, force: :cascade do |t|
    t.text "nombre"
    t.text "nombre_en_servidor"
    t.text "url_imagen"
    t.text "image_file_name"
    t.binary "image_content_type"
    t.text "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "cfg_insumo", primary_key: "id_insumo", id: :serial, force: :cascade do |t|
    t.string "codigo_insumo", limit: 20
    t.string "nombre_insumo", limit: 150
    t.text "observacion"
    t.float "valor"
    t.boolean "aplica_iva"
    t.boolean "aplica_cree"
    t.integer "id_administradora", comment: "adminstradora"
  end

# Could not dump table "cfg_items_horario" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora_inicio'

  create_table "cfg_maestros_clasificaciones", primary_key: "maestro", id: :string, limit: 50, force: :cascade do |t|
    t.text "descripcion", comment: "descripcion de lo que coresponde esta clasificacion"
  end

  create_table "cfg_maestros_txt_predefinidos", primary_key: "id_maestro", id: :serial, force: :cascade, comment: "tabla con los nombres de las agrupaciones de textos predefinidos" do |t|
    t.string "nombre", limit: 100
  end

  create_table "cfg_medicamento", primary_key: "id_medicamento", id: :serial, force: :cascade do |t|
    t.string "codigo_medicamento", limit: 20, comment: "CODIGO DEL MEDICAMENTO"
    t.string "codigo_cums", limit: 20, comment: "CODIGO CUMS"
    t.string "codigo_cups", limit: 20, comment: "CODIGO CUPS\n"
    t.string "nombre_medicamento", limit: 150
    t.string "nombre_generico", limit: 150
    t.string "nombre_comercial", limit: 150
    t.string "forma_medicamento", limit: 150
    t.boolean "pos"
    t.string "concentracion", limit: 100
    t.string "unidad_medida", limit: 20
    t.boolean "control_medico"
    t.string "registro_sanitario", limit: 50
    t.string "mod_admin", limit: 50
    t.float "valor"
    t.boolean "aplica_iva"
    t.boolean "aplica_cree"
    t.integer "id_producto"
  end

  create_table "cfg_municipios", id: :serial, force: :cascade do |t|
    t.text "nombre", null: false
    t.integer "cfg_departamento_id", null: false
  end

  create_table "cfg_opciones_menu", id: :integer, default: -> { "nextval('cfg_opciones_menu_seq'::regclass)" }, comment: "identificador de la opcion", force: :cascade, comment: "Son todas las opciones que puede contener el menu principal" do |t|
    t.integer "id_opcion_padre", default: 0
    t.string "nombre_opcion", limit: 40, null: false
    t.string "style", limit: 100, comment: "corresponde al estilo css cuando es un boton de la pagina home(nivel 1,2)\no al icono primefaces si se trata de un item de un menubar de una pagina(nivel >2)"
    t.string "url_opcion", limit: 100
    t.integer "orden", comment: "orden en que se encuentra dentro del menu"
  end

  create_table "cfg_opciones_menu_perfil_usuarios", id: :serial, force: :cascade, comment: "tabla intermedia entre perfil de usuario y opciones de menu que tiene acceso" do |t|
    t.integer "cfg_perfiles_usuario_id", null: false, comment: "identificador de la opcion"
    t.integer "cfg_opciones_menu_id", null: false, comment: "identificador en la tabla opciones_menu"
  end

  create_table "cfg_pacientes", id: :serial, force: :cascade do |t|
    t.string "identificacion", limit: 50, null: false
    t.integer "tipo_identificacion"
    t.string "lugar_expedicion", limit: 30
    t.integer "genero"
    t.datetime "fecha_nacimiento"
    t.string "primer_apellido", limit: 30
    t.string "segundo_apellido", limit: 30
    t.string "primer_nombre", limit: 30
    t.string "segundo_nombre", limit: 30
    t.integer "zona"
    t.integer "regimen"
    t.integer "nivel"
    t.boolean "activo"
    t.integer "foto"
    t.integer "firma"
    t.integer "estado_civil"
    t.integer "grupo_sanguineo"
    t.integer "etnia"
    t.integer "escolaridad"
    t.integer "ocupacion"
    t.string "direccion", limit: 80
    t.string "telefono_residencia", limit: 100
    t.string "telefono_oficina", limit: 20
    t.string "celular", limit: 100
    t.integer "tipo_afiliado"
    t.datetime "fecha_afiliacion"
    t.datetime "fecha_sisben"
    t.string "carnet", limit: 40
    t.integer "cfg_departamento_id"
    t.integer "cfg_municipio_id"
    t.text "barrio"
    t.string "responsable", limit: 50
    t.string "acompanante", limit: 50
    t.string "telefono_acompanante", limit: 100
    t.string "historia", limit: 10
    t.datetime "fechainsc"
    t.boolean "facturarlo"
    t.string "dircatastrofe", limit: 50
    t.integer "municipiocatastrofe"
    t.integer "departamentocatastrofe"
    t.string "zonacatastrofe", limit: 1
    t.datetime "fechacatastrofe"
    t.datetime "fechaop"
    t.string "grado", limit: 30
    t.integer "parentesco"
    t.boolean "pensionado"
    t.integer "fac_administradora_id"
    t.integer "dep_nacimiento", comment: "departamento nacimiento"
    t.integer "mun_nacimiento", comment: "municipio nacimiento"
    t.string "email", limit: 300
    t.integer "categoria_paciente"
    t.string "numero_autorizacion", limit: 50
    t.string "telefono_responsable", limit: 20
    t.datetime "fecha_vence_carnet"
    t.text "observaciones"
    t.integer "id_discapacidad"
    t.integer "id_gestacion", comment: "... Si, No, No aplica"
    t.boolean "poblacion_lbgt", comment: "...LBGT si o no"
    t.boolean "desplazado", comment: "...si o no"
    t.integer "id_religion", comment: "... id de la religion"
    t.boolean "victima_maltrato", comment: "...si o no"
    t.boolean "victima_conflicto_armado", comment: "... si o no"
    t.integer "id_contrato"
    t.integer "parentesco_a"
    t.text "image_file_name"
    t.binary "image_content_type"
    t.text "image_file_size"
    t.datetime "image_updated_at"
    t.integer "estrato_id"
    t.integer "cfg_sede_id"
    t.bigint "cfg_imagen_id"
    t.index ["identificacion"], name: "cfg_pacientes_identificacion_key", unique: true
  end

  create_table "cfg_perfiles_usuarios", id: :integer, default: -> { "nextval('cfg_perfiles_usuario_seq'::regclass)" }, comment: "identificador del perfil", force: :cascade, comment: "un perfil permite agrupar un conjunto de opciones del menu y asi asignarlas a un usario determinado\n" do |t|
    t.string "nombre_perfil", limit: 50, null: false, comment: "nombre del perfil"
    t.boolean "activo", default: true, null: false
    t.boolean "asociado_empresa", default: false, null: false
    t.text "observaciones"
  end

  create_table "cfg_proposito_consulta", primary_key: "id_proposito", id: :integer, default: nil, force: :cascade do |t|
    t.string "descripcion", limit: 50
  end

  create_table "cfg_sedes", id: :integer, default: -> { "nextval('cfg_sede_id_sede_seq'::regclass)" }, force: :cascade do |t|
    t.string "codigo_sede", limit: 10, comment: "CODIGO DE SEDE"
    t.string "nombre_sede", limit: 200, comment: "NOMBRE DE SEDE"
    t.integer "departamento"
    t.integer "municipio"
    t.string "encargado", limit: 200
    t.string "direccion", limit: 200
    t.string "telefono1", limit: 10
    t.string "telefono2", limit: 10
    t.bigint "cfg_empresa_id"
    t.integer "cfg_municipio_id"
    t.boolean "activo", default: true
  end

  create_table "cfg_txt_predefinidos", primary_key: "id_txt_predefinido", id: :serial, force: :cascade, comment: "tabla con cada uno de los textos predefinidos que se pueden usar en el sistema" do |t|
    t.string "nombre", limit: 100
    t.integer "id_maestro"
    t.text "detalle"
  end

  create_table "cfg_unidad", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 20
    t.string "nombre", limit: 50
  end

  create_table "cfg_unidad_funcional", primary_key: "cod_und_funcional", id: :string, limit: 2, comment: "CODIGO DE LA UNIDAD FUNCIONAL", force: :cascade do |t|
    t.string "nom_und_funcional", limit: 100, comment: "NOMBRE UNIDAD FUNCIONAL\n"
    t.string "cod_cto_costo", limit: 3, comment: "CODIGO DEL CENTRO DE COSTOS\n"
  end

  create_table "cfg_usuarios", id: :integer, default: -> { "nextval('cfg_usuarios_id_usuario_seq'::regclass)" }, force: :cascade do |t|
    t.integer "tipo_usuario", limit: 2, comment: "clasificacion TipoUsuario: 1 usuario, 2 prestador"
    t.string "identificacion", limit: 20
    t.integer "tipo_identificacion"
    t.string "login_usuario", limit: 50
    t.boolean "estado_cuenta", default: true
    t.integer "cfg_perfiles_usuario_id", null: false
    t.string "observacion", limit: 500
    t.string "primer_nombre", limit: 60
    t.string "segundo_nombre", limit: 60
    t.string "primer_apellido", limit: 60
    t.string "segundo_apellido", limit: 60
    t.string "direccion", limit: 80
    t.string "telefono_residencia", limit: 10
    t.string "telefono_oficina", limit: 10
    t.string "celular", limit: 20
    t.string "email", limit: 100
    t.string "cargo_actual", limit: 100
    t.string "registro_profesional", limit: 30
    t.integer "especialidad", limit: 2
    t.string "unidad_funcional", limit: 100
    t.float "porcentaje_honorario"
    t.datetime "fecha_creacion"
    t.integer "foto"
    t.integer "firma_id"
    t.integer "genero"
    t.boolean "visible", default: true, comment: "se muetra o no el medico en los listados"
    t.integer "personal_atiende", comment: " sii el usuario es prestador debe permitir especificar el tipo de personal que atiende (por defecto va ser 1):\n1=Médico(a) especialista,\n2 =Médico(a) general\n3 =Enfermera (o)\n4 =Auxiliar de enfermería\n5 =Otro"
    t.boolean "mostrar_en_historias", default: true, comment: "Permite identificar si este usuario se lista en los formularios de registros clinicos"
    t.integer "cfg_municipio_id"
    t.integer "user_id"
    t.integer "cfg_empresa_id"
    t.integer "cfg_sede_id"
    t.integer "cfg_imagen_id"
    t.index ["identificacion"], name: "cfg_usuarios_identificacion_key", unique: true
    t.index ["identificacion"], name: "usuarios_identificacion_key", unique: true
    t.index ["login_usuario"], name: "usuarios_login_usuario_key", unique: true
  end

  create_table "cfg_usuarios2", id: :bigint, default: -> { "nextval('cfg_usuarios_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_cfg_usuarios_on_email", unique: true
    t.index ["reset_password_token"], name: "index_cfg_usuarios_on_reset_password_token", unique: true
  end

  create_table "cit_autorizaciones", primary_key: "id_autorizacion", id: :integer, default: -> { "nextval('autorizaciones_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "cfg_paciente_id"
    t.string "num_autorizacion", limit: 10, comment: "numero de autorizacion"
    t.integer "administradora", comment: "codigo de la administradora"
    t.bigint "id_usuario_creador", comment: "usuario que crea la autorizacion"
    t.boolean "cerrada", comment: "si la autorizacion esta cerrada (puede estar cerrada sin que se haya completado el uso de todos los servicios que contenia)"
    t.boolean "facturada", comment: "determina si se realizo una facura para esta autorizacion (se usa para cobrar 1 sola cuota moderadora por autorizacion)"
    t.date "fecha_sistema"
    t.date "fecha_autorizacion"
  end

  create_table "cit_autorizaciones_servicios", primary_key: ["id_autorizacion", "id_servicio"], force: :cascade do |t|
    t.integer "id_autorizacion", null: false
    t.integer "id_servicio", null: false
    t.integer "sesiones_autorizadas"
    t.integer "sesiones_pendientes"
    t.integer "sesiones_realizadas"
    t.serial "id_sincronizador", null: false
  end

  create_table "cit_citas", primary_key: "id_cita", id: :serial, force: :cascade do |t|
    t.integer "cfg_paciente_id", null: false
    t.integer "id_prestador", null: false
    t.bigint "id_turno", null: false
    t.boolean "atendida", default: false
    t.boolean "cancelada", default: false, comment: "valor boolean para saber si la cita fue o no cancelada"
    t.integer "tipo_cita"
    t.boolean "multado", default: false, comment: "si existe multa"
    t.text "desc_cancelacion", comment: "describcion de la cancelacion"
    t.integer "motivo_cancelacion", comment: "codigo del motivo de cancelacion, se encuentra en la tabla clasificaciones"
    t.integer "id_autorizacion", comment: "referencia al id de la tabla autorizacion, si se requiere para la creacion de la cita"
    t.integer "id_administradora", comment: "codigo administradora"
    t.integer "id_servicio"
    t.date "fecha_registro", comment: "fecha de registro de la cita"
    t.date "fecha_cancelacion", comment: "fecha de cancelacion de la cita"
    t.boolean "facturada", comment: "si la cita se encuentra facturada (pagada)"
    t.integer "id_paquete", comment: "paquete de la cita"
    t.integer "no_paq_aplicado"
    t.boolean "tiene_reg_asociado", comment: "determinar si tiene uno o mas registros clinicos asociados\ntrue=tiene minimo un registro asociado\nfalse=no tiene ningun registro clinico asociado"
    t.string "num_autorizacion", limit: 15, comment: "Numero de autorizacion.  Se usa cuando el modulo de autorizaciones no se tiene en cuenta (optica)"
  end

  create_table "cit_paq_detalle", primary_key: "id_paq_detalle", id: :serial, force: :cascade, comment: "tabla detalle para los paquetes de las citas" do |t|
    t.integer "id_paq_maestro", null: false, comment: "identificacion del paquete maestro"
    t.integer "id_prestador", null: false, comment: "identifcacion del prestador(usuario)"
    t.integer "id_servicio", null: false, comment: "identificacion del servicio"
  end

  create_table "cit_paq_maestro", primary_key: "id_paq_maestro", id: :serial, force: :cascade, comment: "Tabla maestro para los paquetes de citas" do |t|
    t.string "cod_paquete", limit: 5, comment: "codigo del paquete"
    t.string "nom_paquete", limit: 200, comment: "descripcion o nombre del paquete"
    t.string "dias", limit: 7, comment: "dias de atencion valores numericos concatenados "
    t.integer "no_paq_aplicado", comment: "Contador que se incrementa cada vez que se hace uso de un paquete"
  end

# Could not dump table "cit_turnos" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora_ini'

  create_table "completa", id: false, force: :cascade do |t|
    t.integer "id_paciente", default: -> { "nextval('cfg_pacientes_id_seq'::regclass)" }, null: false
    t.string "identificacion", limit: 50, null: false
    t.integer "tipo_identificacion"
    t.string "lugar_expedicion", limit: 30
    t.integer "genero"
    t.datetime "fecha_nacimiento"
    t.string "primer_apellido", limit: 30
    t.string "segundo_apellido", limit: 30
    t.string "primer_nombre", limit: 30
    t.string "segundo_nombre", limit: 30
    t.integer "zona"
    t.integer "regimen"
    t.integer "nivel"
    t.boolean "activo"
    t.integer "foto"
    t.integer "firma"
    t.integer "estado_civil"
    t.integer "grupo_sanguineo"
    t.integer "etnia"
    t.integer "escolaridad"
    t.integer "ocupacion"
    t.string "direccion", limit: 80
    t.string "telefono_residencia", limit: 100
    t.string "telefono_oficina", limit: 20
    t.string "celular", limit: 100
    t.integer "tipo_afiliado"
    t.datetime "fecha_afiliacion"
    t.datetime "fecha_sisben"
    t.string "carnet", limit: 40
    t.integer "departamento"
    t.integer "municipio"
    t.text "barrio"
    t.string "responsable", limit: 50
    t.string "acompanante", limit: 50
    t.string "telefono_acompanante", limit: 100
    t.string "historia", limit: 10
    t.datetime "fechainsc"
    t.boolean "facturarlo"
    t.string "dircatastrofe", limit: 50
    t.integer "municipiocatastrofe"
    t.integer "departamentocatastrofe"
    t.string "zonacatastrofe", limit: 1
    t.datetime "fechacatastrofe"
    t.datetime "fechaop"
    t.string "grado", limit: 30
    t.integer "parentesco"
    t.boolean "pensionado"
    t.integer "id_administradora"
    t.integer "dep_nacimiento", comment: "departamento nacimiento"
    t.integer "mun_nacimiento", comment: "municipio nacimiento"
    t.string "email", limit: 300
    t.integer "categoria_paciente"
    t.string "numero_autorizacion", limit: 50
    t.string "telefono_responsable", limit: 20
    t.datetime "fecha_vence_carnet"
    t.text "observaciones"
    t.integer "id_discapacidad"
    t.integer "id_gestacion", comment: "... Si, No, No aplica"
    t.boolean "poblacion_lbgt", comment: "...LBGT si o no"
    t.boolean "desplazado", comment: "...si o no"
    t.integer "id_religion", comment: "... id de la religion"
    t.boolean "victima_maltrato", comment: "...si o no"
    t.boolean "victima_conflicto_armado", comment: "... si o no"
    t.integer "id_contrato"
    t.integer "parentesco_a"
  end

  create_table "fac_administradoras", id: :integer, default: nil, force: :cascade do |t|
    t.string "codigo_administradora", limit: 20
    t.string "razon_social", limit: 200, comment: "NOMBRE DE LA EMPRESA QUE COMPRA EL SOFTWARE"
    t.integer "codigo_departamento"
    t.integer "codigo_municipio"
    t.integer "tipo_documento"
    t.string "numero_identificacion", limit: 20, comment: "NUMERO DE IDENTIFICACION TRIBUTARIA DE LA EMPRESA CLIENTE, ES DECIR DONDE SE INSTALARA EL SOFTWARE"
    t.string "digito_verificacion", limit: 1, comment: "DIGITO DE VERIFICACION. DEBE SER SOLICITADO DE ACUERDO AL TIPO DE DOCUMENTO QUE SE SELECCIONE"
    t.integer "tipo_documento_representante"
    t.string "numero_documento_representante", limit: 20
    t.string "nombre_representante", limit: 200
    t.string "codigo_rip", limit: 20
    t.string "direccion", limit: 200
    t.string "telefono_1", limit: 50
    t.string "telefono_2", limit: 50
    t.string "website", limit: 60
    t.string "codigo_ingreso", limit: 5, comment: "CONCEPTO DE INGRESO PARA ESTA ADMINISTRADORA"
    t.string "codigo_rubro", limit: 20, comment: "CODIGO DEL RUBRO PRESUPUESTAL"
    t.string "codigo_conc", limit: 5
    t.integer "tipo_administradora", comment: "EPS, ARP ...."
  end

  create_table "fac_caja", primary_key: "id_caja", id: :serial, force: :cascade do |t|
    t.string "nombre_caja", limit: 100, comment: "NOMBRE DE LA CAJA"
    t.integer "cfg_sede_id", comment: "SEDE DONDE SE HUBICA LA CAJA"
    t.integer "id_usuario", comment: "USUARIO ASIGNADO A LA CAJA"
    t.float "valor_base_defecto", comment: "VALOR BASE CON QUE ARRANCA LA CAJA"
    t.string "codigo_caja", limit: 10
    t.boolean "cerrada", comment: "determina si la caja esta abierta=false o cerrada=true"
  end

  create_table "fac_consecutivo", primary_key: "id_consecutivo", id: :serial, force: :cascade, comment: "DETERMINA LOS NUMEROS DE FACTURA QUE SE PUEDEN USAR" do |t|
    t.integer "tipo_documento", comment: "CODIGO DEL DOCUMENTO FA->FACTURA OS-> ORDEN DE SERVICIO"
    t.string "resolucion_dian", limit: 200, comment: "SOLO SE APLICA PARA FATURA DE VENTA"
    t.integer "inicio", comment: "NUMERO DESDE CUAL INICIAN LAS FACTURAS"
    t.integer "fin", comment: "NUMERO HASTA CUAL LLEGAN LA FACTURAS"
    t.integer "actual", comment: "NUMERO EN EL CUAL VAN LAS FACTURAS"
    t.text "texto", comment: "Texto que va en las observaciones de la factura"
    t.string "prefijo", limit: 5, comment: "Texto de maximo 5 caracteres que se coloca antes de la numeracion de una factura"
    t.datetime "fecha_sistema", comment: "fecha en que se creo el consecutivo"
  end

  create_table "fac_consumo_insumo", primary_key: "id_consumo_insumo", id: :serial, force: :cascade, comment: "tabla que contiene los insumos aplicados a un paciente y no han sido facturados" do |t|
    t.integer "cfg_paciente_id", null: false
    t.integer "id_prestador", comment: "prestador a quien se le registra el consumo"
    t.integer "id_insumo", comment: "identificador del insumo"
    t.datetime "fecha", null: false
    t.integer "cantidad"
    t.float "valor_unitario", comment: "valor que se cobro en el momento de adicionar el insumo"
    t.float "valor_final", comment: "valor con descuento y cantidad"
    t.string "numero_autorizacion", limit: 15
    t.integer "id_urgordencobro"
  end

  create_table "fac_consumo_medicamento", primary_key: "id_consumo_medicamento", id: :serial, force: :cascade, comment: "tabla que contiene los medicamentos aplicados a un paciente y no han sido facturados" do |t|
    t.integer "cfg_paciente_id", null: false
    t.integer "id_prestador", comment: "prestador a quien se le registra el consumo"
    t.integer "id_medicamento", comment: "identificador del medicamento"
    t.datetime "fecha", null: false
    t.integer "cantidad"
    t.float "valor_unitario", comment: "valor que se cobro en el momento de adicionar el insumo"
    t.float "valor_final", comment: "valor con descuento y cantidad"
    t.integer "id_urgordencobro"
  end

  create_table "fac_consumo_paquete", primary_key: "id_consumo_paquete", id: :serial, force: :cascade, comment: "tabla que contiene los paquetes aplicados a un paciente y no han sido facturados" do |t|
    t.integer "cfg_paciente_id", null: false
    t.integer "id_prestador", comment: "prestador a quien se le registra el consumo"
    t.integer "id_paquete", comment: "identificador del paquete"
    t.datetime "fecha", null: false
    t.integer "cantidad"
    t.float "valor_unitario", comment: "valor que se cobro en el momento de adicionar el insumo"
    t.float "valor_final", comment: "valor con descuento y cantidad"
    t.integer "id_urgordencobro"
  end

  create_table "fac_consumo_servicio", primary_key: "id_consumo_servicio", id: :serial, force: :cascade, comment: "tabla que contiene los servicios aplicados a un paciente y no han sido facturados" do |t|
    t.integer "cfg_paciente_id", null: false
    t.integer "id_prestador", comment: "prestador a quien se le registra el consumo"
    t.integer "id_servicio", comment: "identificador del servicio"
    t.datetime "fecha", null: false
    t.integer "cantidad"
    t.float "valor_unitario", comment: "valor que se cobro en el momento de adicionar el insumo"
    t.float "valor_final", comment: "valor con descuento y cantidad"
    t.string "tipo_tarifa", limit: 11, comment: "SOAT,ISS,Especifica"
    t.text "diagnostico_principal", comment: "diagnostico principal correspondiente al servicio agregado a consumos"
    t.text "diagnostico_relacionado", comment: "diagnostico relacionado correspondiente al servicio agregado a consumos"
    t.string "numero_autorizacion", limit: 15, comment: "autorizacion del servicio"
    t.integer "id_urgordencobro"
    t.string "observacion"
    t.integer "id_profesional_gestiona"
    t.boolean "estado"
    t.datetime "fecha_cancelacion"
  end

  create_table "fac_contrato", primary_key: "id_contrato", id: :serial, force: :cascade do |t|
    t.string "codigo_contrato", limit: 10, comment: "CONSECUTIVO DE CONTRATOS"
    t.integer "id_administradora"
    t.datetime "fecha_inicio", comment: "FECHA DE INICIO DEL CONTRATO"
    t.datetime "fecha_final", comment: "FECHA FINAL DEL CONTRATO"
    t.string "numero_rip_contrato", limit: 10, comment: "NUMERO DEL RIP DEL CONTRATO"
    t.text "descripcion", comment: "DESCRIPCION DEL CONTRATO"
    t.string "numero_poliza", limit: 10, comment: "NUMERO DE POLIZA DEL CONTRATO"
    t.integer "tipo_contrato", comment: "TIPO DE CONTRATO, corresponde al regimen cuando se trata de relacionarlo con paciente"
    t.integer "tipo_pago", comment: "TIPO DE PAGO DEL CONTRATO EVENTO O CAPITADO"
    t.float "upc", comment: "UNIDAD PER CAPITA = EN CASO DE TIPO PAGO SEA CAPITADO SE DEBE ESTABLECER EL VALOR MENSUAL POR PACIENTE ATENDIDO. AL FINAL PARA FACTURAR SE MULTIPLICA LA UPC POR EL NUMERO DE PACIENTES"
    t.integer "numero_afiliados", comment: "NUMERO DE USUARIOS (PACIENTES) QUE PUEDEN SER ATENDIDOS POR EL CONTRATO"
    t.float "valor_contrato", comment: "VALOR TOTAL DEL CONTRATO"
    t.float "valor_mensual", comment: "VALOR MENSUAL A FACTURAR DEL CONTRATO"
    t.float "valor_validacion_mensual", comment: "VALOR VALIDACION MENSUAL DEL CONTRATO, ALERTAR AL USUARIO CUANDO SE LLEGUE A UN TOPE"
    t.float "valor_alarma", comment: "VALOR SOBRE EL CUAL SE DEBE DISPARAR LA ALARMA"
    t.float "porcentaje_descuento_servicios", comment: "PORCENTAJE DE DESCUENTO EN SERVICIOS"
    t.float "porcenteje_descuento_entidad", comment: "PORCENTAJE DE DESCUENTO A LA ENTIDAD"
    t.string "codigo_tarifa_pos", limit: 3, comment: "CODIGO DEL MANUAL TARIFARIO CON EL CUAL SE VA A FACTURAR LOS SERVICIOS O INSUMOS DENTRO DEL POS. EJ: MANUALES SOAT, ISS O ESPECIFICO"
    t.string "codigo_tarifa_nopos", limit: 3, comment: "CODIGO DEL MANUAL TARIFARIO CON EL CUAL SE VA A FACTURAR LOS SERVICIOS O INSUMOS DENTRO DEL NO POS. EJ: MANUALES SOAT, ISS O ESPECIFICO"
    t.boolean "rip", comment: "SI O NO EXIGE DILIGENCIAR RIPS EN LA FACTURA POR LA IPS"
    t.integer "tipo_facturacion", comment: "TIPO DE FACTURACION"
    t.text "observacion_contrato", comment: "OBSERVACION DEL CONTRATO"
    t.text "observacion_facturacion", comment: "OBSERVACION PARA LA FACTURACION DE ESE CONTRATO"
    t.string "cuenta_cobrar", limit: 20, comment: "CUENTA CONTABLE  a la cuenta contable por cobrar donde se contabilizará el movimiento generado por este Contrato en el momento de asentar la factura.   VALIDAR CONTRA PUC"
    t.string "cuenta_copago", limit: 20, comment: "la cuenta contable donde se contabilizarán los copagos de este contrato en el momento de asentar la factura."
    t.string "codigo_concepto", limit: 5, comment: "Concepto de Cartera al cual afectará el movimiento facturado a crédito por este Contrato."
    t.string "codigo_concepto_descuento", limit: 5, comment: "Concepto de Descuento el cual se utilizará en Cartera como descuento"
    t.float "c1", comment: "algo"
    t.float "cm1", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 1"
    t.float "cm2", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 2"
    t.float "cm3", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 3"
    t.float "cm4", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 4"
    t.float "cm5", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 5"
    t.boolean "cmc", comment: "SI APLICA PARA COTIZANTE"
    t.boolean "cmb", comment: "SI APLICA PARA BENEFICIARIO"
    t.float "cp1", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp2", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp3", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp4", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp5", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.boolean "cpc", comment: "COPAGO SI APLICA PARA COTIZANTE"
    t.boolean "cpb", comment: "COPAGO SI APLICA PARA BENEFICIARIO"
    t.float "medicamento_valor_1", comment: "el valor que pagará el usuario por disfrutar cada uno de los Medicamentos asignados a este programa.  Este valor es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "medicamento_valor_2", comment: "el valor que pagará el usuario por disfrutar cada uno de los Medicamentos asignados a este programa.  Este valor es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "medicamento_valor_3", comment: "el valor que pagará el usuario por disfrutar cada uno de los Medicamentos asignados a este programa.  Este valor es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "insumos_porcentaje_1", comment: "el porcentaje que pagará el usuario por disfrutar cada uno de los insumos asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "insumos_porcentaje_2", comment: "El porcentaje que pagará el usuario por disfrutar cada uno de los insumos asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "insumos_porcentaje_3", comment: "el porcentaje que pagará el usuario por disfrutar cada uno de los insumos asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.integer "id_manual_tarifario", comment: "IDENTIFICADOR MANUAL TARIFARIO"
    t.boolean "aplicar_iva", comment: "determinar si se aplica o no iva"
    t.boolean "aplicar_cree", comment: "determinar si se aplica o impuesto cree"
    t.float "porcentaje", comment: "Porcentaje a calcular"
    t.string "signo_porcentaje", limit: 1, comment: "Signo por el cual se va multiplicar el porcentaje (-,+)"
    t.integer "tipo_manual", comment: "1 - Valor Especifico\n2 - ISS\n3 - SOAT"
    t.integer "annio_manual"
    t.integer "campo", comment: "This is my table."
  end

  create_table "fac_factura_admi", primary_key: "id_factura_admi", id: :integer, default: -> { "nextval('fac_factura_admi_id_factura_seq'::regclass)" }, force: :cascade, comment: "TABLA MAESTRO DE UNA FACTURA PARA ADMINISTRADORA" do |t|
    t.integer "tipo_documento", comment: "CODIGO DEL DOCUMENTO FA->FACTURA OS-> ORDEN DE SERVICIO"
    t.string "codigo_documento", limit: 20, comment: "Corresponde al prefijo (fac_consecutivos.prefijo) mas el numero consecutivo(fac_consecutivos.actual+1) correspontiente a este documento ej: FAC-001"
    t.datetime "fecha_sistema", comment: "fecha y hora del servidor en el momento en que se registro la factura "
    t.datetime "fecha_inicial", comment: "Cuando se trata de una factura correspondiente a una administradora se especifica la fecha inicial que se tomo como base para la creación de la factura"
    t.datetime "fecha_final", comment: "Cuando se trata de una factura correspondiente a una administradora se especifica la fecha final que se tomo como base para la creación de la factura"
    t.datetime "fecha_elaboracion", comment: "Fecha y hora de elaboracion de la factura, esta es ingresada por el usuario"
    t.integer "id_periodo", comment: "PERIODO AL QUE PERTENECE UNA FACTURA"
    t.integer "id_contrato", comment: "SI CONTRATO ES NULL CORRESPONDE A TODOS LOS CONTRATOS(se especificara el contrato correspondiente en los items de esta factura)"
    t.text "observacion", comment: "OBSERVACION"
    t.string "resolucion_dian", limit: 200, comment: "si es una facura se almacena la resolucion que tenga en consecutivo"
    t.integer "id_administradora"
    t.float "valores_iva", comment: "sumatoria de aplicar el impuesto iva en cada uno de los items"
    t.float "valores_cree", comment: "sumatoria de aplicar el impuesto cree en cada uno de los items de la factura"
    t.float "valores_copago", comment: "sumatoria de los copagos que contengan las facturas para la administradora en un rango de fechas"
    t.float "valores_cuota_moderadora", comment: "sumatoria de las cuotas moderadoras que contengan las facturas para la administradora en un rango de fechas"
    t.boolean "anulada", comment: "true=anulada false=activa"
    t.float "valor_parcial", comment: "sumatoria de los valores de cada uno de los items de la factura (sin aplicar impuestos)"
    t.float "valor_empresa", comment: "sumatoria del campo valor_empresa que contengan las facturas para la administradora en un rango de fechas"
    t.float "valor_total", comment: "sumatoria de fac_factura.valor_total que contengan las facturas para la administradora en un rango de fechas, (valor_empresa+valor_usuario)"
    t.integer "numero_documento", comment: "entero correspondiente al numero de documento, este es entero, encambio codigo_documento es string y tiene concatenado el prefijo"
    t.text "observacion_anulacion", comment: "observacion que explica del por que una factura fue anulada (opcional)"
    t.float "valor_usuarios", comment: "valor que pagan los usuarios"
    t.integer "clasificacion", comment: "clasificacion de la factura: insumos, servicios"
  end

  create_table "fac_factura_admi_detalle", primary_key: ["id_factura_paciente", "id_factura_admi"], force: :cascade, comment: "tabla intermiedia entre fac_factura_admi y fac_factura_paciente" do |t|
    t.integer "id_factura_paciente", null: false
    t.integer "id_factura_admi", null: false
  end

  create_table "fac_factura_insumo", primary_key: ["id_detalle", "id_factura"], force: :cascade do |t|
    t.integer "id_detalle", null: false, comment: "NUMERO CONSECUTIVO DEL ITEM DENTRO DE LA FACTURA"
    t.integer "id_factura", null: false
    t.integer "id_insumo"
    t.datetime "fecha_insumo", comment: "FECHA EN QUE SE PRESTO EL insumo"
    t.integer "cantidad_insumo", limit: 2, comment: "CANTIDAD QUE SE PRESTARON DEL insumo"
    t.float "valor_insumo", comment: "ESTA EN insumoS PERO SE ALMACENA POR QUE CAMBIA EN EL TIEMPO"
    t.float "valor_parcial", comment: "VALOR PARCIAL = insumo X CANTIDAD"
    t.float "valor_usuario", comment: "VALOR QUE DEBE PAGAR EL USUARIO. campo muestra el valor total que debe pagar el usuario por todos los insumos prestados."
    t.float "valor_empresa", comment: "VALOR QUE DEBE PAGAR LA EMPRESA QUE TIENE EL CONTRATO.  muestra el valor total que debe pagar la empresa contratante por todos los insumos prestados al usuario."
    t.float "valor_iva", comment: "monto correspondiente al iva para este insumo (el porcentage se encuantra a nivel de factura)"
    t.float "valor_cree", comment: "monto correspondiente al impuesto cree para este insumo (el porcentage se encuantra a nivel de factura)"
    t.integer "id_medico", comment: "CODIGO CORRESPONDIENTE AL MEDICO O PROFESIONAL DE LA SALUD QUE ELABORA O PRESTA EL insumo"
    t.string "num_autorizacion", limit: 20, comment: "NUMERO DE LA AUTORIZACION SI SE REQUIERE"
    t.datetime "fecha_autorizacion", comment: "FECHA AUTORIZACION SI SE REQUIERE"
    t.integer "cfg_consultorio_id", comment: "CODIGO DEL CONSULTORIO"
    t.serial "id_sincronizador", null: false
  end

  create_table "fac_factura_medicamento", primary_key: ["id_detalle", "id_factura"], force: :cascade do |t|
    t.integer "id_detalle", null: false, comment: "NUMERO CONSECUTIVO DEL ITEM DENTRO DE LA FACTURA"
    t.integer "id_factura", null: false
    t.integer "id_medicamento"
    t.datetime "fecha_medicamento", comment: "FECHA EN QUE SE PRESTO EL medicamento"
    t.integer "cantidad_medicamento", limit: 2, comment: "CANTIDAD QUE SE PRESTARON DEL medicamento"
    t.float "valor_medicamento", comment: "ESTA EN medicamentoS PERO SE ALMACENA POR QUE CAMBIA EN EL TIEMPO"
    t.float "valor_parcial", comment: "VALOR PARCIAL = medicamento X CANTIDAD"
    t.float "valor_usuario", comment: "VALOR QUE DEBE PAGAR EL USUARIO. campo muestra el valor total que debe pagar el usuario por todos los medicamentos prestados."
    t.float "valor_empresa", comment: "VALOR QUE DEBE PAGAR LA EMPRESA QUE TIENE EL CONTRATO.  muestra el valor total que debe pagar la empresa contratante por todos los medicamentos prestados al usuario."
    t.float "valor_iva", comment: "monto correspondiente al iva para este medicamento (el porcentage usado se encuantra a nivel de factura)"
    t.float "valor_cree", comment: "monto correspondiente al impesto cree para este insumo (el porcentage usado se encuantra a nivel de factura)"
    t.integer "id_medico", comment: "CODIGO CORRESPONDIENTE AL MEDICO O PROFESIONAL DE LA SALUD QUE ELABORA O PRESTA EL medicamento"
    t.string "num_autorizacion", limit: 20, comment: "NUMERO DE LA AUTORIZACION SI SE REQUIERE"
    t.datetime "fecha_autorizacion", comment: "FECHA AUTORIZACION SI SE REQUIERE"
    t.integer "cfg_consultorio_id", comment: "CODIGO DEL CONSULTORIO"
    t.serial "id_sincronizador", null: false
  end

  create_table "fac_factura_paciente", primary_key: "id_factura_paciente", id: :integer, default: -> { "nextval('fac_factura_id_factura_seq'::regclass)" }, force: :cascade, comment: "TABLA MAESTRO DE UNA FACTURA, EL DETALE ESTA EN fac_factura_items" do |t|
    t.integer "id_caja", comment: "CODIGO DE LA CAJA DONDE SE GENERA LA FACTURA. ASOCIADO AL USUARIO"
    t.integer "tipo_documento", comment: "CODIGO DEL DOCUMENTO FA->FACTURA OS-> ORDEN DE SERVICIO"
    t.string "codigo_documento", limit: 20, comment: "Corresponde al prefijo (fac_consecutivos.prefijo) mas el numero consecutivo(fac_consecutivos.actual+1) correspontiente a este documento ej: FAC-001"
    t.datetime "fecha_elaboracion", comment: "Fecha y hora de elaboracion de la factura, esta es ingresada por el usuario"
    t.integer "id_periodo", comment: "CODIGO DEL PACIENTE"
    t.integer "tipo_ingreso", comment: "la forma como entro el usuario a las instalaciones a recibir el (los) servicio (s). Se pueden escoger diferentes tipos de ingreso como: Ambulatorio,Hospitalización, Urgencias."
    t.integer "cfg_paciente_id", comment: "CODIGO DEL PACIENTE"
    t.integer "id_contrato", comment: "NUMERO DE CONTRATO"
    t.string "numero_autorizacion", limit: 20, comment: "NUMERO DE LA AUTORIZACION SI SE REQUIERE"
    t.datetime "fecha_autorizacion", comment: "FECHA DE LA AUTORIZACION SI SE REQUIERE"
    t.text "observacion", comment: "OBSERVACION"
    t.string "resolucion_dian", limit: 200, comment: "si es una facura se almacena la resolucion que tenga en consecutivo"
    t.integer "id_administradora"
    t.float "iva"
    t.float "cree"
    t.float "copago", comment: "valor correspondiente a copago si aplica, se usa para de terminar valor_usuario( copago + cuota_moderadora)"
    t.float "cuota_moderadora", comment: "valor correspondiente a cuota moderadora si aplica, se usa para de terminar valor_usuario( copago + cuota_moderadora)"
    t.boolean "anulada", comment: "true=anulada false=activa"
    t.float "valor_parcial", comment: "Suma de valores finales de todos los items de la factura, no tiene iva ni cree"
    t.float "valor_empresa", comment: "valor que pagara la Administradora del paciente: 1) particular=false <=>  valor_paricial + (valor_parcial*IVA) +  (valor_parcial*CREE) - (copago+cuota moderadora) 2) particular=true <=> valor_empresa=0"
    t.float "valor_total", comment: "valor de la factura (valor usuario + valor empresa):  valor_total=valor_paricial + (valor_parcial*IVA)     + (valor_parcial*CREE)"
    t.integer "numero_documento", comment: "entero correspondiente al numero de documento, este es entero, encambio codigo_documento es string y tiene concatenado el prefijo"
    t.boolean "facturar_como_particular", comment: "Determinar si se usuo el manual tarifario correspondiente a esta administradora o la tarifa fue determinada mediante el manual tarifario particular"
    t.text "observacion_anulacion", comment: "observacion que explica del por que una factura fue anulada (opcional)"
    t.datetime "fecha_sistema", comment: "fecha y hora del servidor en el momento en que se registro la factura "
    t.float "valor_usuario", comment: "valor que pagara el usuario: 1) particular=false <=>  copago+cuota moderadora 2) particular=true <=> 0"
    t.integer "id_cita", comment: "relacion entre factura y la cita por la cual se generó la factura"
    t.boolean "facturada_en_admi", comment: "Determina si esta factura ya fue cobrada o no a la administradora"
    t.integer "ambito"
    t.date "fecha_desde_ambito"
    t.date "fecha_hasta_ambito"
  end

  create_table "fac_factura_paquete", primary_key: ["id_detalle", "id_factura"], force: :cascade do |t|
    t.integer "id_detalle", null: false, comment: "NUMERO CONSECUTIVO DEL ITEM DENTRO DE LA FACTURA"
    t.integer "id_factura", null: false
    t.integer "id_paquete"
    t.datetime "fecha_paquete", comment: "FECHA EN QUE SE PRESTO EL paquete"
    t.integer "cantidad_paquete", limit: 2, comment: "CANTIDAD QUE SE PRESTARON DEL paquete"
    t.float "valor_paquete", comment: "ESTA EN paqueteS PERO SE ALMACENA POR QUE CAMBIA EN EL TIEMPO"
    t.float "valor_parcial", comment: "VALOR PARCIAL = paquete X CANTIDAD"
    t.float "valor_usuario", comment: "VALOR QUE DEBE PAGAR EL USUARIO. campo muestra el valor total que debe pagar el usuario por todos los paquetes prestados."
    t.float "valor_empresa", comment: "VALOR QUE DEBE PAGAR LA EMPRESA QUE TIENE EL CONTRATO.  muestra el valor total que debe pagar la empresa contratante por todos los paquetes prestados al usuario."
    t.float "valor_iva", comment: "monto correspondiente al iva para este paquete (el porcentage usado se encuantra a nivel de factura)"
    t.float "valor_cree", comment: "monto correspondiente al impuesto cree para este paquete (el porcentage usado se encuantra a nivel de factura)"
    t.integer "id_medico", comment: "CODIGO CORRESPONDIENTE AL MEDICO O PROFESIONAL DE LA SALUD QUE ELABORA O PRESTA EL paquete"
    t.string "num_autorizacion", limit: 20, comment: "NUMERO DE LA AUTORIZACION SI SE REQUIERE"
    t.datetime "fecha_autorizacion", comment: "FECHA AUTORIZACION SI SE REQUIERE"
    t.integer "cfg_consultorio_id", comment: "CODIGO DEL CONSULTORIO"
    t.serial "id_sincronizador", null: false
  end

  create_table "fac_factura_servicio", primary_key: ["id_detalle", "id_factura"], force: :cascade do |t|
    t.integer "id_detalle", null: false, comment: "NUMERO CONSECUTIVO DEL ITEM DENTRO DE LA FACTURA"
    t.integer "id_factura", null: false
    t.integer "id_servicio"
    t.datetime "fecha_servicio", comment: "FECHA EN QUE SE PRESTO EL SERVICIO"
    t.integer "cantidad_servicio", limit: 2, comment: "CANTIDAD QUE SE PRESTARON DEL SERVICIO"
    t.float "valor_servicio", comment: "ESTA EN SERVICIOS PERO SE ALMACENA POR QUE CAMBIA EN EL TIEMPO"
    t.float "valor_parcial", comment: "VALOR PARCIAL = SERVICIO X CANTIDAD"
    t.float "valor_usuario", comment: "VALOR QUE DEBE PAGAR EL USUARIO. campo muestra el valor total que debe pagar el usuario por todos los servicios prestados."
    t.float "valor_empresa", comment: "VALOR QUE DEBE PAGAR LA EMPRESA QUE TIENE EL CONTRATO.  muestra el valor total que debe pagar la empresa contratante por todos los servicios prestados al usuario."
    t.float "valor_iva", comment: "monto correspondiente al iva para este servicio (el porcentage usado se encuantra a nivel de factura)"
    t.float "valor_cree", comment: "monto correspondiente al impuesto cree para este servicio (el porcentage usado se encuantra a nivel de factura)"
    t.integer "id_medico", comment: "CODIGO CORRESPONDIENTE AL MEDICO O PROFESIONAL DE LA SALUD QUE ELABORA O PRESTA EL SERVICIO"
    t.integer "cfg_consultorio_id", comment: "CODIGO DEL CONSULTORIO"
    t.text "diagnostico_principal", comment: "diagnostico principal relacionado al servicio prestado"
    t.text "diagnostico_relacionado", comment: "diagnostico relacionado al servicio prestado"
    t.serial "id_sincronizador", null: false
  end

  create_table "fac_impuestos", primary_key: "id_impuesto", id: :serial, force: :cascade do |t|
    t.string "nombre", limit: 50, comment: "Determina el tipo de impuesto: CREE, IVA "
    t.float "valor"
    t.date "fecha_inicial"
    t.date "fecha_final"
  end

  create_table "fac_manual_tarifario", primary_key: "id_manual_tarifario", id: :serial, comment: "IDENTIFICADOR MANUAL TARIFARIO", force: :cascade do |t|
    t.string "codigo_manual_tarifario", limit: 50, comment: "CODIGO MANUAL TARIFARIO"
    t.string "nombre_manual_tarifario", limit: 150, comment: "DESCRIPCION MANUAL TARIFIARIO"
  end

  create_table "fac_manual_tarifario_insumo", primary_key: ["id_manual_tarifario", "id_insumo"], force: :cascade do |t|
    t.integer "id_manual_tarifario", null: false, comment: "ID MANUAL TARIFARIO"
    t.integer "id_insumo", null: false, comment: "INSUMO CORRESPONDIETE QUE SE SUMINISTRA, TABLA cfg_insumo"
    t.float "descuento", comment: "DESCUENTO ESPECIFICO SOBRE EL INSUMO"
    t.text "observacion", comment: "CAMPO USADO PARA LA GENERACION DE FACTURAS EN DONDE SE ESPECIFICA UN NOMBRE PARA CADA INSUMO SUMINSTRADO"
    t.boolean "activo", comment: "DETERMINAR SI SE USA O NO ESTE INSUMO EN EL MANUAL TARIFARIO"
    t.float "valor_inicial", comment: "valor sin descuento y obtenido de la tabla insumos en el momento que se agrego el insumo al manual tarifario"
    t.float "valor_final", comment: "valor final, teniendo en cuenta el descuento, o que fue ingresado por el usuario para ese manual tarifario"
  end

  create_table "fac_manual_tarifario_medicamento", primary_key: ["id_manual_tarifario", "id_medicamento"], force: :cascade do |t|
    t.integer "id_manual_tarifario", null: false, comment: "ID MANUAL TARIFARIO"
    t.integer "id_medicamento", null: false, comment: "MEDICAMENTO CORRESPONDIETE QUE SE SUMINISTRA, TABLA cfg_insumo"
    t.float "descuento", comment: "DESCUENTO ESPECIFICO SOBRE EL MEDICAMENTO"
    t.text "observacion", comment: "CAMPO USADO PARA LA GENERACION DE FACTURAS EN DONDE SE ESPECIFICA UN NOMBRE PARA CADA MEDICAMENTO SUMINSTRADO"
    t.boolean "activo", comment: "DETERMINAR SI SE USA O NO ESTE MEDICAMENTO EN EL MANUAL TARIFARIO"
    t.float "valor_inicial", comment: "valor sin descuento y obtenido de la tabla medicamentos en el momento que se agrego el medicamento al manual tarifario"
    t.float "valor_final", comment: "valor final que tiene en cuenta el descuento, o que fue ingresado por el usuario para este manual tarifario"
  end

  create_table "fac_manual_tarifario_paquete", primary_key: ["id_manual_tarifario", "id_paquete"], force: :cascade, comment: "RELACION MANUAL TARIFARIO CON SERVICIO" do |t|
    t.integer "id_manual_tarifario", null: false, comment: "ID MANUAL TARIFARIO"
    t.integer "id_paquete", null: false, comment: "PAQUETE CORRESPONDIETE , TABLA fac_paquete"
    t.float "descuento"
    t.text "observacion"
    t.boolean "activo"
    t.float "valor_inicial", comment: "valor sin descuento y obtenido de la tabla paquetes en el momento que se agrego el paquete al manual tarifario"
    t.float "valor_final", comment: "valor final que tiene en cuenta el descuento, o que fue ingresado por el usuario para este manual tarifario"
  end

  create_table "fac_manual_tarifario_servicio", primary_key: ["id_manual_tarifario", "id_servicio"], force: :cascade, comment: "RELACION MANUAL TARIFARIO CON SERVICIO" do |t|
    t.integer "id_manual_tarifario", null: false, comment: "ID MANUAL TARIFARIO"
    t.integer "id_servicio", null: false, comment: "SERVICIO CORRESPONDIETE QUE SE PRESTA, TABLA fac_servico"
    t.integer "meta_cumplimiento", comment: "NUMERO DE VECES QUE LA ENTIDAD TIENE COMO META LA APLICACION DE ESTE SERVICIO"
    t.integer "periodicidad", comment: "NUMERO QUE INDICA CADA CUANTOS MESES SE DEBE CUMPLIR LA META"
    t.float "descuento", comment: "DESCUENTO ESPECIFICO SOBRE EL SERVICIO"
    t.float "honorario_medico", comment: "VALOR QUE SE LE VA A CANCELAR AL MEDICO POR LA PRESTACION DEL SERVICIO, SIEMPRE Y CUANDO MANEJE HONORARIOS MEDICOS."
    t.text "observacion", comment: "CAMPO USADO PARA LA GENERACION DE FACTURAS EN DONDE SE ESPECIFICA UN NOMBRE PARA CADA SERVICIO PRESTADO"
    t.boolean "activo", comment: "DETERMINAR SI SE USA O NO ESTE SERVICIO EN EL MANUAL TARIFARIO"
    t.string "tipo_tarifa", limit: 11, comment: "Forma en que se calcula el valor: \nSOAT: en base al SMLVD\nISS:  en base al UVR\nESPECIFICA: valor dado por el usuario"
    t.float "valor_inicial", comment: "valor sin descuento y obtenido de la tabla servicios en el momento que se agrego el servicio al manual tarifario"
    t.float "valor_final", comment: "valor final que tiene en cuenta el descuento, o que fue ingresado por el usuario para este manual tarifario"
    t.integer "anio_unidad_valor", comment: "año correspondiente en la tabla unidad_valor, para determinar que SMLVD o UVR se usa"
  end

  create_table "fac_movimiento_caja", primary_key: "id_movimiento", id: :serial, comment: "identificador", force: :cascade do |t|
    t.boolean "abrir_caja", comment: "TRUE: APERTURA DE CAJA - FALSE: CIERRE DE CAJA"
    t.float "valor"
    t.time "fecha"
    t.integer "id_caja", comment: "identificador de la caja correspondiente"
  end

  create_table "fac_paquete", primary_key: "id_paquete", id: :serial, comment: "IDENTIFICADOR PAQUETE", force: :cascade do |t|
    t.string "codigo_paquete", limit: 50, comment: "CODIGO PAQUETE"
    t.string "nombre_paquete", limit: 150, comment: "DESCRIPCION PAQUETE"
    t.float "valor_paquete", comment: "valor del paquete pero  ingresado por el usuario "
    t.float "valor_calculado", comment: "valor que se calcula por los items del paquete"
    t.boolean "aplica_iva"
    t.boolean "aplica_cree"
  end

  create_table "fac_paquete_insumo", primary_key: ["id_paquete", "id_insumo"], force: :cascade do |t|
    t.integer "id_paquete", null: false, comment: "ID PAQUETE"
    t.integer "id_insumo", null: false, comment: "INSUMO CORRESPONDIETE QUE SE SUMINISTRA, TABLA cfg_insumo"
    t.float "valor_final", comment: "valor final que tiene en cuenta el descuento y cantidad, o que fue ingresado por el usuario para este paquete"
    t.float "descuento", comment: "DESCUENTO ESPECIFICO SOBRE EL INSUMO"
    t.text "observacion", comment: "CAMPO USADO PARA LA GENERACION DE FACTURAS EN DONDE SE ESPECIFICA UN NOMBRE PARA CADA INSUMO SUMINSTRADO"
    t.boolean "activo", comment: "DETERMINAR SI SE USA O NO ESTE INSUMO EN EL MANUAL TARIFARIO"
    t.integer "cantidad"
    t.float "valor_unitario"
  end

  create_table "fac_paquete_medicamento", primary_key: ["id_paquete", "id_medicamento"], force: :cascade do |t|
    t.integer "id_paquete", null: false, comment: "ID MANUAL PAQUETE"
    t.integer "id_medicamento", null: false, comment: "MEDICAMENTO CORRESPONDIETE QUE SE SUMINISTRA, TABLA cfg_insumo"
    t.float "valor_final", comment: "valor final que tiene en cuenta el descuento y cantidad, o que fue ingresado por el usuario para este paquete"
    t.float "descuento", comment: "DESCUENTO ESPECIFICO SOBRE EL MEDICAMENTO"
    t.text "observacion", comment: "CAMPO USADO PARA LA GENERACION DE FACTURAS EN DONDE SE ESPECIFICA UN NOMBRE PARA CADA MEDICAMENTO SUMINSTRADO"
    t.boolean "activo", comment: "DETERMINAR SI SE USA O NO ESTE MEDICAMENTO EN EL MANUAL TARIFARIO"
    t.integer "cantidad"
    t.float "valor_unitario"
  end

  create_table "fac_paquete_servicio", primary_key: ["id_paquete", "id_servicio"], force: :cascade, comment: "RELACION PAQUETE CON SERVICIO" do |t|
    t.integer "id_paquete", null: false, comment: "ID MANUAL PAQUETE"
    t.integer "id_servicio", null: false, comment: "SERVICIO CORRESPONDIETE QUE SE PRESTA, TABLA fac_servico"
    t.float "valor_final", comment: "valor final que tiene en cuenta el descuento y cantidad, o que fue ingresado por el usuario para este paquete"
    t.integer "meta_cumplimiento", comment: "NUMERO DE VECES QUE LA ENTIDAD TIENE COMO META LA APLICACION DE ESTE SERVICIO"
    t.integer "periodicidad", comment: "NUMERO QUE INDICA CADA CUANTOS MESES SE DEBE CUMPLIR LA META"
    t.float "descuento", comment: "DESCUENTO ESPECIFICO SOBRE EL SERVICIO"
    t.float "honorario_medico", comment: "VALOR QUE SE LE VA A CANCELAR AL MEDICO POR LA PRESTACION DEL SERVICIO, SIEMPRE Y CUANDO MANEJE HONORARIOS MEDICOS."
    t.text "observacion", comment: "CAMPO USADO PARA LA GENERACION DE FACTURAS EN DONDE SE ESPECIFICA UN NOMBRE PARA CADA SERVICIO PRESTADO"
    t.boolean "activo", comment: "DETERMINAR SI SE USA O NO ESTE SERVICIO EN EL MANUAL TARIFARIO"
    t.string "tipo_tarifa", limit: 11, comment: "Forma en que se calcula el valor: \nSOAT: en base al SMLVD\nISS:  en base al UVR\nESPECIFICA: valor dado por el usuario"
    t.integer "cantidad"
    t.float "valor_unitario"
  end

  create_table "fac_periodo", primary_key: "id_periodo", id: :serial, force: :cascade, comment: "TABLA DE PERIODOS DE FACTURACION" do |t|
    t.integer "anio", comment: "AÑO AL QUE CORRESPONDE EL PERIODO"
    t.integer "mes", comment: "MES AL QUE CORRESPONDE EL PERIODO"
    t.string "nombre", limit: 6
    t.date "fecha_inicial"
    t.date "fecha_final"
    t.date "fecha_limite"
    t.boolean "cerrado"
  end

  create_table "fac_programa", primary_key: "id_programa", id: :serial, force: :cascade do |t|
    t.string "codigo_programa", limit: 10, null: false, comment: "CODIGO DEL PROGRAMA"
    t.integer "id_contrato", comment: "NUMERO DE CONTRATO"
    t.string "nombre_programa", limit: 150, comment: "NOMBRE DEL PROGRAMA"
    t.boolean "activo", comment: "INDICA SI EL PROGRAMA ESTA ACTIVO O NO."
    t.float "cm1", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 1"
    t.float "cm2", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 2"
    t.float "cm3", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 3"
    t.float "cm4", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 4"
    t.float "cm5", comment: "VALOR QUE PAGARA EL USUARIO PARA NIVEL 5"
    t.boolean "cmc", comment: "SI APLICA PARA COTIZANTE"
    t.boolean "cmb", comment: "SI APLICA PARA BENEFICIARIO"
    t.float "cp1", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp2", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp3", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp4", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "cp5", comment: "COPAGO. el porcentaje que pagará el usuario por disfrutar cada uno de los servicios asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.boolean "cpc", comment: "COPAGO SI APLICA PARA COTIZANTE"
    t.boolean "cpb", comment: "COPAGO SI APLICA PARA BENEFICIARIO"
    t.float "medicamento_valor_1", comment: "el valor que pagará el usuario por disfrutar cada uno de los Medicamentos asignados a este programa.  Este valor es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "medicamento_valor_2", comment: "el valor que pagará el usuario por disfrutar cada uno de los Medicamentos asignados a este programa.  Este valor es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "medicamento_valor_3", comment: "el valor que pagará el usuario por disfrutar cada uno de los Medicamentos asignados a este programa.  Este valor es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "insumos_porcentaje_1", comment: " el porcentaje que pagará el usuario por disfrutar cada uno de los insumos asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "insumos_porcentaje_2", comment: "El porcentaje que pagará el usuario por disfrutar cada uno de los insumos asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.float "insumos_porcentaje_3", comment: " el porcentaje que pagará el usuario por disfrutar cada uno de los insumos asignados a este programa.  Este valor del porcentaje es asignado según el nivel que tenga el usuario dentro del Contrato."
    t.integer "finalidad_consulta", comment: "FINALIDAD DE LA CONSULTA correspondiente a los Servicios de  Consulta que está insertando para este programa.   el listado de los tipos que puede escoger (Según Resolución 3374)."
    t.integer "causa_externa", comment: "causa externa correspondiente a los Servicios de Consulta que está insertando para este programa.  Si desea desplegar los  diferentes tipos de causas externas de clic en el botón  y aparecerá el listado de los tipos que puede escoger (Según Resolución 3374)."
    t.integer "finalidad_procedimineto", comment: "FINALIDA DEL PROCEDIMIENTO.  ar la finalidad correspondiente a los Servicios de Procedimiento que está insertando para este programa.  Si desea desplegar  los diferentes tipos de finalidad de clic en el botón  y aparecerá el listado de los tipos que puede escoger (Según Resolución 3374)."
    t.string "codigo_diagnostico", limit: 10, comment: "CODIGO DEL DIAGNOSTICO DE ACUERDO AL CIE 10"
    t.integer "id_manual_tarifario"
  end

  create_table "fac_servicio", primary_key: "id_servicio", id: :serial, force: :cascade do |t|
    t.string "codigo_servicio", limit: 20, comment: "CODIGO DEL SERVICIO"
    t.string "codigo_cup", limit: 10, comment: "CODIGO UNICO DE PROCEDIMIENTO PARA ESTE SERVICIO DE SALUD.  CAMPO OPCIONAL PERO NECESARIO PARA LA GENERACION DEL ARCHIVO PLANO DE RIPS"
    t.string "codigo_cums", limit: 10, comment: "código único de Medicamentos siempre y cuando sea un servicio de tipo Medicamentos."
    t.string "codigo_soat", limit: 10, comment: " código normalizado SOAT para este Servicio de Salud.  Campo opcional pero necesario para la generación del archivo plano de RIPS."
    t.string "codigo_iss", limit: 10, comment: "código normalizado ISS para este Servicio de Salud.  Campo opcional pero necesario para la generación del archivo plano de RIPS"
    t.string "nombre_servicio", limit: 500, comment: "Nombre o la descripción completa del Servicio de Salud."
    t.integer "grupo_servicio", comment: "código correspondiente al Grupo de Servicio al cual corresponde el Servicio que se está configurando."
    t.integer "tipo_sevicio"
    t.integer "especialidad", comment: "codigo especialidad (la tabla esta en configuracion)"
    t.integer "centro_costo", comment: "tabla cfg_centro de costo"
    t.integer "sexo", comment: "M- MASCULINO F- FEMENINO A-AMBOS"
    t.integer "edad_inicial", comment: "EDAD A PARTIR DE LA CUAL SE PUEDE APLICAR EL PROGRAMA"
    t.integer "unidad_edad_inicial", comment: "UNIDAD DE MEDIDA DE LA EDAD D- DIAS M- MESES A-AÑOS"
    t.integer "edad_final", comment: "EDAD A MAXIMA A LA CUAL SE PUEDE APLICAR EL PROGRAMA"
    t.integer "unidad_edad_final", comment: "UNIDAD DE MEDIDA DE LA EDAD D- DIAS M- MESES A-AÑOS"
    t.boolean "tipo_pos", comment: "TIPO DE SERVICIO POS (EN 1) O NO POS (0)"
    t.float "porcentaje_honorarios", comment: "porcentaje a que equivalen los Honorarios que genera el Servicio de Salud que se está configurando.  Se usa para discriminar el porcentaje de Honorarios que tiene involucrado la realización de un Servicio."
    t.string "tipo_servicio_furip", limit: 2, comment: "TIPO DE SERVICIO FURIPS AL CUAL APLICARA EL SERVICIO"
    t.string "rip_aplica", limit: 1, comment: "Los tipos que puede escoger entre los cuales se encuentran: RIP de Consulta, RIP de Procedimiento, RIP de Urgencias, RIP de Hospitalización, RIP de Recién Nacidos, RIP de Medicamentos y RIP de Otros Servicios."
    t.string "codigo_diagnostico", limit: 10, comment: "CODIGO DIAGNOSTICO"
    t.boolean "autorizacion", comment: "requiere o no autorizacion"
    t.boolean "visible"
    t.float "valor_particular"
    t.float "factor_soat", comment: "se multiplica este factor por el smlvd para determinar el valor"
    t.float "factor_iss", comment: "se multiplica este factor por el UVR para determinar el valor de este servicio"
    t.boolean "aplica_iva"
    t.boolean "aplica_cree"
    t.integer "zona", comment: "zona a la que aplica este servicio"
    t.integer "finalidad", comment: "La finalidad correspondiente al Servicio de consulta que está insertando.  Si desea desplegar los diferentes tipos de finalidad de clic en el botón  aparecerá el listado de los tipos que puede escoger (Según Resolución 3374)."
    t.integer "acto_quirurgico", comment: "si lo tiene correspondiente al Servicio de procedimiento que está insertando.  Si desea desplegar los diferentes tipos de actos quirúrgicos de clic en el botón  y aparecerá el listado de los tipos que puede escoger (Según Resolución 3374).  Diagnóstico: Introduzca el código correspondiente"
    t.integer "ambito", comment: "identificador del Ambito de realización del procedimiento"
    t.boolean "examen", comment: "Para distinguir si se trata de un examen clinico"
  end

  create_table "fac_smlv", primary_key: "annio", id: :integer, default: nil, force: :cascade do |t|
    t.float "valor_salario"
  end

  create_table "fac_unidad_valor", primary_key: "anio", id: :integer, default: nil, force: :cascade, comment: "TABLA QUE ALMACENA SALARIOS MINIMOS Y VALORES DE UVR EN UNA FECHA DETERMINADA INGRESADA POR EL USUARIO" do |t|
    t.float "smlvd"
    t.float "uvr"
    t.float "smlvm"
  end

  create_table "hc_archivos", primary_key: "id_archivo", force: :cascade do |t|
    t.text "url_file"
    t.integer "cfg_paciente_id"
    t.date "fecha_upload"
    t.text "descripcion"
  end

  create_table "hc_campos_reg", primary_key: "id_campo", id: :serial, force: :cascade, comment: "Lista de cada uno de los campos que contiene un tipo de registro clinico" do |t|
    t.integer "id_tipo_reg"
    t.string "nombre", limit: 100
    t.string "tabla", limit: 50, comment: "determinar cual tabla de la base de datos se usa de ser necesario"
    t.integer "posicion", comment: "este campo permite identificar la posicion que ocupa Cuando se crea un arreglo con todos los campos de un registro clinico"
    t.text "nombre_pdf", comment: "Contine el nombre que saldra en el PDF para este campo"
  end

  create_table "hc_detalle", primary_key: ["id_registro", "id_campo"], force: :cascade, comment: "contiene los datos adicionales de un registro clinico ingresado\n" do |t|
    t.integer "id_registro", null: false
    t.integer "id_campo", null: false
    t.text "valor"
    t.serial "id_sincronizador", null: false
  end

  create_table "hc_estructura_familiar", id: false, force: :cascade, comment: "Lista de familiares del paciente" do |t|
    t.serial "id_estructura_familiar", null: false
    t.integer "cfg_paciente_id", null: false
    t.integer "edad"
    t.string "nombre", limit: 100
    t.integer "id_parentesco"
    t.integer "id_ocupacion"
  end

  create_table "hc_items", primary_key: "id_item", id: :serial, force: :cascade, comment: "se usa cuando un registro contiene listas de items; ejemplo:\n  1 recetario          <->   N medicamentos\n  1 orden de servicio  <->   N servicios" do |t|
    t.integer "id_registro"
    t.string "tabla", limit: 50, comment: "tabla en que se debe buscar el valor; ejemplo: mediacamentos, servicios, etc..."
    t.string "id_tabla", limit: 50, comment: "iidentificador en la tabla que se realiza la busuqeda"
    t.integer "cantidad"
    t.text "observacion"
    t.string "dosis", limit: 50
    t.string "posologia", limit: 100
  end

  create_table "hc_registro", primary_key: "id_registro", id: :serial, force: :cascade, comment: "contiene los datos principales de un registro clinico ingresado, cada que se registra una historia clinica se ingresa una nueva fila en esta tabla, y en la tabla hc_detalle entran N filas (la cantidad de campos que contenga el formulario), maestro=> detalle    hc_registro=>hc_detalle" do |t|
    t.integer "id_tipo_reg"
    t.integer "id_paciente"
    t.integer "id_medico"
    t.datetime "fecha_reg"
    t.datetime "fecha_sis"
    t.integer "id_cita", comment: "relacion entre registro clinico y la cita en la cual se geneó el registro clinico"
    t.integer "folio", comment: "numero consecutivo que indica cuantos registros clinicos tiene el paciente\n"
  end

  create_table "hc_rep_examenes", primary_key: "id_rep_examenes", id: :serial, force: :cascade do |t|
    t.integer "id_registro"
    t.integer "posicion"
    t.integer "nombre_paraclinico"
    t.date "fecha"
    t.text "resultado"
  end

  create_table "hc_tipo_reg", primary_key: "id_tipo_reg", id: :serial, force: :cascade, comment: "Lista de los diferentes tipos de registros clinicos que maneja la entidad" do |t|
    t.string "nombre", limit: 100
    t.string "url_pagina", limit: 200
    t.boolean "activo", comment: "determinar si se usa o no"
    t.integer "cant_campos", comment: "cantidad de campos que contiene este tipo de registro clinico (se usa para saber el tamaño del vector que representara un registro)"
    t.integer "consecutivo", comment: "Consecutivo que indica la cantidad de registros que se llevan de este tipo de registro"
  end

  create_table "inv_articulo", id: false, force: :cascade, comment: "Tabla donde se crean articulos de todo tipo que se maneje en la empresa. Ejemplo: Artiulos de aseo, medicamentos, papeleria, etc" do |t|
    t.integer "idsubGrupo", comment: "identificador del subgrupo al cual pertenece el articulo"
    t.integer "idArticulo", comment: "Identificador de la tabla - incremental"
    t.string "codArticulo", limit: 100, comment: "Codigo del artiuclo definido por el usuario"
    t.string "codbarArticulo", limit: 100, comment: "Codigo de barras del articulo para agilizarlos ingresos y egresos de los articulos"
    t.string "nomArticulo", limit: 150, comment: "Nombre del articulo suministrado por el usuario"
    t.string "nomGenericoArticulo", limit: 150, comment: "Nombre generico del articulo"
    t.integer "costoArticulo", comment: "Valor al cual se compra el articulo"
    t.integer "stockMinimo", comment: "Cantidad minima que se debe tener del articulo, la cual debe generar alertas al usuario."
    t.integer "idUnidadMedida", comment: "Identificador de la unidad de medida. Ejemplo: Litros, Kilos, gramos,etc."
    t.integer "idPresentacin", comment: "identificador de la forma de presentacion del articulo. Ejemplo: Inyeccion, Polvo, inyeccion, unguento, etc."
    t.bit "posArticulo", limit: 1, comment: "indica si el producto es POS (Plan Obligatorio de salud)"
    t.bit "Activo", limit: 1, comment: "indica si el producto esta activo o no"
    t.integer "concentracion", comment: "Concretacion que maneja el articulo"
    t.string "registroSanitario", limit: 150, comment: "Registro sanitario que maneja el articulo "
    t.integer "ultCostoArticulo", comment: "Ultimo costo del articulo. Este campo se actualiza cuando el costo se actualiza"
    t.integer "stockMaximo", comment: "Maximo numero de unidades de un articulo"
    t.bit "vencimiento", limit: 1, comment: "Indica si el articulo maneja vencimiento o no."
    t.bit "lote", limit: 1, comment: "Indicador si maneja lote o no"
    t.string "principioActivo", limit: 150, comment: "Almacena el principio activo si es medicamento"
    t.string "accionFarmacologica", limit: 160, comment: "Indica cual es el uso o proposito o para que sirve en caso de que sea medicamento"
    t.integer "idLaboratorio", comment: "Identifica el laboratorio el cual fabrica si es medicamento"
  end

  create_table "inv_bodega", id: false, force: :cascade, comment: "Tabla donde se crean las diferentes bodegas que maneja la empresa" do |t|
    t.integer "idSede", null: false, comment: "identificador de la tabla de bodegas"
    t.integer "idBodega", comment: "Identificador de la tabla de bodegas"
    t.integer "codBodega", comment: "Codigo de la bodega definido por el usuario"
    t.string "nomBodega", limit: 1, comment: "Nombre de la bodega suministrado por el usuario"
  end

  create_table "inv_bodega_productos", primary_key: "id_bodega_producto", id: :serial, force: :cascade do |t|
    t.integer "id_bodega"
    t.integer "id_producto"
    t.float "existencia"
    t.integer "id_lote"
  end

  create_table "inv_bodegas", primary_key: "id_bodega", id: :integer, default: -> { "nextval('inv_bodegas_id_bodeha_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_sede"
    t.integer "tipo", comment: "1. Bodega\n2. Farmacia"
    t.string "codigo_bodega", limit: 20
    t.string "nombre", limit: 40
    t.integer "responsable"
    t.boolean "principal"
    t.boolean "activo"
    t.integer "id_bodega_padre"
    t.bigint "cfg_empresa_id"
  end

  create_table "inv_categorias", primary_key: "id_categoria", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 10
    t.string "nombre", limit: 50
    t.string "observacion", limit: 100
    t.boolean "medicamentos_insumos", comment: "Permite clasificar las categorías que pertencen al grupos de medicamentos o insumos"
    t.boolean "activo"
  end

  create_table "inv_consecutivos", id: :integer, default: nil, force: :cascade do |t|
    t.integer "consecutivo"
    t.string "prefijo", limit: 5
    t.string "tipo", limit: 1, comment: "B Bodega\nF Farmacia"
    t.string "observaciones", limit: 100
  end

  create_table "inv_entrega_medicamentos", primary_key: "id_entrega", id: :serial, force: :cascade do |t|
    t.string "numero_entrega", limit: 10
    t.datetime "fecha_entrega"
    t.integer "id_paciente"
    t.integer "id_sede"
    t.integer "id_historia_clinica"
    t.integer "id_bodega"
    t.string "estado", limit: 1
    t.integer "usuario_elabora"
    t.integer "usuario_actualiza"
    t.datetime "fecha_actualizacion"
    t.string "observaciones", limit: 1000
    t.bigint "cfg_empresa_id"
  end

  create_table "inv_entrega_medicamentos_detalle", primary_key: "id_entrega_detalle", id: :serial, force: :cascade do |t|
    t.integer "id_entrega"
    t.integer "id_producto"
    t.float "cantidad_recetada"
    t.float "cantidad_recibida"
    t.string "observaciones", limit: 1000
  end

  create_table "inv_grupoArticulo", id: false, force: :cascade, comment: "Tabla donde se almacenara los diferentes grupos para los articulos que se manejen en la empresa" do |t|
    t.interval "idGrupoArticulo", comment: "Identificador de la tabla"
    t.integer "codGrupoArticulo", comment: "Codigo del grupo de los articulos definido por el usuario. "
    t.string "nomGrupoArticulo", limit: 1, comment: "Nombre del grupo de articulos definidos por el usuario. Ejemplo: Medicamentos, Aseo, etc."
  end

  create_table "inv_lote_productos", primary_key: "id_lote_producto", id: :serial, force: :cascade do |t|
    t.integer "id_lote", null: false
    t.integer "id_producto", null: false
  end

  create_table "inv_lotes", primary_key: "id_lote", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 10, null: false
    t.date "fecha_vencimiento", null: false
    t.string "observacion", limit: 2000
    t.datetime "fecha_creacion"
    t.integer "usuario_crea"
  end

  create_table "inv_movimiento_productos", primary_key: "id_movimiento_producto", id: :serial, force: :cascade do |t|
    t.integer "id_movimiento"
    t.integer "id_producto"
    t.float "cantidad_solicitada"
    t.float "cantidad_recibida"
    t.float "existencia"
  end

  create_table "inv_movimientos", primary_key: "id_movimiento", id: :serial, force: :cascade, comment: "Tabla donde se almacenara los diferentes movimientos positivos o negativos del inventario" do |t|
    t.string "tipo_movimiento", limit: 1, comment: "E:Entrada - S:Salida"
    t.string "numero_documento", limit: 10
    t.datetime "fecha_movimiento"
    t.integer "id_orden_compra"
    t.string "numero_documento_proveedor", limit: 20
    t.string "observaciones", limit: 2000
    t.integer "usuario_aprueba"
    t.datetime "fecha_aprobacion"
    t.integer "id_bodega_origen", comment: "Bodega de donde provienen los productos, para el ingreso de orden esta se toma como principal"
    t.integer "id_bodega_destino"
    t.integer "id_movimiento_origen", comment: "Id movimiento, por el cual se genera un movimiento de entrada proveniente de una salida"
    t.string "estado", limit: 1, comment: "C:Cerrada, P:Pendiente, N:Anulada"
    t.integer "id_usuario_crea"
    t.integer "tipo_proceso", comment: "Proceso derivado del tipo de movimiento, 1: Traslados, 2: Saldo Inicial,3:Ajuste Positivo,4 Devolución Compra,5:Ingreso por compra,6:Ajustes Negativos,7:Devoluciones Compras,8:Entrega Medicamentos,9:Ventas"
    t.integer "id_entrega", comment: "Permite asociar una entrega de medicamentos al movimiento"
    t.bigint "cfg_empresa_id"
  end

  create_table "inv_orden_compra", primary_key: "id_orden_compra", id: :integer, default: -> { "nextval('inv_orden_compra_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_proveedor"
    t.datetime "fecha"
    t.string "nro_documento", limit: 15
    t.string "observaciones", limit: 2000
    t.float "subtotal"
    t.float "descuento"
    t.float "iva"
    t.float "total"
    t.string "estado", limit: 1
    t.integer "usuario_crea"
    t.datetime "fecha_creacion"
    t.integer "usuario_actualiza"
    t.datetime "fecha_actualizacion"
    t.integer "id_lote"
    t.bigint "cfg_empresa_id"
  end

  create_table "inv_orden_compra_productos", primary_key: "id_orden_compra_producto", id: :integer, default: -> { "nextval('inv_proveedor_productos_id_proveedor_producto_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_orden_compra"
    t.integer "id_producto"
    t.float "cantidad"
    t.float "precio_unidad"
    t.float "descuento"
    t.float "cantidad_entregada"
  end

  create_table "inv_productos", primary_key: "id_producto", id: :integer, default: -> { "nextval('inv_productos_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_categoria", null: false
    t.string "codigo", limit: 20, null: false
    t.string "codigo_cums", limit: 20
    t.string "nombre", limit: 100, null: false
    t.string "nombre_generico"
    t.string "registro_sanitario", limit: 30
    t.float "costo"
    t.integer "stock_minimo"
    t.boolean "lote"
    t.integer "id_unidad_medida"
    t.integer "id_via_administracion"
    t.boolean "producto_pos"
    t.boolean "activo"
    t.integer "id_presentacion", comment: "Presentación del producto (crema,jarabe,liquido en barra,etc)"
    t.boolean "medicamento", comment: "Marcamos el producto como medicamento para mejorar filtros"
    t.float "precio_venta", default: 0.0
    t.integer "stock_maximo", default: 0
  end

  create_table "inv_proveedor", id: false, force: :cascade, comment: "Tabla donde se almacena los proveedores de los productos" do |t|
    t.integer "idProveedor", comment: "Identificador de la tabla proveedor"
    t.string "codProveedor", limit: 100, comment: "Codigo del proveedor configurado por el usuario"
    t.integer "idTipodDcumento", comment: "Identiicador del tipo de documento puede ser cedula,pasaporte,rut,etc"
    t.string "numIdentificacion", limit: 20, comment: "Numero de la identificacion del proveedor"
    t.string "nomProveedor", limit: 150, comment: "Nombre del proveedor"
    t.string "dirProveedor", limit: 150, comment: "Direccion del proveedor\n"
    t.string "telefono1", limit: 10, comment: "Numero 1 del telefono del proveedor"
    t.string "telefono2", limit: 10, comment: "Numero 2 del proveedor\n"
    t.string "contacto", limit: 150, comment: "Nombre de la persona de contacto del proveedor, puede ser el vendedor o el gerente, etc"
    t.string "celContacto", limit: 10, comment: "numero del celular del contacto"
    t.string "mail", limit: 20, comment: "Correo electronico del proveedor"
    t.integer "idDepartamento", comment: "Id de la departamento del proveedor"
    t.integer "idCiudad", comment: "Identificador de la ciudad donde se encuentra el proveedor"
    t.integer "idTiempoPago", comment: "Identificador del tiempo de pago que se establece con el proveedor, 30,45,60, etc en diias"
  end

  create_table "inv_proveedor_productos", primary_key: "id_proveedor_producto", id: :serial, force: :cascade do |t|
    t.integer "id_proveedor", null: false
    t.integer "id_producto", null: false
  end

  create_table "inv_proveedores", primary_key: "id_proveedor", id: :serial, force: :cascade do |t|
    t.string "codigo_proveedor", limit: 10, null: false
    t.integer "id_departamento", null: false
    t.integer "id_municipio", null: false
    t.integer "tipo_documento", null: false
    t.string "numero_documento", limit: 15, null: false
    t.string "razon_social", limit: 50, null: false
    t.string "contacto", limit: 30
    t.string "direccion", limit: 20, null: false
    t.string "telefono", limit: 10, null: false
    t.string "celular", limit: 10
    t.string "email", limit: 30
    t.string "web", limit: 40
    t.float "cupo_credito"
    t.integer "forma_pago"
    t.boolean "activo"
    t.index ["codigo_proveedor"], name: "uniq_codigo_proveedor", unique: true
  end

  create_table "inv_subGrupoArticulo", id: false, force: :cascade, comment: "Tabla donde se almacena los subgrupos de articulos. Todo subgrupo pertenece a un grupo." do |t|
    t.integer "idSubGrupoArticulo", comment: "Identificador de la tabla incremental"
    t.integer "idGrupo", comment: "Campo que se relaciona con la tabla de grupo de articulos"
    t.integer "codGrupo", comment: "Codigo del subgrupo definido por el usuario"
    t.string "nomSubGrupoArticulo", limit: 1, comment: "Nombre del subgrupo definido por el usuario"
  end

  create_table "odo_convenccion", primary_key: "id_convenccion", id: :integer, default: -> { "nextval('odo_convencion_id_convencion_seq'::regclass)" }, force: :cascade do |t|
    t.string "descripcion", limit: 100
    t.boolean "activo"
    t.boolean "todo"
    t.index ["descripcion"], name: "uniq_descripio_convenccion", unique: true
  end

  create_table "odo_diente", primary_key: "id_diente", id: :serial, force: :cascade do |t|
    t.integer "numeracion_fdi", comment: "nomenclatura dental"
    t.integer "tipo_diente_id"
    t.integer "orden"
    t.index ["numeracion_fdi"], name: "uniq_fdi", unique: true
  end

  create_table "odo_diente_odontograma", primary_key: "id_diente_odontograma", id: :serial, force: :cascade do |t|
    t.integer "odontograma_id"
    t.integer "diente_id"
    t.integer "odontograma_imagen_id"
    t.integer "convenccion_id"
    t.integer "superficie_dental_id"
    t.datetime "fecha_creacion"
  end

  create_table "odo_imagen_odontograma", primary_key: "id_imagen_odontograma", id: :serial, force: :cascade do |t|
    t.text "url_imagen"
    t.integer "id_convenccion"
    t.integer "id_superficie_dental"
    t.boolean "activo"
  end

  create_table "odo_odontograma", primary_key: "id_odontograma", id: :serial, force: :cascade do |t|
    t.integer "id_registro"
    t.text "observacion"
    t.datetime "fecha_creacion"
    t.datetime "fecha_actualizacion"
  end

  create_table "odo_registro", id: :integer, default: -> { "nextval('odo_registros_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "id_tipo_registro"
    t.integer "cfg_paciente_id"
    t.integer "id_medico"
    t.integer "id_cita"
    t.date "fecha_registro"
    t.datetime "fecha_sistema"
    t.integer "folio"
  end

  create_table "odo_registro_detalle", primary_key: "id_registro_detalle", id: :integer, default: -> { "nextval('odo_registro_detalle_id'::regclass)" }, force: :cascade do |t|
    t.integer "id_registro"
    t.integer "id_campo"
    t.text "valor"
  end

  create_table "odo_superficie_dental", primary_key: "id_superficie_dental", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 1, comment: "S:Superior, D: Derecho, I:Izquiero, B:Bajo,C:Centro,T:Todo el Diente\n"
    t.string "nombre", limit: 20
  end

  create_table "odo_tipo_diente", primary_key: "id_tipo_diente", id: :serial, force: :cascade do |t|
    t.string "nombre", limit: 20, comment: "Tipo de dientes que serán mostrados en el odontograma\n"
  end

  create_table "pyp_programa", primary_key: "id_programa", id: :serial, force: :cascade, comment: "Almacena los Programas de Promocion y Prevencion " do |t|
    t.string "cod_programa", limit: 20, null: false
    t.text "nom_programa", null: false
    t.boolean "estado", null: false, comment: "Activo o Inactivo"
  end

  create_table "pyp_programa_asoc", primary_key: "id_programa_asoc", id: :serial, force: :cascade do |t|
    t.integer "id_program", null: false
    t.integer "id_administradora", null: false
    t.integer "id_contrato", null: false
    t.integer "meta", limit: 2, null: false
  end

  create_table "pyp_programa_cita", primary_key: "id_pyp_programa_cita", id: :serial, force: :cascade do |t|
    t.integer "id_programa_items", null: false
    t.integer "id_cita", null: false
  end

  create_table "pyp_programa_item", primary_key: "id_programa_items", id: :serial, force: :cascade, comment: "Items de los programas creados" do |t|
    t.integer "id_programa"
    t.integer "id_servicio"
    t.integer "id_medicamento"
    t.integer "id_insumo"
    t.text "nom_actividad", null: false
    t.string "finalidad", limit: 50
    t.integer "genero", null: false, comment: "1-hombre\n2-mujer\n3-ambos"
    t.integer "edad_inicial", limit: 2, null: false
    t.integer "edad_inicial_list", null: false
    t.integer "edad_final", limit: 2, null: false
    t.integer "edad_final_list", null: false
    t.string "dosis", limit: 50, null: false
    t.string "frecuencia", limit: 50, null: false
    t.integer "meta", limit: 2, null: false
  end

  create_table "rips_ac", primary_key: ["id_rip_almacenado", "num_registro"], force: :cascade do |t|
    t.integer "id_rip_almacenado", null: false
    t.integer "num_registro", null: false
    t.string "num_fac", limit: 20, comment: "Número de la factura"
    t.string "cod_pre", limit: 12, comment: "Código del prestador de servicios de salud"
    t.string "tip_ide", limit: 2, comment: "Tipo de identificación del usuario\nCC\nCE\nPA\nRC\nTI\nAS\nMS\nNU"
    t.string "num_ide", limit: 20, comment: "Número de identificación del usuario en el sistema"
    t.string "fec_cons", limit: 10, comment: "Fecha del procedimiento dd/mm/aaaa"
    t.string "num_aut", limit: 15, comment: "Número de Autorización"
    t.string "cod_con", limit: 20, comment: "Código de consulta CuPS"
    t.string "fin_con", limit: 2, comment: "Finalidad de la consulta\n01 = Atención del parto (puerperio)\n02 = Atención del recién nacido\n03 = Atención en planificación familiar\n04 = Detección de alteraciones de crecimiento y desarrollo del menor de diez años\n05 = Detección de alteración del desarrollo joven\n06 = Detección de alteraciones del embarazo\n07 = Detección de alteraciones del adulto\n08 = Detección de alteraciones de agudeza visual\n09 = Detección de enfermedad profesional\n10 = No aplica"
    t.string "cau_ext", limit: 2, comment: "Causa externa\n01 = Accidente de trabajo\n02 = Accidente de tránsito\n03 = Accidente rábico\n04 = Accidente ofídico\n05 = Otro tipo de accidente\n06 = Evento catastrófico\n07 = Lesión por agresión\n08 = Lesión auto infligida\n09 = Sospecha de maltrato físico\n10 = Sospecha de abuso sexual\n11 = Sospecha de violencia sexual\n12 = Sospecha de maltrato emocional\n13 = Enfermedad general\n14 = Enfermedad profesional\n15 = Otra"
    t.string "dx_ppal", limit: 4, comment: "Diagnóstivo prinicipal"
    t.string "dx_rel1", limit: 4, comment: "Código del diagnóstico relacionado 1"
    t.string "dx_rel2", limit: 4, comment: "Código del diagnóstico relacionado 2"
    t.string "dx_rel3", limit: 4, comment: "Código del diagnóstico relacionado 3"
    t.string "dx_rel4", limit: 4, comment: "Código del diagnóstico relacionado 4"
    t.string "tipo_dx_ppal", limit: 2, comment: "tipo de diagnostico principal (para la entidad sera 3)\n1. impresion diagnóstica\n2. confirmado nuevo\n3. confirmado repetido\n"
    t.float "vlr_cons", comment: "valor de la consulta, separador PUNTO"
    t.float "vlr_cuo_mod", comment: "valor de la cuota moderadoraseparador PUNTO"
    t.float "vlr_neto", comment: "Valor Neto a pagar, separador decimal PUNTO"
  end

  create_table "rips_af", primary_key: ["id_rip_almacenado", "num_registro"], force: :cascade, comment: "Registros correspondientes a los rips de control, datos relacionados con la transaccion de los servicios facturados" do |t|
    t.integer "id_rip_almacenado", null: false, comment: "Llave foranea a la tabla rips_almacenados"
    t.integer "num_registro", null: false, comment: "Serial para identificar el numero de registro dentro del archivo"
    t.string "cod_pre", limit: 12, comment: "codigo del prestador, se solicita en la seccion configuracion>empresa>datosEmpresa>Código Ministerio"
    t.string "raz_soc", limit: 60, comment: "Razón Social o Apellidos y nombres del prestador"
    t.string "tip_ide", limit: 2, comment: "Tipo de Identificación\nNIT\nCC\nCE\nPA"
    t.string "num_ide", limit: 20, comment: "Número de Identificación"
    t.string "num_fac", limit: 20, comment: "Número de la factura"
    t.string "fec_exp", limit: 10, comment: "Fecha de expedición de la factura (cuando se creo la factura)\nformato dd/mm/aaaa"
    t.string "fec_inc", limit: 10, comment: "Fecha de Inicio (desde cuando se está facturando)\nformatodd/mm/aaaa"
    t.string "fec_fin", limit: 10, comment: "Fecha final (hasta cuando se está facturando)\nformato dd/mm/aaaa"
    t.string "cod_ent", limit: 20, comment: "Código entidad Administradora\ndato obtenido de fac_administradora.codigo_rip"
    t.string "nom_ent", limit: 30, comment: "Nombre entidad administradora (no se maneja queda vacio)"
    t.string "num_con", limit: 15, comment: "Número del Contrato"
    t.string "plan_ben", limit: 30, comment: "Plan de Beneficios (no se maneja queda vacio)"
    t.string "num_poli", limit: 10, comment: "Número de la póliza (sale de tabla fac_contrato)"
    t.float "val_copa", comment: "Valor total del pago compartido COPAGO, separador PUNTO, decimal de 15"
    t.float "val_com", comment: "Valor de la comisión, separador decimal PUNTO (no aplica para a entidad es cero)"
    t.float "val_desc", comment: "Valor total de Descuentos, separador decimal PUNTO"
    t.float "val_net", comment: "Valor Neto a Pagar por la entidad Contratante, separador decimal PUNTO"
  end

  create_table "rips_almacenados", primary_key: "id_rip_almacenado", id: :serial, force: :cascade, comment: "Conjuntos de RIPS que han sido creados en el sistema" do |t|
    t.string "nombre", limit: 300
    t.datetime "fecha", comment: "fecha de creacion"
    t.string "archivos", limit: 100, comment: "siglas de los tipos de archivos que contiene separados por coma"
    t.datetime "fecha_inicial"
    t.datetime "fecha_final"
    t.integer "id_administradora", comment: "identificador de la administradora, de aplicar a todas queda null"
    t.integer "id_contrato", comment: "identificador del contrato, de aplicar a todos los contratos queda NULL"
  end

  create_table "rips_ap", primary_key: ["id_rip_almacenado", "num_registro"], force: :cascade, comment: "registro de datos para el archivo de procedimientos(aquí van solo servicios y paquetes)" do |t|
    t.integer "id_rip_almacenado", null: false
    t.integer "num_registro", null: false
    t.string "num_fac", limit: 20, comment: "Número de la factura"
    t.string "cod_pre", limit: 12, comment: "Código del prestador de servicios de salud (Dato se obtiene de empresa.codigo_empresa)"
    t.string "tip_ide", limit: 2, comment: "Tipo de identificación del usuario\nCC\nCE\nPA\nRC\nTI\nAS\nMS\nNU"
    t.string "num_ide", limit: 20, comment: "Número de identificación del usuario en el sistema"
    t.string "fec_proc", limit: 10, comment: "Fecha del procedimiento dd/mm/aaaa"
    t.string "num_aut", limit: 15, comment: "Número de Autorización"
    t.string "cod_pro", limit: 20, comment: "Código del procedimiento, (Dato obtenido de fac_servicio.código_cup)"
    t.string "amb_pro", limit: 1, comment: "Ambito de realización del procedimiento (servicios.codigo_ambito) (Para la entidada por defecto va ser 1)\n1. Ambulatorio\n2. Hostipatalario\n3. Urgencias"
    t.string "fin_pro", limit: 1, comment: "Finalidad del procedimiento (servicios.finalidad) (por defecto 2)\n1 = Diagnóstico\n2 = Terapéutico\n3 = Protección específica\n4 = Detección temprana de enfermedad general\n5 = Detección temprana de enfermedad profesional"
    t.string "pers_ati", limit: 1, comment: "Personal que atiende (cfg_usuarios.tipo_prestador) (por defecto 1)\n1 =Médico (a) especialista\n2 =Médico (a) general\n3 =Enfermera (o)\n4 =Auxiliar de enfermería\n5 =Otro"
    t.string "dx_ppal", limit: 4, comment: "Diagnóstivo prinicipal"
    t.string "dx_rel", limit: 4, comment: "Código del diagnóstico relacionado"
    t.string "complicacion", limit: 4, comment: "Complicación: CIE 9 Ajustado y Complementado (No se usa para la entidad)"
    t.string "act_quirur", limit: 1, comment: "Forma de realización del acto quirúrgico (fac_servicio.acto_qx)(por defecto 1)\n1 = Unico o unilateral\n2 = Múltiple o bilateral, misma vía, diferente especialidad\n3 = Múltiple o bilateral, misma vía, igual especialidad\n4 = Múltiple o bilateral, diferente vía, diferente especialidad\n5 = Múltiple o bilateral, diferente vía, igual especialidad"
    t.float "valor", comment: "Valor del Procedimiento\nvalor del servicio – (copago+ cuota_mo) + (iva+cree)"
  end

  create_table "rips_at", primary_key: ["id_rip_almacenado", "num_registro"], force: :cascade do |t|
    t.integer "id_rip_almacenado", null: false
    t.integer "num_registro", null: false
    t.string "num_fac", limit: 20, comment: "Numero de la factura"
    t.string "cod_pre", limit: 10, comment: "codigo prestador de salud"
    t.string "tip_ide", limit: 2, comment: "tipo de identificacion del usuario"
    t.string "num_ide", limit: 20, comment: "numero de identificacion del usuario en el sistema"
    t.string "num_aut", limit: 15, comment: "numero de autorizacion"
    t.string "tip_ser", limit: 1, comment: "tipo de servicio"
    t.string "cod_ser", limit: 20, comment: "nombre del servicio"
    t.string "nom_ser", limit: 60, comment: "Nombre del servicio: suministro e insumo"
    t.string "cantidad", limit: 5
    t.string "vr_uni", limit: 15, comment: "valor unitario del material e insumo"
    t.string "vr_tot", limit: 15, comment: "valor total del material e insumo"
  end

  create_table "rips_ct", primary_key: ["id_rip_almacenado", "num_registro"], force: :cascade, comment: "Registros correspondientes a los rips de cntrol que se hayan generado\n" do |t|
    t.integer "id_rip_almacenado", null: false, comment: "Llave foranea a la tabla rips_almacenados"
    t.integer "num_registro", null: false, comment: "Serial para identificar el numero de registro dentro del archivo"
    t.string "cod_pres", limit: 12, comment: "codigo del prestador, se solicita en la seccion configuracion>empresa>datosEmpresa>Código Ministerio"
    t.datetime "fec_rem", comment: "fecha de remision: fecha en que se creó cada uno de los archivos de rips"
    t.string "cod_arc", limit: 20, comment: "codigo del archivo (queda pendiente como se determina el numero de remision)\n2 caracteres para siglas del archivo\n6 caracteres para numero de remision"
    t.integer "total_reg", comment: "total de registros "
  end

  create_table "rips_us", primary_key: ["id_rip_almacenado", "num_registro"], force: :cascade, comment: "registro de datos para el archivo de usuarios de los servicios de salud" do |t|
    t.integer "id_rip_almacenado", null: false
    t.integer "num_registro", null: false
    t.string "tip_ide", limit: 3, comment: "Tipo de Identificación del Usuario"
    t.string "num_ide", limit: 20, comment: "Número de Identifiación del Usuario en el Sistema"
    t.string "cod_ent_adm", limit: 6, comment: "Código Entidad Administradora (se obtiene de fac_administradora.código_rip )"
    t.string "tip_usu", limit: 3, comment: "Tipo de Usuario (cfg_pacientes.regimen)\n1. contributivo\n2. Subsidiado\n3. Vinculado\n4. Particular\n5. Otro"
    t.string "pri_ape", limit: 21, comment: "Primer apellido"
    t.string "seg_ape", limit: 21, comment: "Segundo apellido"
    t.string "pri_nom", limit: 21, comment: "primer nombre"
    t.string "seg_nom", limit: 21, comment: "segundo nombre"
    t.integer "edad", comment: "se redondea al año, si es menor al año se redondea a mes, si es menor de un mes en días"
    t.string "tipo_edad", limit: 3, comment: "Unidad de medida de la Edad\n1. años\n2. meses\n3. dias"
    t.string "sexo", limit: 3, comment: "Sexo del usuario\nM masculino\nF femenino"
    t.string "cod_dep_res", limit: 8, comment: "Código del departamento de residencia habitual"
    t.string "cod_mun_res", limit: 8, comment: "Código de municipios de residencia habitual"
    t.string "zona_res", limit: 8, comment: "Zona de residencia habitual (Si no tiene por defectu U)\nU. Urbana\nR. Rural"
  end

  create_table "sessiones", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessiones_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessiones_on_updated_at"
  end

  create_table "sin_nodos", primary_key: "id_nodo", id: :serial, force: :cascade, comment: "Nodos para posible sincronizacion" do |t|
    t.string "nombre", limit: 20
    t.text "descripcion"
    t.string "tipo", limit: 1, default: "1"
    t.string "usuario_db", limit: 20
    t.integer "cfg_sede_id"
    t.boolean "status"
  end

  create_table "sin_status", primary_key: ["id_tabla", "id_local", "id_nodo"], force: :cascade, comment: "Status de registros para sincronizacion" do |t|
    t.integer "id_tabla", null: false
    t.integer "id_local", null: false
    t.integer "id_remoto"
    t.integer "id_nodo", null: false
    t.boolean "status"
  end

  create_table "sin_tablas", primary_key: "id_tabla", id: :serial, force: :cascade, comment: "Tablas para sincronización" do |t|
    t.string "tabla", limit: 40
    t.integer "orden"
    t.string "tipo", limit: 1, default: "1"
    t.boolean "status"
  end

  create_table "tabla_procedimiento", id: false, force: :cascade do |t|
    t.decimal "codigo_interno"
    t.text "codigo_soat"
    t.text "codigo_cups"
    t.text "codigo_extra"
    t.text "nombre_soat"
    t.text "nombre_cups"
    t.text "tiporips_soat"
    t.text "tipo_rips_cups"
    t.integer "concepto"
    t.integer "finalidad"
    t.integer "centro_costo"
    t.text "nivel"
    t.integer "clase"
    t.text "sexo"
    t.integer "mayor"
    t.integer "menor"
    t.integer "pos"
    t.text "grupo_qui"
    t.float "puntos"
    t.integer "valor_costo"
    t.integer "frecuencia"
    t.float "cant_maxima"
    t.integer "tipo_frec"
    t.text "parametros"
    t.text "contable1"
    t.text "contable2"
    t.text "contable3"
    t.text "bodega"
    t.integer "activo"
    t.text "reg_ctrol"
  end

# Could not dump table "urg_admision" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora_admision'

# Could not dump table "urg_consulta_paciente" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora_iniconsulta'

# Could not dump table "urg_control_prescripcion_medicamentos" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora'

# Could not dump table "urg_detalle_consulta_paciente" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora_iniconsulta'

# Could not dump table "urg_notas_enfermerias" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora'

# Could not dump table "urg_notas_medicas" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora'

# Could not dump table "urg_orden_cobro" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora'

# Could not dump table "urg_plan_manejo_paciente" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora'

# Could not dump table "urg_prescripcion_medicamentos" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora'

# Could not dump table "urg_triage" because of following StandardError
#   Unknown type 'time with time zone' for column 'hora_triage'

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "username"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "users_username_key", unique: true
  end

  create_table "xlab_estudio", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 20
    t.string "nombre", limit: 50
    t.index ["codigo"], name: "uniq_codigo_estudio", unique: true
  end

  create_table "xlab_estudio_pruebas", primary_key: ["estudio_id", "prueba_id"], force: :cascade do |t|
    t.integer "estudio_id", null: false
    t.integer "prueba_id", null: false
  end

  create_table "xlab_orden", id: :serial, force: :cascade do |t|
    t.string "nro_orden", limit: 9
    t.datetime "fecha_orden"
    t.integer "cfg_paciente_id"
    t.integer "embarazo"
    t.integer "origen_id"
    t.string "autorizacion", limit: 20
    t.datetime "fecha_autorizacion"
    t.string "nro_exteno", limit: 10
    t.integer "medico_id"
    t.integer "servicio_id"
    t.integer "cama"
    t.string "diagnostico_id", limit: 10
    t.text "nota"
    t.string "estado", limit: 1
    t.datetime "fecha_estado"
    t.integer "usuario_crea"
    t.datetime "fecha_eliminacion"
    t.integer "usuario_elimina"
    t.datetime "fecha_creacion"
    t.datetime "fecha_actualizacion"
    t.bigint "cfg_empresa_id"
    t.index ["nro_orden"], name: "unique_nro_orden", unique: true
  end

  create_table "xlab_orden_estudio_prueba_resultados", id: :serial, force: :cascade do |t|
    t.integer "orden_estudios_pruebas_id"
    t.float "neutrofilos"
    t.float "linfocitos"
    t.float "cayados"
    t.float "eosinofilos"
    t.float "monocitos"
    t.float "basofilos"
    t.float "metamielocitos"
    t.float "normoblastos"
    t.text "observaciones"
  end

  create_table "xlab_orden_estudios", primary_key: ["orden_id", "estudio_id"], force: :cascade do |t|
    t.integer "orden_id", null: false
    t.integer "estudio_id", null: false
  end

  create_table "xlab_orden_estudios_pruebas", id: :serial, force: :cascade do |t|
    t.integer "estudio_id"
    t.integer "prueba_id"
    t.string "estado", limit: 1
    t.boolean "confirmado"
    t.float "ref_min"
    t.float "ref_max"
    t.text "nota"
    t.datetime "fecha_actualizacion"
    t.integer "usuario_actualiza"
    t.boolean "eliminado"
    t.datetime "fecha_eliminacion"
    t.integer "usuario_elimina"
    t.integer "orden_id"
    t.string "resultado", limit: 20
  end

  create_table "xlab_prueba", id: :serial, force: :cascade do |t|
    t.string "codigo", limit: 10
    t.string "nombre", limit: 200
    t.string "formato_resultado", limit: 1, comment: "N:Númerico\nT:Texto\nC:Compuesto\nF:Fórmulo\nG:Gráfico"
    t.integer "grupo_area"
    t.integer "unidad_prueba"
    t.integer "tipo_tecnica"
    t.integer "decimales_prueba"
    t.string "codigo_alterno", limit: 10
    t.string "formula", limit: 100
    t.text "comp_formato"
    t.text "rangos_externos"
    t.index ["codigo"], name: "uniq_codigo_xlab_pruebas", unique: true
  end

  create_table "xlab_prueba_referencia", id: :serial, force: :cascade do |t|
    t.integer "prueba_id"
    t.string "tipo", limit: 1, comment: "H:Hombres\nM:Mujeres\nN:Niños"
    t.float "minimo_referencia"
    t.float "maximo_referencia"
  end

  create_table "xlab_tipo_tecnica", id: :serial, force: :cascade do |t|
    t.string "nombre", limit: 100
    t.string "borrado", limit: 1
  end

  add_foreign_key "cfg_consultorios", "cfg_clasificaciones", column: "cod_especialidad", name: "cfg_consultorios_cod_especialidad_fkey"
  add_foreign_key "cfg_consultorios", "cfg_sedes", name: "cfg_consultorios_id_sede_fkey"
  add_foreign_key "cfg_empresas", "cfg_clasificaciones", column: "tipo_doc", name: "cfg_empresa_tipo_doc_fkey"
  add_foreign_key "cfg_empresas", "cfg_clasificaciones", column: "tipo_doc_rep_legal", name: "cfg_empresa_tipo_doc_rep_legal_fkey"
  add_foreign_key "cfg_empresas", "cfg_imagenes", name: "cfg_imagen_fk"
  add_foreign_key "cfg_empresas", "cfg_municipios", name: "cfg_empresas_cfg_municipio_id_fkey"
  add_foreign_key "cfg_insumo", "fac_administradoras", column: "id_administradora", name: "cfg_insumo_id_administradora_fkey"
  add_foreign_key "cfg_items_horario", "cfg_horario", column: "id_horario", primary_key: "id_horario", name: "cfg_items_horario_id_horario_fkey"
  add_foreign_key "cfg_medicamento", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "cfg_medicamento_id_producto_fkey"
  add_foreign_key "cfg_municipios", "cfg_departamentos", name: "cfg_municipios_cfg_departamento_id_fkey"
  add_foreign_key "cfg_opciones_menu", "cfg_opciones_menu", column: "id_opcion_padre", name: "opciones_id_opcion_padre_fkey"
  add_foreign_key "cfg_opciones_menu_perfil_usuarios", "cfg_opciones_menu", name: "cfg_opciones_menu_perfil_usuarios_cfg_opciones_menu_id_fkey"
  add_foreign_key "cfg_opciones_menu_perfil_usuarios", "cfg_perfiles_usuarios", name: "cfg_opciones_menu_perfil_usuario_id_perfil_usuario_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "categoria_paciente", name: "cfg_pacientes_categoria_paciente_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "escolaridad", name: "pacientes_escolaridad_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "estado_civil", name: "pacientes_estado_civil_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "estrato_id", name: "estrato_fk"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "etnia", name: "pacientes_etnia_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "genero", name: "pacientes_genero_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "grupo_sanguineo", name: "pacientes_grupo_sanguineo_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "nivel", name: "pacientes_nivel_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "ocupacion", name: "pacientes_ocupacion_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "parentesco", name: "cfg_pacientes_parentesco_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "regimen", name: "pacientes_regimen_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "tipo_afiliado", name: "pacientes_tipo_afiliado_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "tipo_identificacion", name: "pacientes_tipo_identificacion_fkey"
  add_foreign_key "cfg_pacientes", "cfg_clasificaciones", column: "zona", name: "pacientes_zona_fkey"
  add_foreign_key "cfg_pacientes", "cfg_departamentos", column: "dep_nacimiento", name: "cfg_pacientes_dep_nacimiento_fkey"
  add_foreign_key "cfg_pacientes", "cfg_departamentos", name: "pacientes_departamento_fkey"
  add_foreign_key "cfg_pacientes", "cfg_imagenes", column: "firma", name: "pacientes_firma_fkey"
  add_foreign_key "cfg_pacientes", "cfg_imagenes", column: "foto", name: "pacientes_foto_fkey"
  add_foreign_key "cfg_pacientes", "cfg_imagenes", name: "cfg_imagen_fk"
  add_foreign_key "cfg_pacientes", "cfg_municipios", column: "mun_nacimiento", name: "cfg_pacientes_mun_nacimiento_fkey"
  add_foreign_key "cfg_pacientes", "cfg_municipios", name: "cfg_pacientes_municipio_fkey"
  add_foreign_key "cfg_pacientes", "cfg_sedes", name: "cfg_sede_fk"
  add_foreign_key "cfg_pacientes", "fac_administradoras", name: "fac_administradora_fk"
  add_foreign_key "cfg_sedes", "cfg_empresas", name: "cfg_sede_cfg_empresa_fk"
  add_foreign_key "cfg_sedes", "cfg_municipios", name: "cfg_sede_cfg_municipio_fkey"
  add_foreign_key "cfg_txt_predefinidos", "cfg_maestros_txt_predefinidos", column: "id_maestro", primary_key: "id_maestro", name: "cfg_txt_predefinidos_id_maestro_fkey"
  add_foreign_key "cfg_usuarios", "cfg_clasificaciones", column: "especialidad", name: "cfg_usuarios_especialidad_fkey"
  add_foreign_key "cfg_usuarios", "cfg_clasificaciones", column: "genero", name: "cfg_usuarios_genero_fkey"
  add_foreign_key "cfg_usuarios", "cfg_clasificaciones", column: "personal_atiende", name: "cfg_usuarios_personal_atiende_fkey"
  add_foreign_key "cfg_usuarios", "cfg_clasificaciones", column: "tipo_identificacion", name: "usuarios_tipo_identificacion_fkey"
  add_foreign_key "cfg_usuarios", "cfg_clasificaciones", column: "tipo_usuario", name: "cfg_usuarios_tipo_usuario_fkey"
  add_foreign_key "cfg_usuarios", "cfg_imagenes", column: "firma_id", name: "usuarios_firma_fkey"
  add_foreign_key "cfg_usuarios", "cfg_imagenes", column: "foto", name: "usuarios_foto_fkey"
  add_foreign_key "cfg_usuarios", "cfg_imagenes", name: "cfg_usuarios_cfg_imagen_id_fkey"
  add_foreign_key "cfg_usuarios", "cfg_perfiles_usuarios", name: "usuarios_id_perfil_fkey"
  add_foreign_key "cfg_usuarios", "users", name: "cfg_usuarios_user_id_fkey"
  add_foreign_key "cit_autorizaciones", "cfg_pacientes", name: "autorizaciones_paciente_fkey"
  add_foreign_key "cit_autorizaciones", "cfg_usuarios", column: "id_usuario_creador", name: "cit_autorizaciones_usuario_creador_fkey"
  add_foreign_key "cit_autorizaciones", "fac_administradoras", column: "administradora", name: "cit_autorizaciones_administradora_fkey"
  add_foreign_key "cit_citas", "cfg_clasificaciones", column: "motivo_cancelacion", name: "cit_citas_motivo_cancelacion_fkey"
  add_foreign_key "cit_citas", "cfg_clasificaciones", column: "tipo_cita", name: "cit_citas_tipo_cita_fkey"
  add_foreign_key "cit_citas", "cfg_pacientes", name: "cit_citas_id_paciente_fkey"
  add_foreign_key "cit_citas", "cfg_usuarios", column: "id_prestador", name: "cit_citas_id_prestador_fkey"
  add_foreign_key "cit_citas", "cit_autorizaciones", column: "id_autorizacion", primary_key: "id_autorizacion", name: "cit_citas_id_autorizacion_fkey"
  add_foreign_key "cit_citas", "cit_paq_maestro", column: "id_paquete", primary_key: "id_paq_maestro", name: "cit_citas_id_paquete_fkey"
  add_foreign_key "cit_citas", "cit_turnos", column: "id_turno", primary_key: "id_turno", name: "cit_citas_id_turno_fkey"
  add_foreign_key "cit_citas", "fac_administradoras", column: "id_administradora", name: "cit_citas_id_administradora_fkey"
  add_foreign_key "cit_citas", "fac_servicio", column: "id_servicio", primary_key: "id_servicio", name: "cit_citas_id_servicio_fkey"
  add_foreign_key "cit_paq_detalle", "cfg_usuarios", column: "id_prestador", name: "cit_paq_detalle_id_prestador_fkey"
  add_foreign_key "cit_paq_detalle", "cit_paq_maestro", column: "id_paq_maestro", primary_key: "id_paq_maestro", name: "cit_paq_detalle_id_paq_maestro_fkey"
  add_foreign_key "cit_paq_detalle", "fac_servicio", column: "id_servicio", primary_key: "id_servicio", name: "cit_paq_detalle_id_servicio_fkey"
  add_foreign_key "cit_turnos", "cfg_horario", column: "id_horario", primary_key: "id_horario", name: "cit_turnos_id_horario_fkey"
  add_foreign_key "fac_administradoras", "cfg_clasificaciones", column: "codigo_departamento", name: "fac_administradora_codigo_departamento_fkey"
  add_foreign_key "fac_administradoras", "cfg_clasificaciones", column: "codigo_municipio", name: "fac_administradora_codigo_municipio_fkey"
  add_foreign_key "fac_administradoras", "cfg_clasificaciones", column: "tipo_administradora", name: "fac_administradora_tipo_administradora_fkey"
  add_foreign_key "fac_administradoras", "cfg_clasificaciones", column: "tipo_documento", name: "fac_administradora_tipo_documento_fkey"
  add_foreign_key "fac_administradoras", "cfg_clasificaciones", column: "tipo_documento_representante", name: "fac_administradora_tipo_documento_representante_fkey"
  add_foreign_key "fac_caja", "cfg_sedes", name: "fac_caja_id_sede_fkey"
  add_foreign_key "fac_consecutivo", "cfg_clasificaciones", column: "tipo_documento", name: "fac_consecutivo_tipo_documento_fkey"
  add_foreign_key "fac_consumo_insumo", "cfg_insumo", column: "id_insumo", primary_key: "id_insumo", name: "fac_consumo_insumo_id_insumo_fkey"
  add_foreign_key "fac_consumo_insumo", "cfg_pacientes", name: "fac_consumo_insumo_id_paciente_fkey"
  add_foreign_key "fac_consumo_insumo", "cfg_usuarios", column: "id_prestador", name: "fac_consumo_insumo_id_prestador_fkey"
  add_foreign_key "fac_consumo_medicamento", "cfg_medicamento", column: "id_medicamento", primary_key: "id_medicamento", name: "fac_consumo_medicamento_id_medicamento_fkey"
  add_foreign_key "fac_consumo_medicamento", "cfg_pacientes", name: "fac_consumo_medicamento_id_paciente_fkey"
  add_foreign_key "fac_consumo_medicamento", "cfg_usuarios", column: "id_prestador", name: "fac_consumo_medicamento_id_prestador_fkey"
  add_foreign_key "fac_consumo_paquete", "cfg_pacientes", name: "fac_consumo_paquete_id_paciente_fkey"
  add_foreign_key "fac_consumo_paquete", "cfg_usuarios", column: "id_prestador", name: "fac_consumo_paquete_id_prestador_fkey"
  add_foreign_key "fac_consumo_paquete", "fac_paquete", column: "id_paquete", primary_key: "id_paquete", name: "fac_consumo_paquete_id_paquete_fkey"
  add_foreign_key "fac_consumo_servicio", "cfg_pacientes", name: "fac_consumo_servicio_id_paciente_fkey"
  add_foreign_key "fac_consumo_servicio", "cfg_usuarios", column: "id_prestador", name: "fac_consumo_servicio_id_prestador_fkey"
  add_foreign_key "fac_consumo_servicio", "fac_servicio", column: "id_servicio", primary_key: "id_servicio", name: "fac_consumo_servicio_id_servicio_fkey"
  add_foreign_key "fac_contrato", "cfg_clasificaciones", column: "tipo_contrato", name: "fac_contrato_tipo_contrato_fkey"
  add_foreign_key "fac_contrato", "cfg_clasificaciones", column: "tipo_facturacion", name: "fac_contrato_tipo_facturacion_fkey"
  add_foreign_key "fac_contrato", "cfg_clasificaciones", column: "tipo_pago", name: "fac_contrato_tipo_pago_fkey"
  add_foreign_key "fac_contrato", "fac_administradoras", column: "id_administradora", name: "fac_contrato_id_administradora_fkey"
  add_foreign_key "fac_contrato", "fac_manual_tarifario", column: "id_manual_tarifario", primary_key: "id_manual_tarifario", name: "fac_contrato_id_manual_tarifario_fkey"
  add_foreign_key "fac_factura_admi", "cfg_clasificaciones", column: "tipo_documento", name: "fac_factura_admi_tipo_documento_fkey"
  add_foreign_key "fac_factura_admi", "fac_administradoras", column: "id_administradora", name: "fac_factura_admi_id_administradora_fkey"
  add_foreign_key "fac_factura_admi", "fac_contrato", column: "id_contrato", primary_key: "id_contrato", name: "fac_factura_admi_id_contrato_fkey"
  add_foreign_key "fac_factura_admi", "fac_periodo", column: "id_periodo", primary_key: "id_periodo", name: "fac_factura_admi_id_periodo_fkey"
  add_foreign_key "fac_factura_insumo", "cfg_consultorios", name: "fac_factura_insumo_id_consultorio_fkey"
  add_foreign_key "fac_factura_insumo", "cfg_insumo", column: "id_insumo", primary_key: "id_insumo", name: "fac_factura_insumo_id_insumo_fkey"
  add_foreign_key "fac_factura_insumo", "cfg_usuarios", column: "id_medico", name: "fac_factura_insumo_id_medico_fkey"
  add_foreign_key "fac_factura_insumo", "fac_factura_paciente", column: "id_factura", primary_key: "id_factura_paciente", name: "fac_factura_insumo_id_factura_fkey"
  add_foreign_key "fac_factura_medicamento", "cfg_consultorios", name: "fac_factura_medicamento_id_consultorio_fkey"
  add_foreign_key "fac_factura_medicamento", "cfg_medicamento", column: "id_medicamento", primary_key: "id_medicamento", name: "fac_factura_medicamento_id_medicamento_fkey"
  add_foreign_key "fac_factura_medicamento", "cfg_usuarios", column: "id_medico", name: "fac_factura_medicamento_id_medico_fkey"
  add_foreign_key "fac_factura_medicamento", "fac_factura_paciente", column: "id_factura", primary_key: "id_factura_paciente", name: "fac_factura_medicamento_id_factura_fkey"
  add_foreign_key "fac_factura_paciente", "cfg_clasificaciones", column: "tipo_documento", name: "fac_factura_tipo_documento_fkey"
  add_foreign_key "fac_factura_paciente", "cfg_clasificaciones", column: "tipo_ingreso", name: "fac_factura_tipo_ingreso_fkey"
  add_foreign_key "fac_factura_paciente", "cfg_pacientes", name: "fac_factura_id_paciente_fkey"
  add_foreign_key "fac_factura_paciente", "cit_citas", column: "id_cita", primary_key: "id_cita", name: "fac_factura_id_cita_fkey"
  add_foreign_key "fac_factura_paciente", "fac_administradoras", column: "id_administradora", name: "fac_factura_id_administradora_fkey"
  add_foreign_key "fac_factura_paciente", "fac_caja", column: "id_caja", primary_key: "id_caja", name: "fac_factura_id_caja_fkey"
  add_foreign_key "fac_factura_paciente", "fac_contrato", column: "id_contrato", primary_key: "id_contrato", name: "fac_factura_id_contrato_fkey"
  add_foreign_key "fac_factura_paciente", "fac_periodo", column: "id_periodo", primary_key: "id_periodo", name: "fac_factura_id_periodo_fkey"
  add_foreign_key "fac_factura_paquete", "cfg_consultorios", name: "fac_factura_paquete_id_consultorio_fkey"
  add_foreign_key "fac_factura_paquete", "cfg_usuarios", column: "id_medico", name: "fac_factura_paquete_id_medico_fkey"
  add_foreign_key "fac_factura_paquete", "fac_factura_paciente", column: "id_factura", primary_key: "id_factura_paciente", name: "fac_factura_paquete_id_factura_fkey"
  add_foreign_key "fac_factura_paquete", "fac_paquete", column: "id_paquete", primary_key: "id_paquete", name: "fac_factura_paquete_id_paquete_fkey"
  add_foreign_key "fac_factura_servicio", "cfg_consultorios", name: "fac_factura_servicio_id_consultorio_fkey"
  add_foreign_key "fac_factura_servicio", "cfg_usuarios", column: "id_medico", name: "fac_factura_servicio_id_medico_fkey"
  add_foreign_key "fac_factura_servicio", "fac_factura_paciente", column: "id_factura", primary_key: "id_factura_paciente", name: "fac_factura_servicio_id_factura_fkey"
  add_foreign_key "fac_factura_servicio", "fac_servicio", column: "id_servicio", primary_key: "id_servicio", name: "fac_factura_servicio_id_servicio_fkey"
  add_foreign_key "fac_manual_tarifario_insumo", "cfg_insumo", column: "id_insumo", primary_key: "id_insumo", name: "fac_manual_tarifario_insumo_id_insumo_fkey"
  add_foreign_key "fac_manual_tarifario_insumo", "fac_manual_tarifario", column: "id_manual_tarifario", primary_key: "id_manual_tarifario", name: "fac_manual_tarifario_insumo_id_manual_tarifario_fkey"
  add_foreign_key "fac_manual_tarifario_medicamento", "cfg_medicamento", column: "id_medicamento", primary_key: "id_medicamento", name: "fac_manual_tarifario_medicamento_id_medicamento_fkey"
  add_foreign_key "fac_manual_tarifario_medicamento", "fac_manual_tarifario", column: "id_manual_tarifario", primary_key: "id_manual_tarifario", name: "fac_manual_tarifario_medicamento_id_manual_tarifario_fkey"
  add_foreign_key "fac_manual_tarifario_paquete", "fac_manual_tarifario", column: "id_manual_tarifario", primary_key: "id_manual_tarifario", name: "fac_manual_tarifario_paquete_id_manual_tarifario_fkey"
  add_foreign_key "fac_manual_tarifario_servicio", "fac_manual_tarifario", column: "id_manual_tarifario", primary_key: "id_manual_tarifario", name: "fac_manual_tarifario_servicio_id_manual_tarifario_fkey"
  add_foreign_key "fac_manual_tarifario_servicio", "fac_unidad_valor", column: "anio_unidad_valor", primary_key: "anio", name: "fac_manual_tarifario_servicio_anio_unidad_valor_fkey"
  add_foreign_key "fac_movimiento_caja", "fac_caja", column: "id_caja", primary_key: "id_caja", name: "fac_movimiento_caja_id_caja_fkey"
  add_foreign_key "fac_paquete_insumo", "cfg_insumo", column: "id_insumo", primary_key: "id_insumo", name: "fac_paquete_insumo_id_insumo_fkey"
  add_foreign_key "fac_paquete_insumo", "fac_paquete", column: "id_paquete", primary_key: "id_paquete", name: "fac_paquete_insumo_id_paquete_fkey"
  add_foreign_key "fac_paquete_medicamento", "cfg_medicamento", column: "id_medicamento", primary_key: "id_medicamento", name: "fac_paquete_medicamento_id_medicamento_fkey"
  add_foreign_key "fac_paquete_medicamento", "fac_paquete", column: "id_paquete", primary_key: "id_paquete", name: "fac_paquete_medicamento_id_paquete_fkey"
  add_foreign_key "fac_programa", "cfg_clasificaciones", column: "causa_externa", name: "fac_programa_causa_externa_fkey"
  add_foreign_key "fac_programa", "cfg_clasificaciones", column: "finalidad_consulta", name: "fac_programa_finalidad_consulta_fkey"
  add_foreign_key "fac_programa", "cfg_clasificaciones", column: "finalidad_procedimineto", name: "fac_programa_finalidad_procedimineto_fkey"
  add_foreign_key "fac_programa", "cfg_diagnostico", column: "codigo_diagnostico", primary_key: "codigo_diagnostico", name: "fac_programa_codigo_diagnostico_fkey"
  add_foreign_key "fac_programa", "fac_contrato", column: "id_contrato", primary_key: "id_contrato", name: "fac_programa_id_contrato_fkey"
  add_foreign_key "fac_programa", "fac_manual_tarifario", column: "id_manual_tarifario", primary_key: "id_manual_tarifario", name: "fac_programa_id_manual_tarifario_fkey"
  add_foreign_key "fac_servicio", "cfg_centro_costo", column: "centro_costo", primary_key: "id_centro_costo", name: "fac_servicio_codigo_centro_costo_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "acto_quirurgico", name: "fac_servicio_acto_quirurgico_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "ambito", name: "fac_servicio_ambito_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "especialidad", name: "fac_servicio_codigo_especialidad_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "finalidad", name: "fac_servicio_finalidad_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "grupo_servicio", name: "fac_servicio_codigo_grupo_servicio_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "sexo", name: "fac_servicio_sexo_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "tipo_sevicio", name: "fac_servicio_codigo_tipo_sevicio_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "unidad_edad_final", name: "fac_servicio_unidad_medida_final_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "unidad_edad_inicial", name: "fac_servicio_unidad_edad_fkey"
  add_foreign_key "fac_servicio", "cfg_clasificaciones", column: "zona", name: "fac_servicio_zona_fkey"
  add_foreign_key "fac_servicio", "cfg_diagnostico", column: "codigo_diagnostico", primary_key: "codigo_diagnostico", name: "fac_servicio_codigo_diagnostico_fkey"
  add_foreign_key "hc_archivos", "cfg_pacientes", name: "hc_archivos_id_paciente_fkey"
  add_foreign_key "hc_campos_reg", "hc_tipo_reg", column: "id_tipo_reg", primary_key: "id_tipo_reg", name: "hc_campos_reg_id_tipo_reg_fkey"
  add_foreign_key "hc_detalle", "hc_registro", column: "id_registro", primary_key: "id_registro", name: "hc_detalle_id_registro_fkey"
  add_foreign_key "hc_estructura_familiar", "cfg_clasificaciones", column: "id_ocupacion", name: "hc_estructura_familiar_id_ocupacion_fkey"
  add_foreign_key "hc_estructura_familiar", "cfg_clasificaciones", column: "id_parentesco", name: "hc_estructura_familiar_id_parentesco_fkey"
  add_foreign_key "hc_estructura_familiar", "cfg_pacientes", name: "hc_estructura_familiar_id_paciente_fkey"
  add_foreign_key "hc_items", "hc_registro", column: "id_registro", primary_key: "id_registro", name: "hc_items_id_registro_fkey"
  add_foreign_key "hc_registro", "hc_tipo_reg", column: "id_tipo_reg", primary_key: "id_tipo_reg", name: "hc_registro_id_tipo_reg_fkey"
  add_foreign_key "hc_rep_examenes", "hc_registro", column: "id_registro", primary_key: "id_registro", name: "hc_rep_examenes_id_registro_fkey", on_update: :cascade, on_delete: :cascade
  add_foreign_key "inv_bodega_productos", "inv_bodegas", column: "id_bodega", primary_key: "id_bodega", name: "fk_inv_bodega_productos_id_bodega"
  add_foreign_key "inv_bodega_productos", "inv_lotes", column: "id_lote", primary_key: "id_lote", name: "fk_inbodega_lote"
  add_foreign_key "inv_bodega_productos", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "fk_inv_bodega_productos_id_producto"
  add_foreign_key "inv_bodegas", "cfg_empresas", name: "fk_inv_bodegas_id_empresa"
  add_foreign_key "inv_bodegas", "inv_bodegas", column: "id_bodega", primary_key: "id_bodega", name: "fk_inv_bodega_id_bodega_padre"
  add_foreign_key "inv_entrega_medicamentos", "cfg_empresas", name: "fk_inv_entrega_medicamentos_id_empresa"
  add_foreign_key "inv_entrega_medicamentos", "cfg_usuarios", column: "usuario_actualiza", name: "fk_inv_entrega_medicamentos_usuario_actualiza"
  add_foreign_key "inv_entrega_medicamentos", "cfg_usuarios", column: "usuario_elabora", name: "fk_inv_entrega_medicamentos_usuario_elabora"
  add_foreign_key "inv_entrega_medicamentos", "inv_bodegas", column: "id_bodega", primary_key: "id_bodega", name: "fk_inv_entrega_medicamentos_id_bodega"
  add_foreign_key "inv_entrega_medicamentos_detalle", "inv_entrega_medicamentos", column: "id_entrega", primary_key: "id_entrega", name: "fk_inv_entrega_medicamentos_detalle_id_entrega"
  add_foreign_key "inv_entrega_medicamentos_detalle", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "pk_inv_entrega_medicamentos_detalle_id_producto"
  add_foreign_key "inv_lote_productos", "inv_lotes", column: "id_lote", primary_key: "id_lote", name: "fk_inv_lote_productos_id_lote"
  add_foreign_key "inv_lote_productos", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "fk_inv_lote_productos_id_producto"
  add_foreign_key "inv_movimiento_productos", "inv_movimientos", column: "id_movimiento", primary_key: "id_movimiento", name: "fk_inv_movimiento_producto_id_movimiento"
  add_foreign_key "inv_movimiento_productos", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "fk_inv_movimiento_productos_id_producto"
  add_foreign_key "inv_movimientos", "cfg_empresas", name: "fk_inv_movimientos_id_empresa"
  add_foreign_key "inv_movimientos", "inv_bodegas", column: "id_bodega_destino", primary_key: "id_bodega", name: "fk_inv_movimientos_id_bodega_salida"
  add_foreign_key "inv_movimientos", "inv_bodegas", column: "id_bodega_origen", primary_key: "id_bodega", name: "fk_inv_movimientos_id_bodega_entrada"
  add_foreign_key "inv_movimientos", "inv_entrega_medicamentos", column: "id_entrega", primary_key: "id_entrega", name: "fk_inv_movimientos_id_entrega"
  add_foreign_key "inv_movimientos", "inv_orden_compra", column: "id_orden_compra", primary_key: "id_orden_compra", name: "pk_inv_movimientos_id_compra"
  add_foreign_key "inv_orden_compra", "cfg_empresas", name: "fk_inv_orden_compra_id_empresa"
  add_foreign_key "inv_orden_compra", "inv_lotes", column: "id_lote", primary_key: "id_lote", name: "fk_inv_orden_compra_lote"
  add_foreign_key "inv_orden_compra", "inv_proveedores", column: "id_proveedor", primary_key: "id_proveedor", name: "fk_inv_orden_compra_id_proveedor"
  add_foreign_key "inv_orden_compra_productos", "inv_orden_compra", column: "id_orden_compra", primary_key: "id_orden_compra", name: "fk_inv_orden_compra_producto_id_orden_compra"
  add_foreign_key "inv_orden_compra_productos", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "fk_inv_orden_compra_productos_id_producto"
  add_foreign_key "inv_productos", "cfg_clasificaciones", column: "id_via_administracion", name: "fk_inv_producto_id_via_administracion"
  add_foreign_key "inv_productos", "inv_categorias", column: "id_categoria", primary_key: "id_categoria", name: "fk_inv_productos_id_categoria"
  add_foreign_key "inv_proveedor_productos", "inv_productos", column: "id_producto", primary_key: "id_producto", name: "fk_inv_proveedor_productos_id_producto"
  add_foreign_key "inv_proveedor_productos", "inv_proveedores", column: "id_proveedor", primary_key: "id_proveedor", name: "fk_inv_proveedor_productos_id_proveedor"
  add_foreign_key "inv_proveedores", "cfg_clasificaciones", column: "id_departamento", name: "fk_inv_proveedores_id_departamento"
  add_foreign_key "inv_proveedores", "cfg_clasificaciones", column: "id_municipio", name: "fk_inv_proveedores_id_municipio"
  add_foreign_key "inv_proveedores", "cfg_clasificaciones", column: "tipo_documento", name: "fk_inv_proveedores_tipo_documento"
  add_foreign_key "odo_diente", "odo_tipo_diente", column: "tipo_diente_id", primary_key: "id_tipo_diente", name: "fk_id_tipo_diente_id"
  add_foreign_key "odo_diente_odontograma", "odo_convenccion", column: "convenccion_id", primary_key: "id_convenccion", name: "fk_conveccion_id_do"
  add_foreign_key "odo_diente_odontograma", "odo_diente", column: "diente_id", primary_key: "id_diente", name: "fk_diente_id_do"
  add_foreign_key "odo_diente_odontograma", "odo_imagen_odontograma", column: "odontograma_imagen_id", primary_key: "id_imagen_odontograma", name: "fk_odo_imagen_odontograma_id_do"
  add_foreign_key "odo_diente_odontograma", "odo_odontograma", column: "odontograma_id", primary_key: "id_odontograma", name: "fk_odo_odontograma_id_do"
  add_foreign_key "odo_diente_odontograma", "odo_superficie_dental", column: "superficie_dental_id", primary_key: "id_superficie_dental", name: "fk_superficie_dental_id_do"
  add_foreign_key "odo_imagen_odontograma", "odo_convenccion", column: "id_convenccion", primary_key: "id_convenccion", name: "fk_odo_conveccion_id_io"
  add_foreign_key "odo_imagen_odontograma", "odo_superficie_dental", column: "id_superficie_dental", primary_key: "id_superficie_dental", name: "fk_odo_superficie_dental_id_od"
  add_foreign_key "odo_odontograma", "odo_registro", column: "id_registro", name: "fk_odontograma_registro_id"
  add_foreign_key "odo_registro", "cfg_pacientes", name: "fk_odo_id_paciente"
  add_foreign_key "odo_registro", "cfg_usuarios", column: "id_medico", name: "fk_odo_medico"
  add_foreign_key "odo_registro", "cit_citas", column: "id_cita", primary_key: "id_cita", name: "fk_odo_id_cita"
  add_foreign_key "odo_registro", "hc_tipo_reg", column: "id_tipo_registro", primary_key: "id_tipo_reg", name: "fk_odo_id_tipo_registros"
  add_foreign_key "odo_registro_detalle", "hc_campos_reg", column: "id_campo", primary_key: "id_campo", name: "fk_id_campo_id_rd"
  add_foreign_key "odo_registro_detalle", "odo_registro", column: "id_registro", name: "fk_id_registro_id_rd"
  add_foreign_key "pyp_programa_asoc", "fac_administradoras", column: "id_administradora", name: "pyp_programa_asoc_id_administradora_fkey"
  add_foreign_key "pyp_programa_asoc", "fac_contrato", column: "id_contrato", primary_key: "id_contrato", name: "pyp_programa_asoc_id_contrato_fkey"
  add_foreign_key "pyp_programa_asoc", "pyp_programa", column: "id_program", primary_key: "id_programa", name: "pyp_programa_asoc_id_program_fkey"
  add_foreign_key "pyp_programa_cita", "pyp_programa_item", column: "id_programa_items", primary_key: "id_programa_items", name: "pyp_programa_cita_id_programa_items_fkey"
  add_foreign_key "pyp_programa_item", "cfg_insumo", column: "id_insumo", primary_key: "id_insumo", name: "pyp_programa_item_id_insumo_fkey"
  add_foreign_key "pyp_programa_item", "cfg_medicamento", column: "id_medicamento", primary_key: "id_medicamento", name: "pyp_programa_item_id_medicamento_fkey"
  add_foreign_key "pyp_programa_item", "fac_servicio", column: "id_servicio", primary_key: "id_servicio", name: "pyp_programa_item_id_servicio_fkey"
  add_foreign_key "pyp_programa_item", "pyp_programa", column: "id_programa", primary_key: "id_programa", name: "pyp_programa_item_id_programa_fkey"
  add_foreign_key "rips_ac", "rips_almacenados", column: "id_rip_almacenado", primary_key: "id_rip_almacenado", name: "rips_ac_id_rip_almacenado_fkey"
  add_foreign_key "rips_af", "rips_almacenados", column: "id_rip_almacenado", primary_key: "id_rip_almacenado", name: "rips_af_id_rip_almacenado_fkey"
  add_foreign_key "rips_almacenados", "fac_contrato", column: "id_contrato", primary_key: "id_contrato", name: "rips_almacenados_id_contrato_fkey"
  add_foreign_key "rips_ap", "rips_almacenados", column: "id_rip_almacenado", primary_key: "id_rip_almacenado", name: "rips_ap_id_rip_almacenado_fkey"
  add_foreign_key "rips_at", "rips_almacenados", column: "id_rip_almacenado", primary_key: "id_rip_almacenado", name: "rips_at_id_rip_almacenado_fkey"
  add_foreign_key "rips_ct", "rips_almacenados", column: "id_rip_almacenado", primary_key: "id_rip_almacenado", name: "rips_ct_id_rip_almacenado_fkey"
  add_foreign_key "rips_us", "rips_almacenados", column: "id_rip_almacenado", primary_key: "id_rip_almacenado", name: "rips_us_id_rip_almacenado_fkey"
  add_foreign_key "sin_nodos", "cfg_sedes", name: "fk_sin_nodos_sede"
  add_foreign_key "sin_status", "sin_nodos", column: "id_nodo", primary_key: "id_nodo", name: "fk_sin_status_nodos"
  add_foreign_key "sin_status", "sin_tablas", column: "id_tabla", primary_key: "id_tabla", name: "fk_sin_status_tablas"
  add_foreign_key "xlab_estudio_pruebas", "xlab_estudio", column: "estudio_id", name: "fk_estudio_prueba_id_estudio"
  add_foreign_key "xlab_estudio_pruebas", "xlab_prueba", column: "prueba_id", name: "fk_estudio_prueba_id_prueba"
  add_foreign_key "xlab_orden", "cfg_clasificaciones", column: "servicio_id", name: "fk_xlab_orden_servicio_id"
  add_foreign_key "xlab_orden", "cfg_diagnostico", column: "diagnostico_id", primary_key: "codigo_diagnostico", name: "fk_xlab_orden_diagnostico_id"
  add_foreign_key "xlab_orden", "cfg_empresas", name: "fk_xlab_orden_empresa_id"
  add_foreign_key "xlab_orden", "cfg_pacientes", name: "fk_xlab_orden_paciente_id"
  add_foreign_key "xlab_orden", "cfg_usuarios", column: "medico_id", name: "fk_xlab_orden_medico_id"
  add_foreign_key "xlab_orden", "cfg_usuarios", column: "usuario_crea", name: "fk_xlab_orden_usuario_crea"
  add_foreign_key "xlab_orden", "cfg_usuarios", column: "usuario_elimina", name: "fk_xlab_orden_usuario_id_elimina"
  add_foreign_key "xlab_orden", "fac_administradoras", column: "origen_id", name: "fx_xlab_orden_origen_id"
  add_foreign_key "xlab_orden_estudio_prueba_resultados", "xlab_orden_estudios_pruebas", column: "orden_estudios_pruebas_id", name: "fk_xlab_orden_estudio_prueba_resultados_estudio_prueba_id"
  add_foreign_key "xlab_orden_estudios", "xlab_estudio", column: "estudio_id", name: "fk_xlab_estudio_estudio_id"
  add_foreign_key "xlab_orden_estudios", "xlab_orden", column: "orden_id", name: "fk_xlab_estudio_orden_id"
  add_foreign_key "xlab_orden_estudios_pruebas", "cfg_usuarios", column: "usuario_actualiza", name: "fk_xlab_orden_estudio_pruebas_usuario_actualiza"
  add_foreign_key "xlab_orden_estudios_pruebas", "cfg_usuarios", column: "usuario_elimina", name: "fk_xlab_orden_estudio_prueba_usuario_elimina"
  add_foreign_key "xlab_orden_estudios_pruebas", "xlab_estudio", column: "estudio_id", name: "fk_xlab_orden_Estudios_pruebas_estudio_id"
  add_foreign_key "xlab_orden_estudios_pruebas", "xlab_orden", column: "orden_id", name: "fk_xlab_orden_Estudios_pruebas_orden_id"
  add_foreign_key "xlab_orden_estudios_pruebas", "xlab_prueba", column: "prueba_id", name: "fk_xlab_orden_Estudio_pruebas_prueba_id"
  add_foreign_key "xlab_prueba", "cfg_clasificaciones", column: "grupo_area", name: "fk_xlab_grupo_area_id"
  add_foreign_key "xlab_prueba", "cfg_unidad", column: "unidad_prueba", name: "fk_xlab_unidad_prueba"
  add_foreign_key "xlab_prueba", "xlab_tipo_tecnica", column: "tipo_tecnica", name: "fk_xlab_tipo_Tecnica_id"
  add_foreign_key "xlab_prueba_referencia", "xlab_prueba", column: "prueba_id", name: "fk_xlab_prueba_id"
end
