# Load the Rails application.
require_relative 'application'
# Para incluir las constantes de la aplicacion
require 'yaml'
CONFIG = YAML::load(File.open("#{Rails.root}/config/constantes.yml"))
#
# Initialize the Rails application.
Rails.application.initialize!
