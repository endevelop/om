Rails.application.routes.draw do

  resources :cfg_opciones_menus do
    collection do
      get 'buscar'
    end
    resources :cfg_operaciones_opciones_menu
  end
  resources :cfg_perfiles_usuarios do
    collection do
      get 'buscar'
      post 'actualizar_operaciones'
      post 'guardar_operacion_opcion'
      post 'guardar_todas_operacion_opcion'
    end
    member do
      get 'asociar_opciones_menu'
      post 'guardar_asociar_opciones_menu'
    end
  end
  resources :cfg_usuarios do
    collection do
      get 'buscar'
      post 'actualizar_sedes_por_empresa'
    end
    member do
      get 'reiniciar_clave'
      post 'guardar_reinicio_clave'
    end 
  end
  
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'my_devise/registrations'  }
  
  resources :cfg_pacientes
  resources :cfg_items_horarios
  resources :cfg_horarios do
    collection do
      post 'listar_horarios'  
      post 'editar_horarios'    
    end
  end
  resources :cfg_empresas do
    resources :cfg_sedes do
      resources :cfg_consultorios
    end    
  end

  resources :cit_turnos do
    collection do
      post 'buscar_usuario'
      post 'buscar_horarios'
      post 'buscar_agenda'
      post 'buscar_evento'      
      get 'search'
      post 'buscar'
      post 'buscar_prestador'
    end
    resources :cit_citas
  end

  resources :cfg_dias_no_laborales
  resources :citas do
    collection do
      post 'buscar_usuario'
      post 'buscar_agenda'
      post 'buscar_paciente'
      post 'cancelar_cita'
      get 'usuarios'
    end
  end
  root 'bienvenida#index'
  get 'bienvenida/index'
  resources :especialidades
  post '/cfg_empresas/:id/:activo', to: 'cfg_empresas#estatus_empresa', as: 'estatus_empresa'
  post '/cfg_empresas/:cfg_empresa_id/cfg_sedes/:id/:activo', to: 'cfg_sedes#estatus_sede', as: 'estatus_sede'
  post '/cfg_empresas/:cfg_empresa_id/cfg_sedes/:cfg_sede_id/cfg_consultorios/:id/:activo', to: 'cfg_consultorios#estatus_consultorio', as: 'estatus_consultorio'
  post '/cfg_pacientes/buscar_pacientes', to: 'cfg_pacientes#buscar_pacientes', as: 'buscar_pacientes'
  post '/cfg_pacientes/buscar_paciente', to: 'cfg_pacientes#buscar_paciente', as: 'buscar_paciente'
  # usuarios
  post '/cfg_usuarios/:id/:activo', to: 'cfg_usuarios#estatus_usuario', as: 'estatus_usuario'
  post '/cfg_perfiles_usuarios/:id/:activo', to: 'cfg_perfiles_usuarios#estatus_perfil', as: 'estatus_perfil'
  post '/cfg_opciones_menus/:id/:activo', to: 'cfg_opciones_menus#estatus_opcion', as: 'estatus_opcion'
  post '/cfg_opciones_menu/:cfg_opciones_menu_id/cfg_operaciones_opciones_menu/:id/:activo', to: 'cfg_operaciones_opciones_menu#estatus_operacion', as: 'estatus_operacion' 
  post '/cit_citas/buscar_paciente_cita', to: 'cit_citas#buscar_paciente_cita', as: 'buscar_paciente_cita'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Errores generados por id's o recursos no existentes, entre otros
  get 'errors/not_found'
  get 'errors/internal_server_error'  
  get '*path', to: 'application#routing_error'
  # Obligatoriamente va en el último lugar. Caso contrario, devuelve cualquier request, así no tenga error
  get '*unmatched_route', to: 'errors#not_found'
end
