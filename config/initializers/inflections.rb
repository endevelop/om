# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
 ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
   inflect.plural /^(.*)([lrndy])$/i,'\1\2es'
   inflect.singular /^(.*)([lrndy])es$/i, '\1\2'
   inflect.irregular 'sede', 'sedes'
   inflect.irregular 'user', 'users'
   inflect.irregular 'cfg_opciones_menu', 'cfg_opciones_menu'
   inflect.irregular 'cfg_operaciones_opciones_menu', 'cfg_operaciones_opciones_menu'
   inflect.irregular 'cfg_operaciones_detalle', 'cfg_operaciones_detalle'   
   inflect.irregular 'cfg_imagen', 'cfg_imagenes'
   inflect.irregular 'cfg_especialidad', 'cfg_especialidades'
   inflect.irregular 'cit_cita', 'cit_citas'
   inflect.irregular 'fac_servicio', 'fac_servicio'
   inflect.irregular 'cfg_dias_no_laborales', 'cfg_dias_no_laborales'
 end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
