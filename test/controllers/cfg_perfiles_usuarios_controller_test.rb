require 'test_helper'

class CfgPerfilesUsuariosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cfg_perfiles_usuario = cfg_perfiles_usuarios(:one)
  end

  test "should get index" do
    get cfg_perfiles_usuarios_url
    assert_response :success
  end

  test "should get new" do
    get new_cfg_perfiles_usuario_url
    assert_response :success
  end

  test "should create cfg_perfiles_usuario" do
    assert_difference('CfgPerfilesUsuario.count') do
      post cfg_perfiles_usuarios_url, params: { cfg_perfiles_usuario: { activo: @cfg_perfiles_usuario.activo, asociado_empresa: @cfg_perfiles_usuario.asociado_empresa, nombre_perfil: @cfg_perfiles_usuario.nombre_perfil, observaciones: @cfg_perfiles_usuario.observaciones } }
    end

    assert_redirected_to cfg_perfiles_usuario_url(CfgPerfilesUsuario.last)
  end

  test "should show cfg_perfiles_usuario" do
    get cfg_perfiles_usuario_url(@cfg_perfiles_usuario)
    assert_response :success
  end

  test "should get edit" do
    get edit_cfg_perfiles_usuario_url(@cfg_perfiles_usuario)
    assert_response :success
  end

  test "should update cfg_perfiles_usuario" do
    patch cfg_perfiles_usuario_url(@cfg_perfiles_usuario), params: { cfg_perfiles_usuario: { activo: @cfg_perfiles_usuario.activo, asociado_empresa: @cfg_perfiles_usuario.asociado_empresa, nombre_perfil: @cfg_perfiles_usuario.nombre_perfil, observaciones: @cfg_perfiles_usuario.observaciones } }
    assert_redirected_to cfg_perfiles_usuario_url(@cfg_perfiles_usuario)
  end

  test "should destroy cfg_perfiles_usuario" do
    assert_difference('CfgPerfilesUsuario.count', -1) do
      delete cfg_perfiles_usuario_url(@cfg_perfiles_usuario)
    end

    assert_redirected_to cfg_perfiles_usuarios_url
  end
end
