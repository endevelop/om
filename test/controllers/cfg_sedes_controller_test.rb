require 'test_helper'

class CfgSedesControllerTest < ActionController::TestCase
  setup do
    @cfg_empresa = cfg_empresas(:one)
    @cfg_sede = cfg_sedes(:one)
  end

  test "should get index" do
    get :index, params: { cfg_empresa_id: @cfg_empresa }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { cfg_empresa_id: @cfg_empresa }
    assert_response :success
  end

  test "should create cfg_sede" do
    assert_difference('CfgSede.count') do
      post :create, params: { cfg_empresa_id: @cfg_empresa, cfg_sede: @cfg_sede.attributes }
    end

    assert_redirected_to cfg_empresa_cfg_sede_path(@cfg_empresa, CfgSede.last)
  end

  test "should show cfg_sede" do
    get :show, params: { cfg_empresa_id: @cfg_empresa, id: @cfg_sede }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { cfg_empresa_id: @cfg_empresa, id: @cfg_sede }
    assert_response :success
  end

  test "should update cfg_sede" do
    put :update, params: { cfg_empresa_id: @cfg_empresa, id: @cfg_sede, cfg_sede: @cfg_sede.attributes }
    assert_redirected_to cfg_empresa_cfg_sede_path(@cfg_empresa, CfgSede.last)
  end

  test "should destroy cfg_sede" do
    assert_difference('CfgSede.count', -1) do
      delete :destroy, params: { cfg_empresa_id: @cfg_empresa, id: @cfg_sede }
    end

    assert_redirected_to cfg_empresa_cfg_sedes_path(@cfg_empresa)
  end
end
