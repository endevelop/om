require 'test_helper'

class CfgUsuariosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cfg_usuario = cfg_usuarios(:one)
  end

  test "should get index" do
    get cfg_usuarios_url
    assert_response :success
  end

  test "should get new" do
    get new_cfg_usuario_url
    assert_response :success
  end

  test "should create cfg_usuario" do
    assert_difference('CfgUsuario.count') do
      post cfg_usuarios_url, params: { cfg_usuario: { estado_cuenta: @cfg_usuario.estado_cuenta, identificacion: @cfg_usuario.identificacion, integer: @cfg_usuario.integer, login_usuario: @cfg_usuario.login_usuario, primer_apellido: @cfg_usuario.primer_apellido, primer_nombre: @cfg_usuario.primer_nombre, segundo_apellido: @cfg_usuario.segundo_apellido, segundo_nombre: @cfg_usuario.segundo_nombre, tipo_identificacion: @cfg_usuario.tipo_identificacion, tipo_usuario: @cfg_usuario.tipo_usuario } }
    end

    assert_redirected_to cfg_usuario_url(CfgUsuario.last)
  end

  test "should show cfg_usuario" do
    get cfg_usuario_url(@cfg_usuario)
    assert_response :success
  end

  test "should get edit" do
    get edit_cfg_usuario_url(@cfg_usuario)
    assert_response :success
  end

  test "should update cfg_usuario" do
    patch cfg_usuario_url(@cfg_usuario), params: { cfg_usuario: { estado_cuenta: @cfg_usuario.estado_cuenta, identificacion: @cfg_usuario.identificacion, integer: @cfg_usuario.integer, login_usuario: @cfg_usuario.login_usuario, primer_apellido: @cfg_usuario.primer_apellido, primer_nombre: @cfg_usuario.primer_nombre, segundo_apellido: @cfg_usuario.segundo_apellido, segundo_nombre: @cfg_usuario.segundo_nombre, tipo_identificacion: @cfg_usuario.tipo_identificacion, tipo_usuario: @cfg_usuario.tipo_usuario } }
    assert_redirected_to cfg_usuario_url(@cfg_usuario)
  end

  test "should destroy cfg_usuario" do
    assert_difference('CfgUsuario.count', -1) do
      delete cfg_usuario_url(@cfg_usuario)
    end

    assert_redirected_to cfg_usuarios_url
  end
end
