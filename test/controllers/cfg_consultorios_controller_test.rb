require 'test_helper'

class CfgConsultoriosControllerTest < ActionController::TestCase
  setup do
    @cfg_sede = cfg_sedes(:one)
    @cfg_consultorio = cfg_consultorios(:one)
  end

  test "should get index" do
    get :index, params: { cfg_sede_id: @cfg_sede }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { cfg_sede_id: @cfg_sede }
    assert_response :success
  end

  test "should create cfg_consultorio" do
    assert_difference('CfgConsultorio.count') do
      post :create, params: { cfg_sede_id: @cfg_sede, cfg_consultorio: @cfg_consultorio.attributes }
    end

    assert_redirected_to cfg_sede_cfg_consultorio_path(@cfg_sede, CfgConsultorio.last)
  end

  test "should show cfg_consultorio" do
    get :show, params: { cfg_sede_id: @cfg_sede, id: @cfg_consultorio }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { cfg_sede_id: @cfg_sede, id: @cfg_consultorio }
    assert_response :success
  end

  test "should update cfg_consultorio" do
    put :update, params: { cfg_sede_id: @cfg_sede, id: @cfg_consultorio, cfg_consultorio: @cfg_consultorio.attributes }
    assert_redirected_to cfg_sede_cfg_consultorio_path(@cfg_sede, CfgConsultorio.last)
  end

  test "should destroy cfg_consultorio" do
    assert_difference('CfgConsultorio.count', -1) do
      delete :destroy, params: { cfg_sede_id: @cfg_sede, id: @cfg_consultorio }
    end

    assert_redirected_to cfg_sede_cfg_consultorios_path(@cfg_sede)
  end
end
