require 'test_helper'

class CfgOpcionesMenusControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cfg_opciones_menu = cfg_opciones_menu(:one)
  end

  test "should get index" do
    get cfg_opciones_menu_index_url
    assert_response :success
  end

  test "should get new" do
    get new_cfg_opciones_menu_url
    assert_response :success
  end

  test "should create cfg_opciones_menu" do
    assert_difference('CfgOpcionesMenu.count') do
      post cfg_opciones_menu_index_url, params: { cfg_opciones_menu: { id_opcion_padre: @cfg_opciones_menu.id_opcion_padre, nombre_opcion: @cfg_opciones_menu.nombre_opcion, orden: @cfg_opciones_menu.orden, url_opcion: @cfg_opciones_menu.url_opcion } }
    end

    assert_redirected_to cfg_opciones_menu_url(CfgOpcionesMenu.last)
  end

  test "should show cfg_opciones_menu" do
    get cfg_opciones_menu_url(@cfg_opciones_menu)
    assert_response :success
  end

  test "should get edit" do
    get edit_cfg_opciones_menu_url(@cfg_opciones_menu)
    assert_response :success
  end

  test "should update cfg_opciones_menu" do
    patch cfg_opciones_menu_url(@cfg_opciones_menu), params: { cfg_opciones_menu: { id_opcion_padre: @cfg_opciones_menu.id_opcion_padre, nombre_opcion: @cfg_opciones_menu.nombre_opcion, orden: @cfg_opciones_menu.orden, url_opcion: @cfg_opciones_menu.url_opcion } }
    assert_redirected_to cfg_opciones_menu_url(@cfg_opciones_menu)
  end

  test "should destroy cfg_opciones_menu" do
    assert_difference('CfgOpcionesMenu.count', -1) do
      delete cfg_opciones_menu_url(@cfg_opciones_menu)
    end

    assert_redirected_to cfg_opciones_menu_index_url
  end
end
