require 'test_helper'

class CfgPacientesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cfg_paciente = cfg_pacientes(:one)
  end

  test "should get index" do
    get cfg_pacientes_url
    assert_response :success
  end

  test "should get new" do
    get new_cfg_paciente_url
    assert_response :success
  end

  test "should create cfg_paciente" do
    assert_difference('CfgPaciente.count') do
      post cfg_pacientes_url, params: { cfg_paciente: { acompanante: @cfg_paciente.acompanante, activo: @cfg_paciente.activo, barrio: @cfg_paciente.barrio, carnet: @cfg_paciente.carnet, categoria_paciente: @cfg_paciente.categoria_paciente, celular: @cfg_paciente.celular, cfg_departamento_id: @cfg_paciente.cfg_departamento_id, cfg_municipio_id: @cfg_paciente.cfg_municipio_id, dep_nacimiento: @cfg_paciente.dep_nacimiento, departamentocatastrofe: @cfg_paciente.departamentocatastrofe, desplazado: @cfg_paciente.desplazado, dircatastrofe: @cfg_paciente.dircatastrofe, direccion: @cfg_paciente.direccion, email: @cfg_paciente.email, escolaridad: @cfg_paciente.escolaridad, estado_civil: @cfg_paciente.estado_civil, etnia: @cfg_paciente.etnia, facturarlo: @cfg_paciente.facturarlo, fecha_afiliacion: @cfg_paciente.fecha_afiliacion, fecha_nacimiento: @cfg_paciente.fecha_nacimiento, fecha_sisben: @cfg_paciente.fecha_sisben, fecha_vence_carnet: @cfg_paciente.fecha_vence_carnet, fechacatastrofe: @cfg_paciente.fechacatastrofe, fechainsc: @cfg_paciente.fechainsc, fechaop: @cfg_paciente.fechaop, firma: @cfg_paciente.firma, foto: @cfg_paciente.foto, genero: @cfg_paciente.genero, grado: @cfg_paciente.grado, grupo_sanguineo: @cfg_paciente.grupo_sanguineo, historia: @cfg_paciente.historia, id_administradora: @cfg_paciente.id_administradora, id_contrato: @cfg_paciente.id_contrato, id_discapacidad: @cfg_paciente.id_discapacidad, id_gestacion: @cfg_paciente.id_gestacion, id_religion: @cfg_paciente.id_religion, identificacion: @cfg_paciente.identificacion, lugar_expedicion: @cfg_paciente.lugar_expedicion, mun_nacimiento: @cfg_paciente.mun_nacimiento, municipiocatastrofe: @cfg_paciente.municipiocatastrofe, nivel: @cfg_paciente.nivel, numero_autorizacion: @cfg_paciente.numero_autorizacion, observaciones: @cfg_paciente.observaciones, ocupacion: @cfg_paciente.ocupacion, parentesco: @cfg_paciente.parentesco, parentesco_a: @cfg_paciente.parentesco_a, pensionado: @cfg_paciente.pensionado, poblacion_lbgt: @cfg_paciente.poblacion_lbgt, primer_apellido: @cfg_paciente.primer_apellido, primer_nombre: @cfg_paciente.primer_nombre, regimen: @cfg_paciente.regimen, responsable: @cfg_paciente.responsable, segundo_apellido: @cfg_paciente.segundo_apellido, segundo_nombre: @cfg_paciente.segundo_nombre, telefono_acompanante: @cfg_paciente.telefono_acompanante, telefono_oficina: @cfg_paciente.telefono_oficina, telefono_residencia: @cfg_paciente.telefono_residencia, telefono_responsable: @cfg_paciente.telefono_responsable, tipo_afiliado: @cfg_paciente.tipo_afiliado, tipo_identificacion: @cfg_paciente.tipo_identificacion, victima_conflicto_armado: @cfg_paciente.victima_conflicto_armado, victima_maltrato: @cfg_paciente.victima_maltrato, zona: @cfg_paciente.zona, zonacatastrofe: @cfg_paciente.zonacatastrofe } }
    end

    assert_redirected_to cfg_paciente_url(CfgPaciente.last)
  end

  test "should show cfg_paciente" do
    get cfg_paciente_url(@cfg_paciente)
    assert_response :success
  end

  test "should get edit" do
    get edit_cfg_paciente_url(@cfg_paciente)
    assert_response :success
  end

  test "should update cfg_paciente" do
    patch cfg_paciente_url(@cfg_paciente), params: { cfg_paciente: { acompanante: @cfg_paciente.acompanante, activo: @cfg_paciente.activo, barrio: @cfg_paciente.barrio, carnet: @cfg_paciente.carnet, categoria_paciente: @cfg_paciente.categoria_paciente, celular: @cfg_paciente.celular, cfg_departamento_id: @cfg_paciente.cfg_departamento_id, cfg_municipio_id: @cfg_paciente.cfg_municipio_id, dep_nacimiento: @cfg_paciente.dep_nacimiento, departamentocatastrofe: @cfg_paciente.departamentocatastrofe, desplazado: @cfg_paciente.desplazado, dircatastrofe: @cfg_paciente.dircatastrofe, direccion: @cfg_paciente.direccion, email: @cfg_paciente.email, escolaridad: @cfg_paciente.escolaridad, estado_civil: @cfg_paciente.estado_civil, etnia: @cfg_paciente.etnia, facturarlo: @cfg_paciente.facturarlo, fecha_afiliacion: @cfg_paciente.fecha_afiliacion, fecha_nacimiento: @cfg_paciente.fecha_nacimiento, fecha_sisben: @cfg_paciente.fecha_sisben, fecha_vence_carnet: @cfg_paciente.fecha_vence_carnet, fechacatastrofe: @cfg_paciente.fechacatastrofe, fechainsc: @cfg_paciente.fechainsc, fechaop: @cfg_paciente.fechaop, firma: @cfg_paciente.firma, foto: @cfg_paciente.foto, genero: @cfg_paciente.genero, grado: @cfg_paciente.grado, grupo_sanguineo: @cfg_paciente.grupo_sanguineo, historia: @cfg_paciente.historia, id_administradora: @cfg_paciente.id_administradora, id_contrato: @cfg_paciente.id_contrato, id_discapacidad: @cfg_paciente.id_discapacidad, id_gestacion: @cfg_paciente.id_gestacion, id_religion: @cfg_paciente.id_religion, identificacion: @cfg_paciente.identificacion, lugar_expedicion: @cfg_paciente.lugar_expedicion, mun_nacimiento: @cfg_paciente.mun_nacimiento, municipiocatastrofe: @cfg_paciente.municipiocatastrofe, nivel: @cfg_paciente.nivel, numero_autorizacion: @cfg_paciente.numero_autorizacion, observaciones: @cfg_paciente.observaciones, ocupacion: @cfg_paciente.ocupacion, parentesco: @cfg_paciente.parentesco, parentesco_a: @cfg_paciente.parentesco_a, pensionado: @cfg_paciente.pensionado, poblacion_lbgt: @cfg_paciente.poblacion_lbgt, primer_apellido: @cfg_paciente.primer_apellido, primer_nombre: @cfg_paciente.primer_nombre, regimen: @cfg_paciente.regimen, responsable: @cfg_paciente.responsable, segundo_apellido: @cfg_paciente.segundo_apellido, segundo_nombre: @cfg_paciente.segundo_nombre, telefono_acompanante: @cfg_paciente.telefono_acompanante, telefono_oficina: @cfg_paciente.telefono_oficina, telefono_residencia: @cfg_paciente.telefono_residencia, telefono_responsable: @cfg_paciente.telefono_responsable, tipo_afiliado: @cfg_paciente.tipo_afiliado, tipo_identificacion: @cfg_paciente.tipo_identificacion, victima_conflicto_armado: @cfg_paciente.victima_conflicto_armado, victima_maltrato: @cfg_paciente.victima_maltrato, zona: @cfg_paciente.zona, zonacatastrofe: @cfg_paciente.zonacatastrofe } }
    assert_redirected_to cfg_paciente_url(@cfg_paciente)
  end

  test "should destroy cfg_paciente" do
    assert_difference('CfgPaciente.count', -1) do
      delete cfg_paciente_url(@cfg_paciente)
    end

    assert_redirected_to cfg_pacientes_url
  end
end
