require 'test_helper'

class CfgEmpresasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cfg_empresa = cfg_empresas(:one)
  end

  test "should get index" do
    get cfg_empresas_url
    assert_response :success
  end

  test "should get new" do
    get new_cfg_empresa_url
    assert_response :success
  end

  test "should create cfg_empresa" do
    assert_difference('CfgEmpresa.count') do
      post cfg_empresas_url, params: { cfg_empresa: { cfg_municipio_id: @cfg_empresa.cfg_municipio_id, cod_departamento: @cfg_empresa.cod_departamento, cod_municipio: @cfg_empresa.cod_municipio, codigo_empresa: @cfg_empresa.codigo_empresa, direccion: @cfg_empresa.direccion, dv: @cfg_empresa.dv, logo: @cfg_empresa.logo, nivel: @cfg_empresa.nivel, nom_rep_legal: @cfg_empresa.nom_rep_legal, num_doc_rep_legal: @cfg_empresa.num_doc_rep_legal, num_identificacion: @cfg_empresa.num_identificacion, observaciones: @cfg_empresa.observaciones, razon_rip: @cfg_empresa.razon_rip, razon_social: @cfg_empresa.razon_social, regimen: @cfg_empresa.regimen, telefono_1: @cfg_empresa.telefono_1, telefono_2: @cfg_empresa.telefono_2, tipo_doc: @cfg_empresa.tipo_doc, tipo_doc_rep_legal: @cfg_empresa.tipo_doc_rep_legal, website: @cfg_empresa.website } }
    end

    assert_redirected_to cfg_empresa_url(CfgEmpresa.last)
  end

  test "should show cfg_empresa" do
    get cfg_empresa_url(@cfg_empresa)
    assert_response :success
  end

  test "should get edit" do
    get edit_cfg_empresa_url(@cfg_empresa)
    assert_response :success
  end

  test "should update cfg_empresa" do
    patch cfg_empresa_url(@cfg_empresa), params: { cfg_empresa: { cfg_municipio_id: @cfg_empresa.cfg_municipio_id, cod_departamento: @cfg_empresa.cod_departamento, cod_municipio: @cfg_empresa.cod_municipio, codigo_empresa: @cfg_empresa.codigo_empresa, direccion: @cfg_empresa.direccion, dv: @cfg_empresa.dv, logo: @cfg_empresa.logo, nivel: @cfg_empresa.nivel, nom_rep_legal: @cfg_empresa.nom_rep_legal, num_doc_rep_legal: @cfg_empresa.num_doc_rep_legal, num_identificacion: @cfg_empresa.num_identificacion, observaciones: @cfg_empresa.observaciones, razon_rip: @cfg_empresa.razon_rip, razon_social: @cfg_empresa.razon_social, regimen: @cfg_empresa.regimen, telefono_1: @cfg_empresa.telefono_1, telefono_2: @cfg_empresa.telefono_2, tipo_doc: @cfg_empresa.tipo_doc, tipo_doc_rep_legal: @cfg_empresa.tipo_doc_rep_legal, website: @cfg_empresa.website } }
    assert_redirected_to cfg_empresa_url(@cfg_empresa)
  end

  test "should destroy cfg_empresa" do
    assert_difference('CfgEmpresa.count', -1) do
      delete cfg_empresa_url(@cfg_empresa)
    end

    assert_redirected_to cfg_empresas_url
  end
end
