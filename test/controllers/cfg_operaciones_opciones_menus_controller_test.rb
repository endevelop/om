require 'test_helper'

class CfgOperacionesOpcionesMenusControllerTest < ActionController::TestCase
  setup do
    @cfg_opciones_menu = cfg_opciones_menu(:one)
    @cfg_operaciones_opciones_menu = cfg_operaciones_opciones_menu(:one)
  end

  test "should get index" do
    get :index, params: { cfg_opciones_menu_id: @cfg_opciones_menu }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { cfg_opciones_menu_id: @cfg_opciones_menu }
    assert_response :success
  end

  test "should create cfg_operaciones_opciones_menu" do
    assert_difference('CfgOperacionesOpcionesMenu.count') do
      post :create, params: { cfg_opciones_menu_id: @cfg_opciones_menu, cfg_operaciones_opciones_menu: @cfg_operaciones_opciones_menu.attributes }
    end

    assert_redirected_to cfg_opciones_menu_cfg_operaciones_opciones_menu_path(@cfg_opciones_menu, CfgOperacionesOpcionesMenu.last)
  end

  test "should show cfg_operaciones_opciones_menu" do
    get :show, params: { cfg_opciones_menu_id: @cfg_opciones_menu, id: @cfg_operaciones_opciones_menu }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { cfg_opciones_menu_id: @cfg_opciones_menu, id: @cfg_operaciones_opciones_menu }
    assert_response :success
  end

  test "should update cfg_operaciones_opciones_menu" do
    put :update, params: { cfg_opciones_menu_id: @cfg_opciones_menu, id: @cfg_operaciones_opciones_menu, cfg_operaciones_opciones_menu: @cfg_operaciones_opciones_menu.attributes }
    assert_redirected_to cfg_opciones_menu_cfg_operaciones_opciones_menu_path(@cfg_opciones_menu, CfgOperacionesOpcionesMenu.last)
  end

  test "should destroy cfg_operaciones_opciones_menu" do
    assert_difference('CfgOperacionesOpcionesMenu.count', -1) do
      delete :destroy, params: { cfg_opciones_menu_id: @cfg_opciones_menu, id: @cfg_operaciones_opciones_menu }
    end

    assert_redirected_to cfg_opciones_menu_cfg_operaciones_opciones_menu_index_path(@cfg_opciones_menu)
  end
end
