﻿ALTER TABLE cfg_imagenes ADD COLUMN image_file_name text;
ALTER TABLE cfg_imagenes ADD COLUMN image_content_type bytea;
ALTER TABLE cfg_imagenes ADD COLUMN image_file_size text;
ALTER TABLE cfg_imagenes ADD COLUMN image_updated_at timestamp without time zone;
ALTER TABLE cfg_pacientes ADD COLUMN cfg_imagen_id bigint;
ALTER TABLE cfg_pacientes ADD CONSTRAINT cfg_imagen_fk FOREIGN KEY (cfg_imagen_id) REFERENCES cfg_imagenes (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cfg_empresas ADD COLUMN cfg_imagen_id bigint;
ALTER TABLE cfg_empresas ADD CONSTRAINT cfg_imagen_fk FOREIGN KEY (cfg_imagen_id) REFERENCES cfg_imagenes (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;