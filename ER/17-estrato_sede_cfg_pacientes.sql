﻿ALTER TABLE cfg_pacientes ADD COLUMN estrato_id integer;
ALTER TABLE cfg_pacientes ADD CONSTRAINT estrato_fk FOREIGN KEY (estrato_id) REFERENCES cfg_clasificaciones (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cfg_pacientes ADD COLUMN cfg_sede_id integer;
ALTER TABLE cfg_pacientes ADD CONSTRAINT cfg_sede_fk FOREIGN KEY (cfg_sede_id) REFERENCES cfg_sedes (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;