-- CREANDO LAS COLUMNAS PARA GUARDAR EN LA BD LA IMAGEN
ALTER TABLE cfg_empresas ADD COLUMN filename text;
ALTER TABLE cfg_empresas ADD COLUMN content_type text;
ALTER TABLE cfg_empresas ADD COLUMN file_contents bytea;

-- ELIMINANDO LAS COLUMNAS PREVIAMENTE CREADAS 
ALTER TABLE cfg_empresas DROP COLUMN logo;
ALTER TABLE cfg_empresas DROP COLUMN image;


-- PLUGIN PAPERCLIP
-- CREANDO LAS COLUMNAS PARA GUARDAR EN LA BD LA IMAGEN
ALTER TABLE cfg_empresas ADD COLUMN image_file_name text;
ALTER TABLE cfg_empresas ADD COLUMN image_content_type bytea;
ALTER TABLE cfg_empresas ADD COLUMN image_file_size text;
ALTER TABLE cfg_empresas ADD COLUMN image_updated_at timestamp without time zone;
