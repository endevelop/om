-- PERFILES
ALTER TABLE cfg_perfiles_usuario RENAME TO cfg_perfiles_usuarios;
ALTER TABLE cfg_perfiles_usuarios RENAME id_perfil  TO id;
COMMENT ON COLUMN cfg_perfiles_usuarios.id IS 'identificador del perfil';
ALTER TABLE cfg_perfiles_usuarios ADD COLUMN activo boolean NOT NULL DEFAULT true;


-- CFG USUARIOS 
ALTER TABLE cfg_usuarios DROP COLUMN clave;
ALTER TABLE cfg_usuarios DROP COLUMN departamento;
ALTER TABLE cfg_usuarios DROP COLUMN municipio;
ALTER TABLE cfg_usuarios RENAME id_usuario  TO id;
ALTER TABLE cfg_usuarios RENAME id_perfil  TO cfg_perfiles_usuario_id;
ALTER TABLE cfg_usuarios ADD COLUMN cfg_municipio_id integer;
ALTER TABLE cfg_usuarios ADD COLUMN user_id integer;
ALTER TABLE cfg_usuarios ADD FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cfg_usuarios ALTER COLUMN estado_cuenta SET DEFAULT true;
ALTER TABLE cfg_usuarios ALTER COLUMN mostrar_en_historias SET DEFAULT true;
COMMENT ON COLUMN cfg_usuarios.mostrar_en_historias IS 'Permite identificar si este usuario se lista en los formularios de registros clinicos';
ALTER TABLE cfg_usuarios ALTER COLUMN visible SET DEFAULT true;
COMMENT ON COLUMN cfg_usuarios.visible IS 'se muetra o no el medico en los listados';


-- OPCIONES DEL MENU
ALTER TABLE cfg_opciones_menu RENAME id_opcion_menu  TO id;
COMMENT ON COLUMN cfg_opciones_menu.id IS 'identificador de la opcion';

-- OPCIONES DEL MENU SEGUN EL PERFIL DE USUARIO
DELETE FROM cfg_opciones_menu_perfil_usuario WHERE id_opcion_menu >= 129;
ALTER TABLE cfg_opciones_menu_perfil_usuario RENAME id_perfil_usuario  TO id;
ALTER TABLE cfg_opciones_menu_perfil_usuario RENAME id_opcion_menu  TO cfg_opciones_menu_id;
ALTER TABLE cfg_opciones_menu_perfil_usuario RENAME TO cfg_opciones_menu_perfil_usuarios;
COMMENT ON COLUMN cfg_opciones_menu_perfil_usuarios.id IS 'identificador del perfil';
COMMENT ON COLUMN cfg_opciones_menu_perfil_usuarios.cfg_opciones_menu_id IS 'identificador en la tabla opciones_menu';

ALTER TABLE cfg_opciones_menu_perfil_usuarios RENAME id  TO cfg_perfiles_usuario_id;
ALTER TABLE cfg_opciones_menu_perfil_usuarios ADD COLUMN id serial;
COMMENT ON COLUMN cfg_opciones_menu_perfil_usuarios.cfg_perfiles_usuario_id IS 'identificador de la opcion';

ALTER TABLE cfg_opciones_menu_perfil_usuarios
  ADD FOREIGN KEY (cfg_opciones_menu_id) REFERENCES cfg_opciones_menu (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cfg_opciones_menu_perfil_usuarios DROP CONSTRAINT opciones_perfil_pkey;
ALTER TABLE cfg_opciones_menu_perfil_usuarios ADD PRIMARY KEY (id);


-- INSERTAR USUARIOS DE PRUEBA
INSERT INTO users VALUES (41, 'enriquep@gmail.com', '$2a$11$/bSwFEdpsuzVdmVyejtxm.yGLt78fyZBTZig7EihBW2duW5ulA9Be', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2018-03-18 02:12:09.819913', '2018-03-18 02:12:09.819913', 'enriquep');
INSERT INTO users VALUES (1, 'diazeliana@gmail.com', '$2a$11$CDbQeX28g.uprNPnJ4wTDeqZhtPqhYpCig7m/hXp5Hk4UmLEyJS36', NULL, NULL, NULL, 30, '2018-03-18 15:40:07.2283', '2018-03-18 00:33:02.970897', '127.0.0.1', '::1', '2018-03-09 23:28:09.177365', '2018-03-18 15:40:07.279955', 'ediaz');
INSERT INTO users VALUES (36, 'ncarrasquero@gmail.com', '$2a$11$VCadGsyXgL604FvcIP9OP.zHsTyowsRF7trrCKX7JZje/7z4BjHDm', NULL, NULL, NULL, 1, '2018-03-18 17:28:58.390555', '2018-03-18 17:28:58.390555', '127.0.0.1', '127.0.0.1', '2018-03-15 01:01:47.35246', '2018-03-18 17:28:58.399384', 'nestor');


INSERT INTO cfg_usuarios VALUES (110, 1773, '123', 1764, 'nestor', true, 1, 'OBSERVACIONES', 'NESTOR', 'D', 'CARRASQUERO', 'P', 'DIRECCION', '1111111111', '2222222222', '3333333333', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-18 02:12:09.845613', NULL, NULL, 276, false, 1854, false, 1, 36);
INSERT INTO cfg_usuarios VALUES (92, 1773, '1234', 1764, 'ediaz', true, 2, 'OBSERVACIONES', 'ELIANA', 'M', 'DIAZ', 'R', 'DIRECCION', '12345678', '123456789', '1234567890', 'diazeliana@gmail.com', NULL, NULL, NULL, NULL, NULL, '2018-03-12 00:00:00', NULL, NULL, 277, true, NULL, true, 1, 1);
INSERT INTO cfg_usuarios VALUES (111, 1774, '123456789', 1764, 'enriquep', true, 3, 'OBSERVACIONES...... OBSERVACIONES...... OBSERVACIONES...... OBSERVACIONES...... OBSERVACIONES...... OBSERVACIONES......', 'ENRIQUE', 'E', 'PEREZ', 'P', 'DIRECCION DIRECCION ', '1111111111', '2222222222', '33333333333333333333', 'enriquep@gmail.com', 'CARGO ACTUAL', 'REGISTRO PROFESIONAL', 59, 'UNIDAD FUNCIONAL', 10, '2018-03-18 02:12:09.845613', NULL, NULL, 276, true, 1854, true, 5, 41);


