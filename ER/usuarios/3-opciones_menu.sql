-- Blanquear todas las url de las opciones del menu
UPDATE cfg_opciones_menu SET url_opcion=null
-- Opcion de Empresas
UPDATE cfg_opciones_menu SET url_opcion='/cfg_empresas' WHERE id IN (22, 23, 64);
-- Opcion de Usuarios
UPDATE cfg_opciones_menu SET url_opcion='/cfg_usuarios' WHERE id IN (27,68);
-- Opcion de Pacientes
UPDATE cfg_opciones_menu SET url_opcion='/cfg_pacientes' WHERE id IN (12);
-- Opcion de Perfiles de usuario
UPDATE cfg_opciones_menu SET url_opcion='/cfg_perfiles_usuarios' WHERE id IN (29);
-- Opcion de Opciones Menu
UPDATE cfg_opciones_menu SET url_opcion='/cfg_opciones_menus' WHERE id IN (28);

--SELECT id, nombre_opcion 
--FROM cfg_opciones_menu 
--WHERE nombre_opcion ilike '%paciente%'
