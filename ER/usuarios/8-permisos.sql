﻿
CREATE TABLE cfg_operaciones_opciones_menu
(
  id serial,
  cfg_opciones_menu_id integer NOT NULL,
  operacion text,
  activo boolean DEFAULT true,
  CONSTRAINT cfg_permisos_opciones_menu_pkey PRIMARY KEY (id),
  CONSTRAINT cfg_permisos_opciones_menu_cfg_opciones_menu_id_fkey FOREIGN KEY (cfg_opciones_menu_id)
      REFERENCES cfg_opciones_menu (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cfg_operaciones_opciones_menu
  OWNER TO postgres;
GRANT ALL ON TABLE cfg_operaciones_opciones_menu TO public;
GRANT ALL ON TABLE cfg_operaciones_opciones_menu TO postgres;




CREATE TABLE cfg_operaciones_detalle
(
  id serial NOT NULL,
  cfg_operaciones_opciones_menu_id integer NOT NULL,
  controlador text NOT NULL,
  accion text NOT NULL,
  activo boolean DEFAULT true,
  CONSTRAINT cfg_operaciones_detalle_pkey PRIMARY KEY (id),
  CONSTRAINT cfg_operaciones_detalle_cfg_operaciones_opciones_menu_id_fkey FOREIGN KEY (cfg_operaciones_opciones_menu_id)
      REFERENCES cfg_operaciones_opciones_menu (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cfg_operaciones_detalle
  OWNER TO postgres;
GRANT ALL ON TABLE cfg_operaciones_detalle TO public;
GRANT ALL ON TABLE cfg_operaciones_detalle TO postgres;




CREATE TABLE cfg_operaciones_perfiles
(
  id serial NOT NULL,
  cfg_perfiles_usuario_id integer NOT NULL,
  cfg_operaciones_opciones_menu_id integer NOT NULL,
  CONSTRAINT cfg_operaciones_perfiles_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cfg_operaciones_perfiles
  OWNER TO postgres;
GRANT ALL ON TABLE cfg_operaciones_perfiles TO public;
GRANT ALL ON TABLE cfg_operaciones_perfiles TO postgres;



