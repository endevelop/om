ALTER TABLE cfg_usuarios
  ADD COLUMN cfg_imagen_id integer;
ALTER TABLE cfg_usuarios
  ADD FOREIGN KEY (cfg_imagen_id) REFERENCES cfg_imagenes (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cfg_usuarios RENAME firma  TO firma_id;
