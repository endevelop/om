-- CFG_PERFILES_USUARIO
ALTER TABLE cfg_perfiles_usuarios  ADD COLUMN asociado_empresa boolean NOT NULL DEFAULT false;
ALTER TABLE cfg_perfiles_usuarios  ADD COLUMN observaciones text;


-- CFG_USUARIOS
ALTER TABLE cfg_usuarios ADD COLUMN cfg_empresa_id integer;
ALTER TABLE cfg_usuarios ADD COLUMN cfg_sede_id integer;
DROP INDEX index_users_on_email;


CREATE OR REPLACE FUNCTION compara_ascii(character varying)
  RETURNS text AS
$BODY$
SELECT TRANSLATE
($1,
'áàâãäéèêëíìïóòôõöúùûüÁÀÂÃÄÉÈÊËÍÌÏÓÒÔÕÖÚÙÛÜçÇ',
'aaaaaeeeeiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcC');
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION compara_ascii(character varying)
  OWNER TO postgres;
