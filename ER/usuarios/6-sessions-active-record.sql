
CREATE TABLE sessiones
(
  id bigserial NOT NULL,
  session_id character varying NOT NULL,
  data text,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  CONSTRAINT sessiones_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sessiones
  OWNER TO postgres;
GRANT ALL ON TABLE sessiones TO public;
GRANT ALL ON TABLE sessiones TO postgres;

-- Index: index_sessiones_on_session_id

-- DROP INDEX index_sessiones_on_session_id;

CREATE UNIQUE INDEX index_sessiones_on_session_id
  ON sessiones
  USING btree
  (session_id COLLATE pg_catalog."default");

-- Index: index_sessiones_on_updated_at

-- DROP INDEX index_sessiones_on_updated_at;

CREATE INDEX index_sessiones_on_updated_at
  ON sessiones
  USING btree
  (updated_at);


