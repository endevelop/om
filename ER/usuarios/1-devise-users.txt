-- Table: users

-- DROP TABLE users;

CREATE TABLE users
(
  id bigserial NOT NULL,
  email character varying NOT NULL DEFAULT ''::character varying,
  encrypted_password character varying NOT NULL DEFAULT ''::character varying,
  reset_password_token character varying,
  reset_password_sent_at timestamp without time zone,
  remember_created_at timestamp without time zone,
  sign_in_count integer NOT NULL DEFAULT 0,
  current_sign_in_at timestamp without time zone,
  last_sign_in_at timestamp without time zone,
  current_sign_in_ip inet,
  last_sign_in_ip inet,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;
GRANT ALL ON TABLE users TO public;
GRANT ALL ON TABLE users TO postgres;

-- Index: index_users_on_email

-- DROP INDEX index_users_on_email;

CREATE UNIQUE INDEX index_users_on_email
  ON users
  USING btree
  (email COLLATE pg_catalog."default");

-- Index: index_users_on_reset_password_token

-- DROP INDEX index_users_on_reset_password_token;

CREATE UNIQUE INDEX index_users_on_reset_password_token
  ON users
  USING btree
  (reset_password_token COLLATE pg_catalog."default");


ALTER TABLE users ADD COLUMN username text;
ALTER TABLE users ADD UNIQUE (username);
