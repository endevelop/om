﻿-- ELIMINAR REFERENCIAS FORANEAS A LA TABLA cfg_empresas
ALTER TABLE public.inv_bodegas DROP CONSTRAINT fk_inv_bodegas_id_empresa;
ALTER TABLE public.xlab_orden DROP CONSTRAINT fk_xlab_orden_empresa_id; 
ALTER TABLE public.inv_entrega_medicamentos DROP CONSTRAINT fk_inv_entrega_medicamentos_id_empresa;
ALTER TABLE public.inv_movimientos DROP CONSTRAINT fk_inv_movimientos_id_empresa;
ALTER TABLE public.inv_orden_compra DROP CONSTRAINT fk_inv_orden_compra_id_empresa;
------------------ALTER TABLE public.cfg_sede DROP CONSTRAINT cfg_sede_cfg_empresa_fk;
-- RENOMBRAR TABLA cfg_empresa
ALTER TABLE public.cfg_empresa RENAME TO cfg_empresas;
-- ELIMINAR PRIMARY KEY EN TABLA cfg_empresas
ALTER TABLE public.cfg_empresas DROP CONSTRAINT cfg_empresa_pkey;
-- ELIMINAR CAMPO EN TABLA cfg_empresas
ALTER TABLE public.cfg_empresas DROP COLUMN cod_empresa;
-- CREAR CAMPO DE ACUERDO A LA CONVENCION RAILS
ALTER TABLE public.cfg_empresas ADD COLUMN id serial;
ALTER TABLE public.cfg_empresas ADD CONSTRAINT cfg_empresa_pkey PRIMARY KEY (id);
-- GENERAR REFERENCIAS A CLAVE FORÁNEA
ALTER TABLE public.inv_bodegas DROP COLUMN id_empresa;
ALTER TABLE public.inv_bodegas ADD COLUMN cfg_empresa_id bigint;
ALTER TABLE public.inv_bodegas ADD CONSTRAINT fk_inv_bodegas_id_empresa FOREIGN KEY (cfg_empresa_id) REFERENCES public.cfg_empresas (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.xlab_orden DROP COLUMN empresa_id;
ALTER TABLE public.xlab_orden ADD COLUMN cfg_empresa_id bigint;
ALTER TABLE public.xlab_orden ADD CONSTRAINT fk_xlab_orden_empresa_id FOREIGN KEY (cfg_empresa_id) REFERENCES public.cfg_empresas (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.inv_entrega_medicamentos DROP COLUMN id_empresa;
ALTER TABLE public.inv_entrega_medicamentos ADD COLUMN cfg_empresa_id bigint;
ALTER TABLE public.inv_entrega_medicamentos ADD CONSTRAINT fk_inv_entrega_medicamentos_id_empresa FOREIGN KEY (cfg_empresa_id) REFERENCES public.cfg_empresas (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.inv_movimientos DROP COLUMN id_empresa;
ALTER TABLE public.inv_movimientos ADD COLUMN cfg_empresa_id bigint;
ALTER TABLE public.inv_movimientos ADD CONSTRAINT fk_inv_movimientos_id_empresa FOREIGN KEY (cfg_empresa_id) REFERENCES public.cfg_empresas (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.inv_orden_compra DROP COLUMN id_empresa;
ALTER TABLE public.inv_orden_compra ADD COLUMN cfg_empresa_id bigint;
ALTER TABLE public.inv_orden_compra ADD CONSTRAINT fk_inv_orden_compra_id_empresa FOREIGN KEY (cfg_empresa_id) REFERENCES public.cfg_empresas (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.cfg_sede ADD COLUMN cfg_empresa_id bigint;
ALTER TABLE public.cfg_sede ADD CONSTRAINT cfg_sede_cfg_empresa_fk FOREIGN KEY (cfg_empresa_id) REFERENCES public.cfg_empresas (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
