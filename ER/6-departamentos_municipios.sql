CREATE TABLE public.cfg_departamentos
(
   id serial NOT NULL, 
   nombre text, 
   PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.cfg_departamentos
  OWNER TO postgres;

CREATE TABLE public.cfg_municipios
(
   id serial NOT NULL, 
   nombre text NOT NULL, 
   cfg_departamento_id integer NOT NULL, 
   PRIMARY KEY (id), 
   FOREIGN KEY (cfg_departamento_id) REFERENCES cfg_departamentos (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.cfg_municipios
  OWNER TO postgres;

INSERT INTO cfg_departamentos(nombre) VALUES('AMAZONAS'); 
INSERT INTO cfg_departamentos(nombre) VALUES('ANTIOQUIA'); 
INSERT INTO cfg_departamentos(nombre) VALUES('ARAUCA');
SELECT * FROM cfg_departamentos;
INSERT INTO cfg_municipios(nombre, cfg_departamento_id) VALUES('LETICIA', 1);  
INSERT INTO cfg_municipios(nombre, cfg_departamento_id) VALUES('LA PEDRERA', 1); 
INSERT INTO cfg_municipios(nombre, cfg_departamento_id) VALUES('EL ENCANTO', 1);   

INSERT INTO cfg_municipios(nombre, cfg_departamento_id) VALUES('NARIÑO', 2);  
INSERT INTO cfg_municipios(nombre, cfg_departamento_id) VALUES('MEDELLIN', 2);  


ALTER TABLE cfg_empresas
  ADD COLUMN cfg_municipio_id integer;
ALTER TABLE cfg_empresas
  ADD FOREIGN KEY (cfg_municipio_id) REFERENCES cfg_municipios (id) ON UPDATE NO ACTION ON DELETE NO ACTION;



--------------------------
--rails generate model CfgDepartamento nombre:string

--rails generate model CfgMunicipio nombre:string cfg_departamento:belongs_to