﻿-- ELIMINAR REFERENCIAS A LA TABLA cfg_sede
ALTER TABLE public.fac_caja DROP CONSTRAINT fac_caja_id_sede_fkey;
ALTER TABLE public.sin_nodos DROP CONSTRAINT fk_sin_nodos_sede;
ALTER TABLE public.cfg_consultorios DROP CONSTRAINT cfg_consultorios_id_sede_fkey;
-- ELIMINAR RESTRICCION PRIMARY KEY EN cfg_sede
ALTER TABLE public.cfg_sede DROP CONSTRAINT cfg_sede_pkey;
-- CAMBIO DE NOMBRE EN TABLA
ALTER TABLE public.cfg_sede RENAME TO cfg_sedes;
-- CAMBIANDO NOMBRE DE COLUMNA
ALTER TABLE public.cfg_sedes RENAME COLUMN id_sede TO id;
ALTER TABLE public.cfg_sedes ADD CONSTRAINT cfg_sede_pkey PRIMARY KEY (id);
-- CAMBIAR NOMBRES DE CAMPOS REFERENCIA Y RESTRICCIONES FOREIGN KEY
ALTER TABLE public.fac_caja RENAME COLUMN id_sede TO cfg_sede_id;
ALTER TABLE public.fac_caja ADD CONSTRAINT fac_caja_id_sede_fkey FOREIGN KEY (cfg_sede_id) REFERENCES public.cfg_sedes (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.sin_nodos RENAME COLUMN id_sede TO cfg_sede_id;
ALTER TABLE public.sin_nodos ADD CONSTRAINT fk_sin_nodos_sede FOREIGN KEY (cfg_sede_id) REFERENCES public.cfg_sedes (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.cfg_consultorios RENAME COLUMN id_sede TO cfg_sede_id;
ALTER TABLE public.cfg_consultorios ADD CONSTRAINT cfg_consultorios_id_sede_fkey FOREIGN KEY (cfg_sede_id) REFERENCES public.cfg_sedes (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
