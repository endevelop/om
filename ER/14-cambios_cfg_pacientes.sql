﻿-- RENOMBRANDO CAMPO CLAVE TABLA cfg_pacientes
ALTER TABLE cfg_pacientes RENAME COLUMN id_paciente TO id;
-- RENOMBRANDO CLAVES FORANEAS municipio y departamento direccion paciente
ALTER TABLE cfg_pacientes RENAME COLUMN municipio TO cfg_municipio_id;
ALTER TABLE cfg_pacientes RENAME COLUMN departamento TO cfg_departamento_id;
ALTER TABLE cfg_pacientes DROP CONSTRAINT cfg_pacientes_municipio_fkey;
ALTER TABLE cfg_pacientes DROP CONSTRAINT pacientes_departamento_fkey;
ALTER TABLE cfg_pacientes ADD CONSTRAINT cfg_pacientes_municipio_fkey FOREIGN KEY (cfg_municipio_id) REFERENCES cfg_municipios MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cfg_pacientes ADD CONSTRAINT pacientes_departamento_fkey FOREIGN KEY (cfg_departamento_id) REFERENCES cfg_departamentos MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
-- RENOMBRANDO CLAVES FORANEAS municipio y departamento de nacimiento paciente
ALTER TABLE cfg_pacientes DROP CONSTRAINT cfg_pacientes_mun_nacimiento_fkey;
ALTER TABLE cfg_pacientes DROP CONSTRAINT cfg_pacientes_dep_nacimiento_fkey;
ALTER TABLE cfg_pacientes ADD CONSTRAINT cfg_pacientes_mun_nacimiento_fkey FOREIGN KEY (mun_nacimiento) REFERENCES cfg_municipios MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cfg_pacientes ADD CONSTRAINT cfg_pacientes_dep_nacimiento_fkey FOREIGN KEY (dep_nacimiento) REFERENCES cfg_departamentos MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
-- SI HAY ALGUN PROBLEMA CON CLAVE FORÁNEA EN TABLA cfg_pacientes, ES MEJOR BORRAR EL CONTENIDO DE LA MISMA. EJECUTAR EL COMANDO DE ABAJO Y LUEGO PROCEDER CON EL SCRIPT DE ARRIBA
TRUNCATE TABLE cfg_pacientes;
-- CONSULTA PARA DETERMINAR AQUELLAS TABLAS QUE REFERENCIAN VIA FOREIGN_KEY A OTRA
SELECT 'ALTER TABLE '||k.table_name||' RENAME COLUMN '||k.column_name||' TO cfg_paciente_id;' FROM information_schema.key_column_usage k 
JOIN information_schema.constraint_table_usage c ON k.constraint_name = c.constraint_name WHERE c.table_name = 'cfg_pacientes';
-- RESULTADO DE LA CONSULTA ANTERIOR. SE SUPRIMEN LAS COMILLAS DOBLES Y EJECUTAN LAS QUE NO INVOLUCREN DIRECTAMENTE LA TABLA CONSULTADA
ALTER TABLE cit_autorizaciones RENAME COLUMN paciente TO cfg_paciente_id;
ALTER TABLE cit_citas RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE fac_consumo_insumo RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE fac_consumo_medicamento RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE fac_consumo_paquete RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE fac_consumo_servicio RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE fac_factura_paciente RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE odo_registro RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE xlab_orden RENAME COLUMN paciente_id TO cfg_paciente_id;
ALTER TABLE hc_archivos RENAME COLUMN id_paciente TO cfg_paciente_id;
ALTER TABLE hc_estructura_familiar RENAME COLUMN id_paciente TO cfg_paciente_id;