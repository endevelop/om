ALTER TABLE cfg_clasificaciones
  ADD COLUMN activo boolean DEFAULT true;

INSERT INTO cfg_clasificaciones(codigo, maestro, descripcion, activo)
  VALUES (1, 'TipoDocumento', 'Rif', true);

INSERT INTO cfg_clasificaciones(codigo, maestro, descripcion, activo)
  VALUES (1, 'TipoDocumento', 'Identificación', true);

