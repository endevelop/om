﻿alter table cfg_pacientes add column image_file_name text;
alter table cfg_pacientes add column image_content_type bytea;
alter table cfg_pacientes add column image_file_size text;
alter table cfg_pacientes add column image_updated_at timestamp without time zone;