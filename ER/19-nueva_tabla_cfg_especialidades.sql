﻿-- CREAR TABLA cfg_especialidades
CREATE TABLE cfg_especialidades
(
  id serial NOT NULL,
  descripcion text,
  duracion_consulta integer DEFAULT 15,
  activo boolean DEFAULT true,
  CONSTRAINT cfg_especialidad_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cfg_especialidades OWNER TO postgres;
GRANT ALL ON TABLE cfg_especialidades TO postgres;
GRANT ALL ON TABLE cfg_especialidades TO public;

-- AGREGAR REFERENCIA A cfg_especialidades EN TABLA cfg_usuarios
ALTER TABLE cfg_usuarios ADD COLUMN cfg_especialidad_id integer;
ALTER TABLE cfg_usuarios ADD CONSTRAINT especialidad_fk FOREIGN KEY (cfg_especialidad_id) REFERENCES cfg_especialidades (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

-- GUARDAR VALORES DE ACUERDO A LOS EXISTENTES EN cfg_clasificaciones
INSERT INTO cfg_especialidades (descripcion)(SELECT descripcion FROM cfg_clasificaciones WHERE maestro = 'Especialidad');