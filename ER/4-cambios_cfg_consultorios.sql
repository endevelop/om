﻿-- ELIMINANDO REFERENCIAS A LA TABLA cfg_consultorios
ALTER TABLE public.fac_factura_insumo DROP CONSTRAINT fac_factura_insumo_id_consultorio_fkey;
ALTER TABLE public.fac_factura_medicamento DROP CONSTRAINT fac_factura_medicamento_id_consultorio_fkey;
ALTER TABLE public.fac_factura_paquete DROP CONSTRAINT fac_factura_paquete_id_consultorio_fkey;
ALTER TABLE public.fac_factura_servicio DROP CONSTRAINT fac_factura_servicio_id_consultorio_fkey;
-- ELIMINANDO REFERENCIA INTERNA EN TABLA cfg_consultorios
ALTER TABLE public.cfg_consultorios DROP CONSTRAINT cfg_consultorios_pkey;
ALTER TABLE public.cfg_consultorios RENAME COLUMN id_consultorio TO id;
-- CREANDO REFERENCIA INTERNA EN TABLA cfg_consultorios, DE NUEVO
ALTER TABLE public.cfg_consultorios ADD CONSTRAINT cfg_consultorios_pkey PRIMARY KEY (id);
-- GENERANDO CAMPOS Y REFERENCIAS EXTERNAS
ALTER TABLE public.fac_factura_insumo RENAME id_consultorio TO cfg_consultorio_id;
ALTER TABLE public.fac_factura_insumo ADD CONSTRAINT fac_factura_insumo_id_consultorio_fkey FOREIGN KEY (cfg_consultorio_id)
      REFERENCES public.cfg_consultorios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.fac_factura_medicamento RENAME id_consultorio TO cfg_consultorio_id;
ALTER TABLE public.fac_factura_medicamento ADD CONSTRAINT fac_factura_medicamento_id_consultorio_fkey FOREIGN KEY (cfg_consultorio_id)
      REFERENCES public.cfg_consultorios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.fac_factura_paquete RENAME id_consultorio TO cfg_consultorio_id;
ALTER TABLE public.fac_factura_paquete ADD CONSTRAINT fac_factura_paquete_id_consultorio_fkey FOREIGN KEY (cfg_consultorio_id)
      REFERENCES public.cfg_consultorios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE public.fac_factura_servicio RENAME id_consultorio TO cfg_consultorio_id;
ALTER TABLE public.fac_factura_servicio ADD CONSTRAINT fac_factura_servicio_id_consultorio_fkey FOREIGN KEY (cfg_consultorio_id)
      REFERENCES public.cfg_consultorios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
