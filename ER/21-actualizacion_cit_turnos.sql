﻿-- CAMBIANDO REFERENCIAS EN LA TABLA cit_turnos
ALTER TABLE cit_turnos RENAME id_turno TO id;
ALTER TABLE cit_turnos RENAME id_prestador TO cfg_usuario_id;
ALTER TABLE cit_turnos RENAME id_consultorio TO cfg_consultorio_id;
ALTER TABLE cit_turnos RENAME id_horario TO cfg_horario_id;
ALTER TABLE cit_turnos ADD CONSTRAINT consultorio_fk FOREIGN KEY (cfg_consultorio_id) REFERENCES cfg_consultorios (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cit_turnos ADD CONSTRAINT usuario_fk FOREIGN KEY (cfg_usuario_id) REFERENCES cfg_usuarios (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;