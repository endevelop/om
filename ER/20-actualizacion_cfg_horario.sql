﻿ALTER TABLE cfg_horario RENAME TO cfg_horarios;
ALTER TABLE cfg_items_horario RENAME TO cfg_items_horarios;
ALTER TABLE cfg_horarios RENAME COLUMN id_horario TO id;
ALTER TABLE cfg_items_horarios RENAME COLUMN id_item_horario TO id;
ALTER TABLE cfg_items_horarios RENAME COLUMN id_horario TO cfg_horario_id;