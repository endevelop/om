﻿-- CAMBIAR RESTRICCIONES EN TABLA cfg_dias_no_laborales 
ALTER TABLE cfg_dias_no_laborales DROP CONSTRAINT cfg_dias_no_laborales_pkey;
ALTER TABLE cfg_dias_no_laborales ADD COLUMN id serial;
ALTER TABLE cfg_dias_no_laborales ADD CONSTRAINT cfg_dias_no_laborales_pk PRIMARY KEY (id);
ALTER TABLE cfg_dias_no_laborales RENAME COLUMN id_sede TO cfg_sede_id;
ALTER TABLE cfg_dias_no_laborales ADD CONSTRAINT unica_fecha_sede UNIQUE (cfg_sede_id, fecha_no_laboral);
