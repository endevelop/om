﻿ALTER TABLE cfg_sedes ADD COLUMN cfg_municipio_id integer;
ALTER TABLE cfg_sedes ADD CONSTRAINT cfg_sede_cfg_municipio_fkey FOREIGN KEY (cfg_municipio_id)
      REFERENCES cfg_municipios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;