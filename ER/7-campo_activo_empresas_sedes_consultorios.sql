﻿ALTER TABLE cfg_empresas ADD COLUMN activo boolean default true;
ALTER TABLE cfg_sedes ADD COLUMN activo boolean default true;
ALTER TABLE cfg_consultorios ADD COLUMN activo boolean default true;