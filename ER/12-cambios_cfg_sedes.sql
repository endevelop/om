-- ELIMINANDO LOS CONSTRAINS DE departamento y municipio
-- SE CREO EL CAMPO cfg_municipio_id
ALTER TABLE cfg_sedes
  DROP CONSTRAINT cfg_sede_departamento_fkey;
ALTER TABLE cfg_sedes
  DROP CONSTRAINT cfg_sede_municipio_fkey;
