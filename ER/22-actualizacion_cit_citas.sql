﻿-- CAMBIO DE NOMBRE EN REFERENCIAS DE LA TABLA cit_citas
ALTER TABLE cit_citas RENAME COLUMN id_administradora TO fac_administradora_id;
ALTER TABLE cit_citas RENAME COLUMN id_autorizacion TO cit_autorizacion_id;
ALTER TABLE cit_citas RENAME COLUMN id_paquete TO cit_paq_maestro_id;
ALTER TABLE cit_citas RENAME COLUMN id_prestador TO cfg_usuario_id;
ALTER TABLE cit_citas RENAME COLUMN id_servicio TO fac_servicio_id;
ALTER TABLE cit_citas RENAME COLUMN id_turno TO cit_turno_id;

-- CAMBIO DE CAMPO CLAVE EN TABLA fac_servicio
ALTER TABLE fac_servicio RENAME COLUMN id_servicio TO id;