﻿ALTER TABLE fac_administradora RENAME TO fac_administradoras;
ALTER TABLE fac_administradoras RENAME COLUMN id_administradora TO id;
ALTER TABLE cfg_pacientes RENAME COLUMN id_administradora TO fac_administradora_id;
ALTER TABLE cfg_pacientes ADD CONSTRAINT fac_administradora_fk FOREIGN KEY (fac_administradora_id) REFERENCES fac_administradoras (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;